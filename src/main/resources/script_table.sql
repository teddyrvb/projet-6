create table book
(
    ID          int auto_increment
        primary key,
    title       text not null,
    author      text not null,
    number_page int  not null
)
    engine = InnoDB;

create table exemplary
(
    id      int auto_increment
        primary key,
    book_id int null,
    constraint book_id_fk
        foreign key (book_id) references book (ID)
)
    engine = InnoDB;

create table user
(
    ID           int auto_increment
        primary key,
    name         varchar(255) not null,
    first_name   varchar(255) not null,
    adress       varchar(255) not null,
    phone_number int          not null,
    email_adress varchar(255) not null
)
    engine = InnoDB;

create table booking
(
    ID            int auto_increment
        primary key,
    user_id       int  not null,
    starting_date datetime not null,
    book_id       int  not null,
    constraint book_id_fk_for_booking
        foreign key (book_id) references book (ID),
    constraint user_id_fk_for_booking
        foreign key (user_id) references user (ID)
)
    engine = InnoDB;

create table loan
(
    ID            int auto_increment
        primary key,
    user_id       int        not null,
    exemplary_id  int        not null,
    starting_date datetime       not null,
    expired_date  datetime       null,
    to_confirm    tinyint(1) null,
    constraint exemplary_id_fk
        foreign key (exemplary_id) references exemplary (id),
    constraint user_id_fk
        foreign key (user_id) references user (ID)
)
    engine = InnoDB;

