<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>formulaire d'édition d'un livre par l'administrateur</title>
    <link href="<c:url value='/static/css/form.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">
</head>
<body>
<div class="container-fluid">
    <form:form method="POST" modelAttribute="bookEditForm">
        <div class="form-group">
            <form:label path="title" for="title" class="title">Titre</form:label>
            <form:input path="title" type="text" class="form-control" id="title" aria-describedby="emailHelp"/>
            <small id="titleHelp" class="form-text text-muted">Veuillez entrer ici le titre du livre</small>
        </div>
        <div class="form-group">
            <form:label path="author" for="author" class="author">Autheur</form:label>
            <form:input path="author" type="text" id="author" class="form-control"/>
            <small id="AuthorHelp" class="form-text text-muted">Veuillez ici indiquer l'auteur du livre, si il y en a
                un
            </small>
        </div>
        <div class="form-group">
            <form:label path="numberPage" for="numberPage" class="numberPage">Nombre de page:</form:label>
            <form:input path="numberPage" type="int" id="numberPage" class="form-control"/>
            <small id="numberPageHelp" class="form-text text-muted">Veuillez indiquer ici un nombre de page:</small>
        </div>

        <button class="btn waves-effect waves-light" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
    </form:form>
    <br />
    <div>
        <a class="btn btn-primary hoverable" href="${contextPath} /">Page d'accueil</a>
        <a class="btn btn-primary hoverable" href="${contextPath} admin">Vers le tableau de gestion administrateur</a>
    </div>
</div>
</body>
</html>
