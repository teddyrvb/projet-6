<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>L'utilisateur et ses prêts</title>
</head>
<body>


<div class="container-fluid">
    <h3>Bonjour <c:out value="${currentUser.name}"/> et bienvenue</h3>
    <h4>Voici les livres que vous avez sélectionné pour ce mois-ci</h4>

    <div class="table-responsive rounded">
        <table class="table table-striped rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Date où vous avez emprunté ce livre</th>
                <th scope="col">Date d'expiration</th>
                <th scope="col">Vous avez atteint la date d'expiration</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${loans}" var="loan">
                <tr>
                    <td><c:out value="${loan.exemplary.book.title}"/></td>
                    <td><c:out value="${loan.startingDate}"/></td>
                    <td><c:out value="${loan.expiredDate}"/></td>
                    <td><c:if test="${loan.late}"><p>Vous avez du retard.</p></c:if>
                        <a class="btn btn-primary hoverable buttonList" href="prolongation?id=${loan.id}">prolonger</a>
                        <a class="btn btn-primary hoverable buttonList" href="restitution?id=${loan.id}&exemplaryId=${loan.exemplary.id}">restituer</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <h4>Liste des livres disponibles</h4>
    <div class="table-responsive rounded">
        <table class="table table-striped rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
                <th scope="col">Nombre de livres disponibles</th>
                <th scope="col">Prêt</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${availables}" var="available">
                <tr>
                    <td><c:out value="${available.book.title}"/></td>
                    <td><c:out value="${available.book.author}"/></td>
                    <td><c:out value="${available.book.numberPage}"/></td>
                    <td><c:out value="${available.quantity}" /></td>
                    <td><a class="btn btn-primary hoverable buttonList" href="emprunt?exemplaryId=${available.id}">emprunter</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <h4>Liste des livres non disponibles</h4>
    <div class="table-responsive rounded">
        <table class="table table-striped rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
                <th scope="col">Réservation</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${unavailables}" var="unavailable">
                <tr>
                    <td><c:out value="${unavailable.book.title}"/></td>
                    <td><c:out value="${unavailable.book.author}"/></td>
                    <td><c:out value="${unavailable.book.numberPage}"/></td>
                    <td><a class="btn btn-primary hoverable buttonList">Reserver</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>


</div>
</body>
</html>
