<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Affichage des livres après recherche</title>
</head>
<body>

<h1>Livre trouvé</h1>

<div class="container-fluid">
    <div class="table-responsive rounded">
        <table class="table table-striped rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${books}" var="book">
                <tr>
                    <td><c:out value="${book.book.title}"/></td>
                    <td><c:out value="${book.book.author}"/></td>
                    <td><c:out value="${book.book.numberPage}"/></td>
                    <td><c:out value="${book.quantity}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
