<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Route admin</title>
</head>
<body>

<div class="container-fluid">
    <h1>Ensemble des routes admin</h1>

    <a class="btn btn-primary hoverable" href='<c:url value="/adminUser" />'>Vers la gestion des utilisateurs</a>
    <a class="btn btn-primary hoverable" href='<c:url value="/adminBook" />'>Vers la gestion des livres</a>
</div>

</body>
</html>
