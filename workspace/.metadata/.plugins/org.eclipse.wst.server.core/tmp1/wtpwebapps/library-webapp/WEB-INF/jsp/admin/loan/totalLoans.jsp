<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <title>Ensemble des utilisateurs et de leurs prêts</title>
</head>
<body>

<h1>Ensemble des prêts</h1>

<p>Voici un tableau contenant l'ensemble des utilisateurs ainsi que leurs prêts</p>

<div class="container-fluid">
    <h1 class="text-center text-uppercase">Vos livres</h1>
    <p class="text-center">Vue de l'ensemble des livres que vous possédez:</p>
    <div class="table-responsive rounded">
        <table class="table table-striped rounded">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Nom de l'utilisateur ayant emprunté ce livre</th>
                <th scope="col">Nom du livre</th>
                <th scope="col">Date de l'emprunt</th>
                <th scope="col">Date butoire</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${loan}" var="loan">
                <tr>
                    <th scope="row">${loan.user.name}</th>
                    <td><c:out value="${loan.exemplary.book.title}"/></td>
                    <td><c:out value="${loan.startingDate}"/></td>
                    <td><c:out value="${loan.expiredDate}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div>
        <a href='<c:url value="/" />'>Page d'accueil</a>
    </div>
</div>

</body>
</html>
