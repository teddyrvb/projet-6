<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<c:url value='/static/css/adminBook.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>

    <title>affichage des livres depuis l'administration</title>
</head>
<body>
<div class="container-fluid">
    <h1 class="text-center text-uppercase">Vos livres</h1>
    <p class="text-center">Vue de l'ensemble des livres que vous possédez:</p>
    <div class="table-responsive rounded">
        <table class="table table-striped rounded">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
                <th scope="col">Vos interventions</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${books}" var="book">
                <tr>
                    <td><c:out value="${book.title}"/></td>
                    <td><c:out value="${book.author}"/></td>
                    <td><c:out value="${book.numberPage}"/></td>
                    <td>
                        <a class="btn yellow accent-4 hoverable sizeButton" href='<c:out value="/book/${book.id}" />'>Modifier un livre</a>
                        <a class="btn red darken-3 hoverable sizeButton" href='<c:out value="/deleteBook/${book.id}" />'>Supprimer un livre</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <p style="text-align:center">
            <a class="btn teal green darken-3" id="formLink" href='<c:url value="/addBook" />'>Ajouter un nouveau livre</a>
        </p>
    </div>
    <div>
        <a class="btn btn-primary hoverable" href='<c:url value="/" />'>Page d'accueil</a>
    </div>
</div>
</body>
</html>