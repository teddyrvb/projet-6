<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: world
  Date: 01/12/2018
  Time: 15:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Enregistrement</title>
    <link href="<c:url value='/static/css/form.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">
</head>
<body>

<div class="container-fluid">
    <form:form method="POST" modelAttribute="userAddForm">
        <div class="form-group">
            <form:label path="name" for="name" class="name">Nom</form:label>
            <form:input path="name" type="text" class="form-control" id="name" aria-describedby="nameHelp"/>
            <small id="nameHelp" class="form-text text-muted">Veuillez entrer ici le votre nom</small>
        </div>

        <div class="form-group">
            <form:label path="firstName" for="firstName" class="firstName">Prénom</form:label>
            <form:input path="firstName" type="text" class="form-control" id="firstName"
                        aria-describedby="fist_nameHelp"/>
            <small id="first_nameHelp" class="form-text text-muted">Veuillez entrer ici votre prénom</small>
        </div>

        <div class="form-group">
            <form:label path="adress" for="adress" class="adress">Adresse</form:label>
            <form:input path="adress" type="text" class="form-control" id="adress" aria-describedby="adressHelp"/>
            <small id="adressHelp" class="form-text text-muted">Veuillez entrer ici votre adresse</small>
        </div>

        <div class="form-group">
            <form:label path="phoneNumber" for="phoneNumber" class="phoneNumber">Numéro de téléphone</form:label>
            <form:input path="phoneNumber" type="int" class="form-control" id="phoneNumber"
                        aria-describedby="phoneNumberHelp"/>
            <small id="phone_numberHelp" class="form-text text-muted">Veuillez entrer ici votre numéro de téléphone
            </small>
        </div>

        <div class="form-group">
            <form:label path="emailAdress" for="emailAdress" class="emailAdress">Adresse mail</form:label>
            <form:input path="emailAdress" type="text" class="form-control" id="emailAdress"
                        aria-describedby="emailAdressHelp"/>
            <small id="email_adressHelp" class="form-text text-muted">Veuillez entrer ici votre adresse email</small>
        </div>
        <button class="btn waves-effect waves-light" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
    </form:form>
    <br />
    <div>
        <a href="${contextPath} /">Page d'accueil</a>
    </div>
</div>
</body>
</html>
