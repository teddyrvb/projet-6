<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page pageEncoding="UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<c:url value='/static/css/home.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>
    <title>menu principal</title>
</head>
<body>

<div class="container-fluid">
    <h1>Liste des livres présents dans la bibliothèque</h1>
    <br/>
    <h2>Liste des livres</h2>

    <div class="table-responsive rounded">
        <table class="table table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${books}" var="book">
                <tr>
                    <td><c:out value="${book.title}"/></td>
                    <td><c:out value="${book.author}"/></td>
                    <td><c:out value="${book.numberPage}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <h2>Liste des livres disponibles</h2>
    <div class="table-responsive rounded">
        <table class="table table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
                <th scope="col">Nombre de livres disponibles</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${availables}" var="available">
                <tr>
                    <td><c:out value="${available.book.title}"/></td>
                    <td><c:out value="${available.book.author}"/></td>
                    <td><c:out value="${available.book.numberPage}"/></td>
                    <td><c:out value="${available.quantity}" /></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <h2>Liste des livres non disponibles</h2>
    <div class="table-responsive rounded">
        <table class="table table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${unavailables}" var="unavailable">
                <tr>
                    <td><c:out value="${unavailable.book.title}"/></td>
                    <td><c:out value="${unavailable.book.author}"/></td>
                    <td><c:out value="${unavailable.book.numberPage}"/></td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>

    <div class="linkButton">
            <a class="btn btn-primary hoverable buttonList" style="color: white; text-underline:none" href='<c:url value="/register" />'>Pour vous enregistrer</a><br />
            <a class="btn btn-primary hoverable buttonList" style="color: white; text-underline:none" href='<c:url value="/loginPath" />'>Pour vous connecter</a><br />
            <a class="btn btn-primary hoverable buttonList"  style="color: white; text-underline:none" href='<c:url value="/displayLoan"/>'>Les utilisateurs et leurs emprunts</a><br />
            <a class="btn btn-primary hoverable buttonList"  style="color: white; text-underline:none" href='<c:url value="/adminPath" />'>Vers l'administration</a><br />
    </div>
</div>
</body>
</html>
