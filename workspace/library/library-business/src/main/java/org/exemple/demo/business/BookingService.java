package org.exemple.demo.business;

import org.exemple.demo.model.*;

import java.util.List;

public interface BookingService {
    List<Booking> findBookingByUser(User user);

    Booking insertNewBooking(User user, Book book);

    Booking findBookingById(int id);

    void removeBooking(int id);

    List<Booking> findAllBooking();

    void manageBooking(User user, Book book);

    List<Booking> startingDateForBooking(User user);

    void twoDayForDate(Booking booking);

    boolean dontAddBookingIfAlreadyPresent(User user, Book book);

    boolean dontAddBookingIfTheSameLoanExist(User user, Book book);

    List<Booking> findBookingByBook(Book book);

    int quantityOfLoawner(Book book);

    boolean toDoDisappearButtonAddIfLoanIsSaturated(User user, Book book);

    int retrievePositionInListOfUser(Booking booking);

    boolean sendMessageForTheFirstPositionIfLoanIsRestitued(Booking booking);
}
