package org.exemple.demo.business.impl;

import org.exemple.demo.business.UserService;
import org.exemple.demo.consumer.contract.dao.UserDao;
import org.exemple.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserDao userDao;


    @Override
    public List<User> findAllUsers(){

        return userDao.findAllUsers();
    }

    @Override
    public User insertNewUser(String name, String firstName, String address, String phoneNumber, String emailAddress) {

        return userDao.insertNewUser(name, firstName, address, phoneNumber, emailAddress);
    }

    @Override
    public User updateUser(User user, String name, String firstName, String address, String phoneNumber, String emailAddress) {

        return userDao.updateUser(user, name, firstName, address, phoneNumber, emailAddress);
    }

    @Override
    public User findUserById(int id){

        return userDao.findUserById(id);
    }

    @Override
    public void removeUser(int id){

        userDao.removeUser(id);
    }

    @Override
    public User login(String name, String emailAddress){

        return userDao.login(name, emailAddress);
    }

}
