package org.exemple.demo.business.impl;

import org.exemple.demo.business.BookingService;
import org.exemple.demo.business.LoanService;
import org.exemple.demo.consumer.contract.dao.BookingDao;
import org.exemple.demo.consumer.contract.dao.LoanDao;
import org.exemple.demo.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.List;


@Service
public class LoanServiceImpl implements LoanService {


    @Autowired
    private LoanDao loanDao;

    @Autowired
    private BookingDao bookingDao;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private WebMail webMail;



    @Override
    public List<Loan>findLoansByUser(User user) {

        return loanDao.findLoansByUser(user);
    }

    @Override
    public List<Loan> displayUserAndLoan(){

        return loanDao.displayUserAndLoan();
    }

    @Override
    public Loan findLoanById(int id) {

        return loanDao.findLoanById(id);
    }


    @Override
    public void prolonger(Loan loan){

        LocalDateTime today = LocalDateTime.now();

        LocalDateTime minus30day = today.minusSeconds(2592000);

        if(loan.getStartingDate().isAfter(minus30day)) {
            loanDao.prolonger(loan);
        }
    }

    @Override
    public boolean hideButtonIfUserClicOnButtonProlong(Loan loan){
        boolean clicOnExpiratedDate = false;

        LocalDateTime today = LocalDateTime.now();

        LocalDateTime minus30day = today.minusSeconds(2592000);

        if(loan.getStartingDate().isBefore(minus30day)){
            clicOnExpiratedDate = true;
        }

        return clicOnExpiratedDate;
    }

    @Override
    public List<Loan> findLoanOrderByExpiredDate() {
        return loanDao.findLoanOrderByExpiredDate();
    }

    @Override
    public void deleteLoan(Loan loan, Exemplary exemplary){
        loanDao.deleteLoan(loan);
    }

    @Override
    public List<Loan> displayAloneBookAvailable(){
        return loanDao.displayAloneBookAvailable();
    }

    @Override
    public  Loan insertNewLoan(User user, Exemplary exemplary, int isConfirm){

       return loanDao.insertNewLoan(user, exemplary, LocalDateTime.now(), LocalDateTime.now().plusWeeks(4), isConfirm);
    }

    ////////////////////////////////////////////////

    @Override
    public List<Loan> expiratedDate(User user){

        return loanDao.expiratedDate(user);
    }

    @Override
    public List<Loan> findLoanByExemplary(Exemplary exemplary_id){
        return loanDao.findLoanByExemplary(exemplary_id);
    }

    @Override
    public void toConfirm(Loan loan) {
        loanDao.toConfirm(loan);
    }

    @Override
    public void restituer(Loan loan){

        List<Booking> bookings = bookingDao.findBookingByBook(loan.getExemplary().getBook());
        int i = 0;
        boolean message = false;

        while(!message && i < bookings.size()){

            Booking booking1 = bookings.get(i);

            if(bookingService.retrievePositionInListOfUser(booking1) == 1){

                //loanDao.deleteLoan(loan);
                insertNewLoan(booking1.getUserId(), loan.getExemplary(), 1);
                webMail.sendMessage("Livre disponible", "Bonjour, le livre que vous avez réservé est " +
                        "désormais disponible, vous pouvez l'emprunter !", booking1.getUserId().getEmailAddress(),
                        booking1.getUserId().getEmailAddress());
                bookingDao.removeBooking(booking1.getID());
                message = true;
            }
            i++;
        }
        loanDao.deleteLoan(loan);
    }

    @Override
    public LocalDateTime findFirstDate(Book book) {

        LocalDateTime today = LocalDateTime.now().withNano(0);
        List<Loan> loans = loanDao.findLoanByBook(book);

        if(!loans.isEmpty()){
            today = loans.get(0).getExpiredDate();
        }

        return today;
    }

    public void ifTheExpiratedDateIsToday(Loan loan) {

        LocalDateTime today = LocalDateTime.now();

        if(loan.getExpiredDate() == today && loan.getConfirm() > 0) {
            loanDao.toConfirm(loan);
        }
    }

    @Override
    public void createATrueLoan(Loan loan){

        if(loan.getConfirm() == 1){
            loanDao.toConfirm(loan);
        }
    }

    @Override
    public List<Loan> findLoanWhereIsConfirmEqualOne(){
        return loanDao.findLoanWhereIsConfirmEqualOne();
    }


}
