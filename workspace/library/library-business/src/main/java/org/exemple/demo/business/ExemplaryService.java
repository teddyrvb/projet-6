package org.exemple.demo.business;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.User;

import java.util.List;

public interface ExemplaryService {


    List<Exemplary> readsAllExemplary();

    Exemplary insertNewExemplary();

    Exemplary findExemplaryById(int id);

    void removeExemplary(int id);

    List<Exemplary> findAllAvailableBooks();

    List<Book> findAllUnavailableBooks();

    List<Exemplary> findExemplaryByBook(Book book);

    Exemplary findExemplaryByBookIdNotInLoan(Book book);
}
