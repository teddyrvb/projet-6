package org.exemple.demo.business.impl;

import org.exemple.demo.business.ExemplaryService;
import org.exemple.demo.consumer.contract.dao.ExemplaryDao;
import org.exemple.demo.consumer.contract.dao.UserDao;
import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExemplaryServiceImpl implements ExemplaryService {


    @Autowired
    private ExemplaryDao exemplaryDao;

    @Autowired
    private UserDao userDao;


    @Override
    public List<Exemplary> readsAllExemplary(){
        return exemplaryDao.readsAllExemplary();
    }

    @Override
    public Exemplary insertNewExemplary(){
        return exemplaryDao.insertNewExemplary();
    }

    @Override
    public Exemplary findExemplaryById(int id){
        return exemplaryDao.findExemplaryById(id);
    }

    @Override
    public void removeExemplary(int id) {
        exemplaryDao.removeExemplary(id);
    }

    @Override
    public List<Exemplary> findAllAvailableBooks(){

        return exemplaryDao.findAllAvailableBooks();
    }

    @Override
    public List<Book> findAllUnavailableBooks() {

        return exemplaryDao.findAllUnavailableBooks();
    }

    @Override
    public List<Exemplary> findExemplaryByBook(Book book) {

        return exemplaryDao.findExemplaryByBook(book);
    }

    @Override
    public Exemplary findExemplaryByBookIdNotInLoan(Book book){
        return exemplaryDao.findExemplaryByBookIdNotInLoan(book.getId());
    }
}
