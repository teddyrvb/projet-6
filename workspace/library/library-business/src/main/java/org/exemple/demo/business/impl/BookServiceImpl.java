package org.exemple.demo.business.impl;

import org.exemple.demo.business.BookService;
import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.consumer.contract.dao.ExemplaryDao;
import org.exemple.demo.consumer.contract.dao.LoanDao;
import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {


    @Autowired
    private BookDao bookDao;

    @Autowired
    private ExemplaryDao exemplaryDao;

    @Autowired
    private LoanDao loanDao;


    @Override
    public List<Book> findAllBooks() {

        return bookDao.findAllBooks();
    }

    @Override
    public Book insertNewBook(String title, String author, int numberPage) {

        return bookDao.insertNewBook(title, author, numberPage);
    }

    @Override
    public Book updateBook(Book book, String title, String author, int numberPage){

        return bookDao.updateBook(book, title, author, numberPage);
    }

    @Override
    public void removeBook(int id){

        bookDao.removeBook(id);
    }

    @Override
    public Book findBookById(int id) {

       return bookDao.findBookById(id);
    }

    @Override
    public List<Book> retrieveAllBooks() {

        return bookDao.findAllBooks();
    }

    @Override
    public int countBook(Book book) {

        int quantity = 0;

        // recupérer la liste des exemplaires du bouquin
        // vérifier si un emprunt existe pour chaque exemplaire
        List<Exemplary> exemplary = exemplaryDao.findExemplaryByBook(book);
        List<Loan> loans = loanDao.findLoanByBook(book);

        quantity = exemplary.size() - loans.size();

        return quantity;
    }

    @Override
    public boolean dontAddBookWithExemplaryEqualToZero(Book book) {
        boolean quantityZero = false;

        if(countBook(book) == 0){
            quantityZero = true;
        }

        return quantityZero;
    }

    @Override
    public List<Book> retrieveAllBooksWhereBookingAreAvailable(){

        return bookDao.retrieveAllBooksWhereBookingAreAvailable();
    }

    public List<Book> findBook(String title){

        return bookDao.findBook(title);
    }
}
