package org.exemple.demo.business;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.Loan;
import org.exemple.demo.model.User;

import java.time.LocalDateTime;
import java.util.List;

public interface LoanService {
    List<Loan>findLoansByUser(User user);

    List<Loan> displayUserAndLoan();

    void prolonger(Loan loan);

    Loan findLoanById(int id);

    void deleteLoan(Loan loan, Exemplary exemplary);

    List<Loan> displayAloneBookAvailable();

    Loan insertNewLoan(User user, Exemplary exemplary, int isConfirm);

    List<Loan> expiratedDate(User user);

    List<Loan> findLoanByExemplary(Exemplary exemplary_id);

    void toConfirm(Loan loan);

    LocalDateTime findFirstDate(Book book);

    boolean hideButtonIfUserClicOnButtonProlong(Loan loan);

    List<Loan> findLoanOrderByExpiredDate();

    void restituer(Loan loan);

    void createATrueLoan(Loan loan);

    List<Loan> findLoanWhereIsConfirmEqualOne();
}
