package org.exemple.demo.business;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Booking;

import java.util.List;


public interface BookService {

    List<Book> findAllBooks();

    Book insertNewBook(String title, String author, int numberPage);

    Book updateBook(Book book, String title, String author, int numberPage);

    void removeBook(int id);

    Book findBookById(int id);

    List<Book> retrieveAllBooks();

    int countBook(Book book);

    boolean dontAddBookWithExemplaryEqualToZero(Book book);

    List<Book> retrieveAllBooksWhereBookingAreAvailable();

    List<Book> findBook(String title);
}
