package org.exemple.demo.business.impl;

import org.exemple.demo.business.BookingService;
import org.exemple.demo.consumer.contract.dao.*;
import org.exemple.demo.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    private BookingDao bookingDao;

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private ExemplaryDao exemplaryDao;


    @Override
    public List<Booking> findBookingByUser(User user) {

        return bookingDao.findBookingByUser(user);
    }

    @Override
    public Booking insertNewBooking(User user, Book book) {

        ZoneId zoneId = ZoneId.of("Europe/Paris");
        LocalDateTime localDateTime = LocalDateTime.now();
        ZonedDateTime zonedDateTime = localDateTime.atZone(zoneId);

        return bookingDao.insertNewBooking(user, zonedDateTime.toLocalDateTime(), book);
    }

    @Override
    public Booking findBookingById(int id) {

        return bookingDao.findBookingById(id);
    }

    @Override
    public void removeBooking(int id){

        bookingDao.removeBooking(id);
    }

    @Override
    public List<Booking> findAllBooking(){

        return bookingDao.findAllBooking();
    }

    @Override
    public void twoDayForDate(Booking booking){

        bookingDao.twoDayForDate(booking);
    }

    /* ----------------------------------------------------------- */
    /* Zone de gestion d'une réservation / Area to manage of a
     * booking */
    /* ----------------------------------------------------------- */

    @Override
    public boolean dontAddBookingIfAlreadyPresent(User user, Book book) {

        int i = 0;
        boolean counter = false;
        List<Booking> booking = findBookingByUser(user);

        /* permet de gérer le cas ou l'utilisateur cherche à rentrer une même réservation,
         * n'insère que si la réservation est unique */
        while (!counter && i < booking.size()) {
            Booking booking1 = booking.get(i);

            if(booking1.getBookId().getId() == book.getId()){
                counter = true;
            }
            i++;
        }
        return counter;
    }

    @Override
    public boolean dontAddBookingIfTheSameLoanExist(User user, Book book) {

        int i = 0;
        boolean counter = false;
        List<Loan> loan = loanDao.findLoansByUser(user);

        /* traverse les id des prêts, ne doit pas tomber
         * sur le même id sinon ajoute 1 */
        while (!counter && i < loan.size()) {
            Loan loan1 = loan.get(i);

            if(loan1.getExemplary().getBook().getId() == book.getId()/* && loan1.getConfirm() == 0*/) {
                counter = true;
            }
            i++;
        }
        return counter;
    }

    @Override
    public void manageBooking(User user, Book book) {

        /* Ici l'ensemble des variables compteurs pour le scan de validité' */
        boolean counter;
        boolean counterForLoan;

        counter = dontAddBookingIfAlreadyPresent(user, book);
        counterForLoan = dontAddBookingIfTheSameLoanExist(user, book);

        /* counterForAll correspond pour un même livre au nombre de fois qu'il est réservé */
        /* ce qui vient ensuite correspond, pour un même livre, à la quantité d'exemplaire */
        if (findBookingByBook(book).size() < (exemplaryDao.findExemplaryByBook(book).size() * 2)) {
            /* On vérifie que l'utilisateur ne fait pas deux fois la même réservation */
            if (!counter) {
                /* Si ajoute 1, l'insert ne se fait pas car emprunt déjà fait */
                if (!counterForLoan) {
                    insertNewBooking(user, book);
                }
            }
        }
    }

    @Override
    public boolean toDoDisappearButtonAddIfLoanIsSaturated(User user, Book book){

        boolean counter;
        boolean counterForLoan;
        boolean saturatedLoan = false;

        counter = dontAddBookingIfAlreadyPresent(user, book);
        counterForLoan = dontAddBookingIfTheSameLoanExist(user, book);

        if (findBookingByBook(book).size() < (exemplaryDao.findExemplaryByBook(book).size() * 2)) {
            if (!counter) {
                if (!counterForLoan) {
                    saturatedLoan = true;
                }
            }
        }
        return  saturatedLoan;
    }

    @Override
    public List<Booking> startingDateForBooking(User user){

        return bookingDao.startingDateForBooking(user);
    }



    @Override
    public List<Booking> findBookingByBook(Book book){
        return bookingDao.findBookingByBook(book);
    }

    public int quantityOfLoawner(Book book) {

        List<Booking> bookings = findBookingByBook(book);

        return bookings.size();
    }

    @Override
    public int retrievePositionInListOfUser(Booking booking){

        int i = 1;
        int positionUser = 0;
        for(Booking bookings : bookingDao.retrieveAllUserForTheSameBookOrderByDate(booking.getBookId())){

            if(bookings.getUserId().getId() == booking.getUserId().getId()){
                positionUser = i;
            }
            i++;
        }
        return positionUser;
    }

    @Override
    public boolean sendMessageForTheFirstPositionIfLoanIsRestitued(Booking booking){

        boolean message = false;
        int i = 0;
        List<Loan> loan = loanDao.displayUserAndLoan();

        while (!message && i < loan.size()) {
            Loan loan1 = loan.get(i);

            if (retrievePositionInListOfUser(booking) == 1) {
                if (booking.getBookId().getId() == loan1.getExemplary().getBook().getId()
                        && loan1.getConfirm() == 1) {
                    message = true;
                }
            }
            i++;
        }
        return message;
    }
}

