package org.exemple.demo.business.impl;

import org.exemple.demo.consumer.impl.dao.UserDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class WebMail {


    private static final String SMTP_HOST1 = "smtp.orange.fr";
 /*   private static final String LOGIN_SMTP1 = "";
    private static final String IMAP_ACCOUNT1 = "teddyrvb@gmail.com";
    private static final String PASSWORD_SMPT1 = "";*/

    public void sendMessage(String subject, String text, String destinataire, String copyDest) {
        // Création de la session
        Properties properties = new Properties();
/*        properties.setProperty("mail.transport.protocol", "smtp");*/
        properties.setProperty("mail.smtp.host", SMTP_HOST1);
      /*  properties.setProperty("mail.smtp.user", LOGIN_SMTP1);
        properties.setProperty("mail.from", IMAP_ACCOUNT1);*/
     /*   properties.setProperty("mail.smtp.starttls.enable", "true");*/
        Session session = Session.getInstance(properties);

        // Création du message
        MimeMessage message = new MimeMessage(session);
        try{
            message.setText(text);
            message.setSubject(subject);
            message.addRecipients(Message.RecipientType.TO, destinataire);
            message.addRecipients(Message.RecipientType.CC, copyDest);
            message.setFrom(new InternetAddress("Teddyrvb@gmail.com"));
        }catch (MessagingException e){
            e.printStackTrace();
        }

        // Envoi du message
        Transport transport;
        try {
          //  transport = session.getTransport("smtp");
        /*    transport.connect(LOGIN_SMTP1, PASSWORD_SMPT1);*/
            Transport.send(message /*, new Address[]{
                    new InternetAddress(destinataire),
                    new InternetAddress(copyDest)*/
          /*  }*/);
        } catch (MessagingException e){
            e.printStackTrace();
        }
    }
}
