package org.exemple.demo.business;

import org.exemple.demo.model.User;
import java.util.List;

public interface UserService {

    List<User> findAllUsers();

    User insertNewUser(String name, String firstName, String address, String phoneNumber, String emailAddress);

    User updateUser(User user, String name, String firstName, String address, String phoneNumber, String emailAddress);

    User findUserById(int id);

    void removeUser(int id);

    User login(String name, String emailAddress);
}
