package org.exemple.demo.business;

import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.model.Book;
import org.exemple.demo.model.Booking;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml"})
public class BookServiceImplTest {

    @Autowired
    BookDao bookDao;

    @Test
    public void verifyThatFindAllBookWork(){
        Assert.assertNotNull(bookDao.findAllBooks());
    }

    @Test
    public void verifyThatFindBookByIdWork(){

        try{
            Assert.assertNotNull(bookDao.findBookById(2));

            try{
                Book book = bookDao.findBookById(4);
                Assert.assertEquals(book.getAuthor(), "Pierre-Yves Cloux");
            } catch (ComparisonFailure e){
                System.out.println("module business");
                System.out.println("classe BookServiceImplTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }


        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }
    }
}
