package org.exemple.demo.business;

import org.exemple.demo.consumer.contract.dao.BookingDao;
import org.exemple.demo.consumer.contract.dao.UserDao;
import org.exemple.demo.model.Booking;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml"})
public class BookingServiceImplTest {

    @Autowired
    BookingService bookingService;

    @Autowired
    BookingDao bookingDao;

    @Autowired
    UserDao userDao;

    @Test
    public void verifyIfFindBookingByUserWork(){
        Assert.assertNotNull(bookingService.findBookingByUser(
                userDao.findUserById(2)
        ));
    }

    @Test
    public void verifyIfFindBookingByIdWork(){

        try{
            Assert.assertNotNull(bookingService.findBookingById(
                    bookingDao.findBookingById(2).getID()));

            Booking booking = bookingService.findBookingById(
                    bookingDao.findBookingById(2).getID());

            try{
                Assert.assertEquals(booking.getUserId().getName(), "Julie");
            }catch (ComparisonFailure e){
                System.out.println("module business");
                System.out.println("classe BookingServiceImplTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }
        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }



    }
}
