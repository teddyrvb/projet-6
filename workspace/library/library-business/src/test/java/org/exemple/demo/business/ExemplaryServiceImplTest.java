package org.exemple.demo.business;

import org.exemple.demo.consumer.contract.dao.ExemplaryDao;
import org.exemple.demo.model.Book;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml"})
public class ExemplaryServiceImplTest {

    @Autowired
    ExemplaryService exemplaryService;

    @Autowired
    ExemplaryDao exemplaryDao;

    @Test
    public void verifyIfReadsAllExemplaryWork(){

        Assert.assertNotNull(exemplaryService.readsAllExemplary());
    }

    @Test
    public void verifyIfFindExemplaryByIdWork(){

        try{
            Assert.assertNotNull(exemplaryService.findExemplaryById(2));

            try{
                Book book = exemplaryService.findExemplaryById(
                        exemplaryDao.findExemplaryById(2).getId()).getBook();
                Assert.assertEquals(book.getAuthor(), "Phil Szostak");

            } catch (ComparisonFailure e){
                System.out.println("module business");
                System.out.println("classe ExemplaryServiceImplTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }


        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }



    }
}
