package org.exemple.demo.business;

import org.exemple.demo.model.User;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml"})
public class UserServiceImplTest {


    @Autowired
    UserService userService;

    @Test
    public void verifyThatFindAllUsersWork(){
        Assert.assertNotNull(userService.findAllUsers());
    }

    @Test
    public void verifyThatFindUserByIdWork() {


        try{
            Assert.assertNotNull(userService.findUserById(2));

            try{
                User user = userService.findUserById(3);

                Assert.assertEquals(user.getAddress(), "2 rue du milieu");
            } catch (ComparisonFailure e){
                System.out.println("module business");
                System.out.println("classe UserServiceImplTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }

        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }
    }
}
