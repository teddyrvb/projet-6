package org.exemple.demo.webservice;

import org.exemple.demo.model.Booking;
import org.exemple.demo.model.Loan;

public class BookingAndLoanWS {

    private BookingWS bookingId;
    private LoanWS loanId;

    public BookingAndLoanWS(Booking booking, Loan loan) {
        this.bookingId = new BookingWS(booking);
        this.loanId = new LoanWS(loan);
    }


    public BookingWS getBookingId() {
        return bookingId;
    }

    public void setBookingId(BookingWS bookingId) {
        this.bookingId = bookingId;
    }

    public LoanWS getLoanId() {
        return loanId;
    }

    public void setLoanId(LoanWS loanId) {
        this.loanId = loanId;
    }
}
