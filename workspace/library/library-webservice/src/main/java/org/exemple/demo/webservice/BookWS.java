package org.exemple.demo.webservice;


import org.exemple.demo.model.Book;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class BookWS {


    private int id;
    private String title;
    private String author;
    private int numberPage;
    private int numberExemplaryAvailable;


    private LocalDateTime earliestDateForExemplaryId;
    private boolean trueForTheSameLoan;
    private boolean trueForTheSameBooking;
    private int numberBookForTheSameBooking;
    private boolean trueIfBookEqualZero;
    private boolean trueIfLoanIsSaturated;


    public BookWS(){}


    public BookWS(Book book) {
        this.id = book.getId();
        this.title = book.getTitle();
        this.author = book.getAuthor();
        this.numberPage = book.getNumberPage();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNumberPage() {
        return numberPage;
    }

    public void setNumberPage(int numberPage) {
        this.numberPage = numberPage;
    }

    public int getNumberExemplaryAvailable() {
        return numberExemplaryAvailable;
    }

    public void setNumberExemplaryAvailable(int numberExemplaryAvailable) {
        this.numberExemplaryAvailable = numberExemplaryAvailable;
    }

    public LocalDateTime getEarliestDateForExemplaryId() {
        return earliestDateForExemplaryId;
    }

    @XmlElement(name = "earliestDateForExemplaryId")
    @XmlJavaTypeAdapter(value = LocalDateXmlAdapter.class)
    public void setEarliestDateForExemplaryId(LocalDateTime earliestDateForExemplaryId) {
        this.earliestDateForExemplaryId = earliestDateForExemplaryId;
    }

    public boolean isTrueForTheSameLoan() {
        return trueForTheSameLoan;
    }

    public void setTrueForTheSameLoan(boolean trueForTheSameLoan) {
        this.trueForTheSameLoan = trueForTheSameLoan;
    }

    public boolean isTrueForTheSameBooking() {
        return trueForTheSameBooking;
    }

    public void setTrueForTheSameBooking(boolean trueForTheSameBooking) {
        this.trueForTheSameBooking = trueForTheSameBooking;
    }

    public int getNumberBookForTheSameBooking() {
        return numberBookForTheSameBooking;
    }

    public void setNumberBookForTheSameBooking(int numberBookForTheSameBooking) {
        this.numberBookForTheSameBooking = numberBookForTheSameBooking;
    }

    public boolean isTrueIfBookEqualZero() {
        return trueIfBookEqualZero;
    }

    public void setTrueIfBookEqualZero(boolean trueIfBookEqualZero) {
        this.trueIfBookEqualZero = trueIfBookEqualZero;
    }

    public boolean isTrueIfLoanIsSaturated() {
        return trueIfLoanIsSaturated;
    }

    public void setTrueIfLoanIsSaturated(boolean trueIfLoanIsSaturated) {
        this.trueIfLoanIsSaturated = trueIfLoanIsSaturated;
    }
}
