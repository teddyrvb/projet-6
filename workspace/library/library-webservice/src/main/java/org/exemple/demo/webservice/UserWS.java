package org.exemple.demo.webservice;

import org.exemple.demo.model.User;

public class UserWS {


    private int id;
    private String name;
    private String firstName;
    private String address;
    private String phoneNumber;
    private String emailAddress;


    public UserWS(){}


    public UserWS(User user){
        this.id = user.getId();
        this.name = user.getName();
        this.firstName = user.getFirstName();
        this.address = user.getAddress();
        this.phoneNumber = user.getPhoneNumber();
        this.emailAddress = user.getEmailAddress();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
