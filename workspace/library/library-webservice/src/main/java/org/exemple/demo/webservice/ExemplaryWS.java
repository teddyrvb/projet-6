package org.exemple.demo.webservice;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;

public class ExemplaryWS {


    private int id;

    private BookWS book;


    public ExemplaryWS(Exemplary exemplary) {
        this.id = exemplary.getId();
        this.book = new BookWS(exemplary.getBook());
    }


    public ExemplaryWS() {}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BookWS getBook() {
        return book;
    }

    public void setBook(BookWS book) {
        this.book = book;
    }
}
