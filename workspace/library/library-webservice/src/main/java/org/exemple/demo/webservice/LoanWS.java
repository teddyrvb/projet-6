package org.exemple.demo.webservice;

import org.exemple.demo.model.Loan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;

@XmlRootElement(name = "LoanWS")
@XmlAccessorType(XmlAccessType.FIELD)
public class LoanWS {


    private int id;
    @XmlElement(name = "startingDate")
    @XmlJavaTypeAdapter(value = LocalDateXmlAdapter.class)
    private LocalDateTime startingDate;

    @XmlElement(name = "expiredDate")
    @XmlJavaTypeAdapter(value = LocalDateXmlAdapter.class)
    private LocalDateTime expiredDate;

    // Deux attributs privates de type objet, ici un user et un exemplaire
    private UserWS user;
    private ExemplaryWS exemplary;
    private boolean late;
    private int isConfirm;

    private int forZeroExemplary;
    private boolean trueIfUserClicOnExpiratedDateButton;

    private int qtyOfBookingForOneBook;


    public LoanWS(){}


    public LoanWS(Loan loan){
        this.id = loan.getId();
        this.startingDate = loan.getStartingDate();
        this.expiredDate = loan.getExpiredDate();
        this.isConfirm = loan.getConfirm();
        this.user = new UserWS(loan.getUser());
        this.exemplary = new ExemplaryWS(loan.getExemplary());
        this.late = loan.getLate();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDateTime startingDate) {
        this.startingDate = startingDate;
    }

    public LocalDateTime getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(LocalDateTime expiredDate) {
        this.expiredDate = expiredDate;
    }

    public int getIsConfirm() {
        return isConfirm;
    }

    public void setIsConfirm(int isConfirm) {
        this.isConfirm = isConfirm;
    }

    public UserWS getUser() {
        return user;
    }

    public void setUser(UserWS user) {
        this.user = user;
    }

    public ExemplaryWS getExemplary() {
        return exemplary;
    }

    public void setExemplary(ExemplaryWS exemplary) {
        this.exemplary = exemplary;
    }

    public boolean isLate(){
        return expiredDate.isBefore(LocalDateTime.now());
    }

    public boolean getLate(){
        return this.isLate();
    }

    public int getForZeroExemplary() {
        return forZeroExemplary;
    }

    public void setForZeroExemplary(int forZeroExemplary) {
        this.forZeroExemplary = forZeroExemplary;
    }

    public int getQtyOfBookingForOneBook() {
        return qtyOfBookingForOneBook;
    }

    public void setQtyOfBookingForOneBook(int qtyOfBookingForOneBook) {
        this.qtyOfBookingForOneBook = qtyOfBookingForOneBook;
    }

    public boolean isTrueIfUserClicOnExpiratedDateButton() {
        return trueIfUserClicOnExpiratedDateButton;
    }

    public void setTrueIfUserClicOnExpiratedDateButton(boolean trueIfUserClicOnExpiratedDateButton) {
        this.trueIfUserClicOnExpiratedDateButton = trueIfUserClicOnExpiratedDateButton;
    }
}
