package org.exemple.demo.webservice;

import org.exemple.demo.model.Booking;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;


@XmlRootElement(name = "BookingWS")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingWS {


    @XmlElement(name = "bookingStartingDate")
    @XmlJavaTypeAdapter(value = LocalDateXmlAdapter.class)
    private LocalDateTime bookingStartingDate;


    private int ID;
    private UserWS userId;
    private BookWS bookId;

    private boolean isLate;

    private int positionOfUserInTheWaitingList;

    private boolean isConfirm;
    private boolean messageForValidate;

    private LocalDate expiratedBooking;


    public BookingWS(){}


    public BookingWS(Booking booking) {
        this.ID = booking.getID();
        this.userId = new UserWS(booking.getUserId());
        this.bookId = new BookWS(booking.getBookId());
        this.bookingStartingDate = booking.getBookingStartingDate();
        this.isLate = booking.isLate();
        this.isConfirm = isConfirm();
    }


    public LocalDateTime getBookingStartingDate() {
        return bookingStartingDate;
    }

    public void setBookingStartingDate(LocalDateTime bookingStartingDate) {
        this.bookingStartingDate = bookingStartingDate;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public UserWS getUserId() {
        return userId;
    }

    public void setUserId(UserWS userId) {
        this.userId = userId;
    }

    public BookWS getBookId() {
        return bookId;
    }

    public void setBookId(BookWS bookId) {
        this.bookId = bookId;
    }

    public boolean isLate() {
        return isLate;
    }

    public void setLate(boolean late) {
        isLate = late;
    }

    public boolean isConfirm() {
        return isConfirm;
    }

    public void setConfirm(boolean confirm) {
        isConfirm = confirm;
    }

    public int getPositionOfUserInTheWaitingList() {
        return positionOfUserInTheWaitingList;
    }

    public void setPositionOfUserInTheWaitingList(int positionOfUserInTheWaitingList) {
        this.positionOfUserInTheWaitingList = positionOfUserInTheWaitingList;
    }

    public boolean isMessageForValidate() {
        return messageForValidate;
    }

    public void setMessageForValidate(boolean messageForValidate) {
        this.messageForValidate = messageForValidate;
    }

}
