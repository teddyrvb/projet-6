package org.exemple.demo.webservice;


import org.exemple.demo.consumer.BookingImpl;
import org.exemple.demo.model.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/* cette annotation indique à l'environnement d'exécution du serveur d'exposer toute
 * les méthodes publiques sur ce bean sous forme d'un service web. */
/* cette annotation indique à l'environnement d'exécution du serveur d'exposer toute
 * les méthodes publiques sur ce bean sous forme d'un service web. */
@WebService
public class LibraryWS {


    /*-------------------------------------------------------------------------------------*/
    /* Liste des méthodes de livres / List of books methods */
    /*-------------------------------------------------------------------------------------*/

    /* Méthode pour retrouver tout les livres ainsi que les users */
    /* Cette méthode va ajouter à une arrayList vide l'ensemble des livres venant des services, qui vienne des dao, qui vienne de
     * la bdd */
    @WebMethod
    public List<BookWS> retrieveAllBook() {
        List<BookWS> bookWS = new ArrayList<>();
        for (Book book : LibraryWSSpring.INSTANCE.bookService.findAllBooks()) {

            BookWS bookWS1 = new BookWS(book);

            bookWS1.setTrueIfBookEqualZero(LibraryWSSpring.INSTANCE.bookService.dontAddBookWithExemplaryEqualToZero(book));

            bookWS1.setNumberExemplaryAvailable(LibraryWSSpring.INSTANCE.bookService.countBook(book));
            bookWS.add(bookWS1);
        }
        return bookWS;
    }

    @WebMethod
    public List<BookWS> retrieveAllBookAndUser(int id) {

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(id);

        List<BookWS> bookWS = new ArrayList<>();
        for (Book book : LibraryWSSpring.INSTANCE.bookService.retrieveAllBooksWhereBookingAreAvailable()) {

            BookWS bookWS1 = new BookWS(book);

            bookWS1.setTrueIfBookEqualZero(
                    LibraryWSSpring.INSTANCE.bookService.dontAddBookWithExemplaryEqualToZero(book));

            bookWS1.setNumberExemplaryAvailable(LibraryWSSpring.INSTANCE.bookService.countBook(book));

            bookWS1.setTrueIfLoanIsSaturated(
                    LibraryWSSpring.INSTANCE.bookingService.toDoDisappearButtonAddIfLoanIsSaturated(user, book));

            bookWS.add(bookWS1);
        }
        return bookWS;
    }

    @WebMethod
    public List<BookWS> retrieveAllBooksWhereBookingAreAvailable(int id) {

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(id);

        List<BookWS> bookWS = new ArrayList<>();
        for (Book book : LibraryWSSpring.INSTANCE.bookService.retrieveAllBooksWhereBookingAreAvailable()) {
            BookWS bookWS1 = new BookWS(book);

            // date la plus proche et nombre de personne qui ont réservées.
            bookWS1.setEarliestDateForExemplaryId(
                    LibraryWSSpring.INSTANCE.loanService.findFirstDate(book));

            bookWS1.setTrueForTheSameBooking(
                    LibraryWSSpring.INSTANCE.bookingService.dontAddBookingIfAlreadyPresent(user, book));

            bookWS1.setTrueForTheSameLoan(
                    LibraryWSSpring.INSTANCE.bookingService.dontAddBookingIfTheSameLoanExist(user, book));

            bookWS1.setNumberBookForTheSameBooking(
                    LibraryWSSpring.INSTANCE.bookingService.findBookingByBook(book).size());

            bookWS1.setTrueIfLoanIsSaturated(
                    LibraryWSSpring.INSTANCE.bookingService.toDoDisappearButtonAddIfLoanIsSaturated(user, book));

            bookWS.add(bookWS1);
        }
        return bookWS;
    }

    @WebMethod
    public BookWS findBookById(int id) {
        Book book = LibraryWSSpring.INSTANCE.bookService.findBookById(id);
        return new BookWS(book);
    }

    @WebMethod
    public BookWS insertNewBook(String title, String author, int numberPage){
        Book book = LibraryWSSpring.INSTANCE.bookService.insertNewBook(title, author, numberPage);
        return new BookWS(book);
    }

    @WebMethod
    public BookWS updateBook(BookWS bookWS,String title, String author, int numberPage){
        Book book = LibraryWSSpring.INSTANCE.bookService.findBookById(bookWS.getId());
        LibraryWSSpring.INSTANCE.bookService.updateBook(book,title,author, numberPage);

        return bookWS;
    }

    /*-------------------------------------------------------------------------------------*/
    /* Liste des méthodes des utilisateurs / List of users methods */
    /*-------------------------------------------------------------------------------------*/

    @WebMethod
    public List<UserWS> retrieveAllUser() {
        List<UserWS> userWS = new ArrayList<>();
        for (User user : LibraryWSSpring.INSTANCE.userService.findAllUsers()) {
            userWS.add(new UserWS(user));
        }
        return userWS;
    }

    @WebMethod
    public UserWS findUserById(int id) {
        User user = LibraryWSSpring.INSTANCE.userService.findUserById(id);

        return new UserWS(user);
    }

    @WebMethod
    public UserWS login(String nameWS, String emailAddressWS){

        User u = LibraryWSSpring.INSTANCE.userService.login(nameWS, emailAddressWS);

        if(u == null) {
            return null;
        }

        return new UserWS(u);
    }

    @WebMethod
    public UserWS insertNewUser(String name, String firstName, String address, String phoneNumber, String emailAddress){

        User user = LibraryWSSpring.INSTANCE.userService.insertNewUser(name, firstName, address, phoneNumber, emailAddress);

        return new UserWS(user);
    }

    @WebMethod
    public UserWS updateUser(UserWS userWS, String name, String firstName, String address, String phoneNumber, String emailAddress){

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(userWS.getId());
        LibraryWSSpring.INSTANCE.userService.updateUser(user, name, firstName, address, phoneNumber, emailAddress);

        return userWS;
    }

    @WebMethod
    public void removeUser(int id) {
        LibraryWSSpring.INSTANCE.userService.removeUser(id);
    }

    /*-------------------------------------------------------------------------------------*/
    /* Liste des méthodes des exemplaires / List of examplarys methods */
    /*-------------------------------------------------------------------------------------*/

    @WebMethod
    public List<ExemplaryWS> retrieveAllExemplary() {

        List<ExemplaryWS> exemplaryWS = new ArrayList<>();
        for (Exemplary exemplary : LibraryWSSpring.INSTANCE.exemplaryService.readsAllExemplary()) {
            exemplaryWS.add(new ExemplaryWS(exemplary));
        }

        return exemplaryWS;
    }

    @WebMethod
    public ExemplaryWS findExemplaryById(int id) {

        Exemplary exemplary = LibraryWSSpring.INSTANCE.exemplaryService.findExemplaryById(id);

        return new ExemplaryWS(exemplary);
    }

    @WebMethod
    public List<BookWS> findBook(String titleWS, UserWS userWS) {

        List<BookWS> bookWS = new ArrayList<>();
        User user = LibraryWSSpring.INSTANCE.userService.findUserById(userWS.getId());

        for (Book book : LibraryWSSpring.INSTANCE.bookService.findBook(titleWS)) {

            BookWS bookWS1 = new BookWS(book);

            // nombre d'exemplaire dispo.
            bookWS1.setNumberExemplaryAvailable(LibraryWSSpring.INSTANCE.bookService.countBook(book));

            // définit la date de retour la plus proche
            bookWS1.setEarliestDateForExemplaryId(LibraryWSSpring.INSTANCE.loanService.findFirstDate(book));

            // définit le nombre de personne ayant réservé l'ouvrage
            bookWS1.setNumberBookForTheSameBooking(
                    LibraryWSSpring.INSTANCE.bookingService.findBookingByBook(book).size());

            // règle de gestion sur une réservation
            bookWS1.setTrueIfLoanIsSaturated(
                    LibraryWSSpring.INSTANCE.bookingService.toDoDisappearButtonAddIfLoanIsSaturated(user, book));

            bookWS.add(bookWS1);
        }

        return bookWS;
    }

    @WebMethod
    public List<ExemplaryWS> findAllAvailableBooks() {

        List<ExemplaryWS> exemplaryWS = new ArrayList<>();
        for (Exemplary exemplary : LibraryWSSpring.INSTANCE.exemplaryService.findAllAvailableBooks()){
            exemplaryWS.add(new ExemplaryWS(exemplary));
        }
        return exemplaryWS;
    }

    @WebMethod
    public List<BookWS> findAllUnavailableBooks() {

        List<BookWS> bookWS = new ArrayList<>();

        for (Book book : LibraryWSSpring.INSTANCE.exemplaryService.findAllUnavailableBooks()){
            bookWS.add(new BookWS(book));
        }
        return bookWS;
    }

    @WebMethod
    public void removeBook(int id){
        LibraryWSSpring.INSTANCE.bookService.removeBook(id);
    }

    /*-------------------------------------------------------------------------------------*/
    /* Liste des méthodes des prêts / List of loans methods */
    /*-------------------------------------------------------------------------------------*/

    @WebMethod
    public List<LoanWS> findAllUserAndThemLoan() {

        List<LoanWS> loanWS = new ArrayList<>();

        for (User user : LibraryWSSpring.INSTANCE.userService.findAllUsers()) {
            for (Loan loan : LibraryWSSpring.INSTANCE.loanService.findLoansByUser(user)) {
                loanWS.add(new LoanWS(loan));
            }
        }

        return loanWS;
    }

    @WebMethod
    public LoanWS findLoanById(int id) {

        Loan loan = LibraryWSSpring.INSTANCE.loanService.findLoanById(id);

        return new LoanWS(loan);
    }

    /* --------------------------------------------------  */
    @WebMethod
    public void insertNewLoan(UserWS userWS, ExemplaryWS exemplaryWS, int isConfirm) {

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(userWS.getId());
        Exemplary exemplary = LibraryWSSpring.INSTANCE.exemplaryService.findExemplaryById(exemplaryWS.getId());
        LibraryWSSpring.INSTANCE.loanService.insertNewLoan(user, exemplary, isConfirm);
    }

    @WebMethod
    public void deleteLoan(LoanWS loanWS, ExemplaryWS exemplaryWS) {

        Loan loan = LibraryWSSpring.INSTANCE.loanService.findLoanById(loanWS.getId());
        Exemplary exemplary = LibraryWSSpring.INSTANCE.exemplaryService.findExemplaryById(exemplaryWS.getId());
        LibraryWSSpring.INSTANCE.loanService.deleteLoan(loan, exemplary);
    }

    @WebMethod
    public void prolonger(LoanWS loanWS){

        Loan loan = LibraryWSSpring.INSTANCE.loanService.findLoanById(loanWS.getId());
        LibraryWSSpring.INSTANCE.loanService.prolonger(loan);
    }


    @WebMethod
    public List<LoanWS> findLoanByUser(int id) {

        List<LoanWS> loanWS = new ArrayList<>();

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(id);
        for (Loan loan : LibraryWSSpring.INSTANCE.loanService.findLoansByUser(user)){

            LoanWS loanWS1 = new LoanWS(loan);
            loanWS1.setTrueIfUserClicOnExpiratedDateButton(
                    LibraryWSSpring.INSTANCE.loanService.hideButtonIfUserClicOnButtonProlong(loan));

            System.out.println(loanWS1.isTrueIfUserClicOnExpiratedDateButton());

            loanWS.add(loanWS1);
        }

        return loanWS;
    }

    @WebMethod
    public List<LoanWS> expiratedDate(UserWS userws){

        List<LoanWS>loanWS = new ArrayList<>();

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(userws.getId());
        for (Loan loan : LibraryWSSpring.INSTANCE.loanService.expiratedDate(user)){
            loanWS.add(new LoanWS(loan));
        }

        return loanWS;
    }

    @WebMethod
    public List<LoanWS> displayUserAndLoan() {

        List<LoanWS> loanWS = new ArrayList<>();

        for (Loan loan : LibraryWSSpring.INSTANCE.loanService.displayUserAndLoan()){

            LoanWS loanWS1 = new LoanWS(loan);

            //va compter le nombre d'exemplaire d'un livre
            loanWS1.setForZeroExemplary(LibraryWSSpring.INSTANCE.bookService.countBook(
                    loan.getExemplary().getBook()
            ));

            // affiche le nombre de personne qui ont réservé.
            loanWS1.setQtyOfBookingForOneBook(
                    LibraryWSSpring.INSTANCE.bookingService.quantityOfLoawner(
                            loan.getExemplary().getBook()
                    ));

            loanWS.add(loanWS1);
        }

        return loanWS;
    }


    /*-------------------------------------------------------------------------------------*/
    /* Liste des méthodes des réservation / List of booking methods */
    /*-------------------------------------------------------------------------------------*/

    /* retourne l'ensemble des réservation pour l'utilisateur / return all booking for user */
    @WebMethod
    public List<BookingWS> findBookingByUser(UserWS userWS) {

        List<BookingWS> bookingWS = new ArrayList<>();

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(userWS.getId());

        for (Booking booking : LibraryWSSpring.INSTANCE.bookingService.findBookingByUser(user)){
            BookingWS bookingWS1 = new BookingWS(booking);

            bookingWS1.setPositionOfUserInTheWaitingList(LibraryWSSpring.INSTANCE.bookingService.retrievePositionInListOfUser(booking));

            bookingWS1.setMessageForValidate(
                    LibraryWSSpring.INSTANCE.bookingService.sendMessageForTheFirstPositionIfLoanIsRestitued(booking));

            bookingWS.add(bookingWS1);
        }
        LibraryWSSpring.INSTANCE.bookingService.findBookingByUser(user);

        return bookingWS;
    }

    @WebMethod
    public int  retrievePositionInListOfUser(BookingWS bookingws){

        Booking booking1 = LibraryWSSpring.INSTANCE.bookingService.findBookingById(bookingws.getID());
        int positionUser = LibraryWSSpring.INSTANCE.bookingService.retrievePositionInListOfUser(booking1);

        return positionUser;
    }

    /* insère en base une nouvelle réservation */
    @WebMethod
    public void insertNewBooking(UserWS userWS, BookWS bookWS) {

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(userWS.getId());
        Book book = LibraryWSSpring.INSTANCE.bookService.findBookById(bookWS.getId());
        LibraryWSSpring.INSTANCE.bookingService.insertNewBooking(user, book);
    }

    @WebMethod
    public BookingWS findBookingById(int id) {
        Booking booking = LibraryWSSpring.INSTANCE.bookingService.findBookingById(id);

        return new BookingWS(booking);
    }

    @WebMethod
    public void removeBooking(int id) {

        LibraryWSSpring.INSTANCE.bookingService.removeBooking(id);
    }

    /* retourne l'ensemble des réservations */
    @WebMethod
    public List<BookingWS> findAllBooking() {

        List<BookingWS> bookingWS = new ArrayList<>();

        for (Booking booking : LibraryWSSpring.INSTANCE.bookingService.findAllBooking() ){
            bookingWS.add(new BookingWS(booking));
        }

        return bookingWS;
    }

    @WebMethod
    public void methodToManageBooking(UserWS userWS, BookWS bookWS) {

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(userWS.getId());
        Book book = LibraryWSSpring.INSTANCE.bookService.findBookById(bookWS.getId());

        LibraryWSSpring.INSTANCE.bookingService.manageBooking(user, book);
    }


    @WebMethod
    public List<BookingWS> startingDateForBooking(UserWS userWS) {

        List<BookingWS> bookingWS = new ArrayList<>();

        User user = LibraryWSSpring.INSTANCE.userService.findUserById(userWS.getId());

        for (Booking booking : LibraryWSSpring.INSTANCE.bookingService.startingDateForBooking(user)){
            bookingWS.add(new BookingWS(booking));
        }

        return bookingWS;
    }

    @WebMethod
    public List<LoanWS> findLoanWhereIsConfirmEqualOne(){

        List<LoanWS> loanWS = new ArrayList<>();

        for (Loan loan: LibraryWSSpring.INSTANCE.loanService.findLoanWhereIsConfirmEqualOne()){

            LoanWS loanWS1 = new LoanWS(loan);
            loanWS.add(loanWS1);
        }
        return loanWS;
    }

    @WebMethod
    public void restituer(LoanWS loanWS){

        Loan loan = LibraryWSSpring.INSTANCE.loanService.findLoanById(loanWS.getId());
        LibraryWSSpring.INSTANCE.loanService.restituer(loan);
    }

    /* Méthode à vérifier.... */
    public void methodToDeleteBookingWhenTheDateIsExpired(LoanWS loanWS, UserWS userWS) {

        LocalDateTime dateNow = LocalDateTime.now();
        LocalDateTime datePlusTwoDay = dateNow.plusDays(2);

        Loan loan = LibraryWSSpring.INSTANCE.loanService.findLoanById(loanWS.getId());
        User user = LibraryWSSpring.INSTANCE.userService.findUserById(userWS.getId());

        for (Booking booking : LibraryWSSpring.INSTANCE.bookingService.findBookingByUser(user)) {


            /* permet de supprimer une réservation */
            /* ---- doit en principe se déclencher dès lors qu'un livre est disponible  ----- */
            if(loan.getExpiredDate() == datePlusTwoDay) {
                removeBooking(booking.getID());
            }
        }
    }

    @WebMethod
    public void createATrueLoan(LoanWS loanWS) {

        Loan loan = LibraryWSSpring.INSTANCE.loanService.findLoanById(loanWS.getId());

        LibraryWSSpring.INSTANCE.loanService.createATrueLoan(loan);
    }

    public ExemplaryWS findExemplaryNotInLoan(BookWS bookWS){

        Book book = LibraryWSSpring.INSTANCE.bookService.findBookById(bookWS.getId());

        Exemplary exemplary = LibraryWSSpring.INSTANCE.exemplaryService.findExemplaryByBookIdNotInLoan(book);

        ExemplaryWS exemplaryWS = new ExemplaryWS(exemplary);

        return exemplaryWS;
    }
}
