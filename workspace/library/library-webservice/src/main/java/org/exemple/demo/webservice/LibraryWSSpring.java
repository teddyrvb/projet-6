package org.exemple.demo.webservice;

import org.exemple.demo.business.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LibraryWSSpring {

    public static LibraryWSSpring INSTANCE;

    @Autowired
    public BookService bookService;
    @Autowired
    public LoanService loanService;
    @Autowired
    public ExemplaryService exemplaryService;
    @Autowired
    public UserService userService;
    @Autowired
    public BookingService bookingService;


    public LibraryWSSpring() {
        INSTANCE = this;
    }
}
