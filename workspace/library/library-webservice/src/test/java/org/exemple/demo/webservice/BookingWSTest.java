package org.exemple.demo.webservice;

import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml",
        "classpath:/org/exemple/demo/webservice/applicationContext.xml"})
public class BookingWSTest {


    @Autowired
    LibraryWS libraryWs;

    @Autowired
    BookingWS bookingWS;

    @Test
    public void test(){}


    @Test
    public void testForBookingWS(){

        BookingWS bookingWS = new BookingWS();
        UserWS userWS = new UserWS();

        userWS.setId(2);
        userWS.setAddress("test en mock");
        bookingWS.setUserId(userWS);

        Assert.assertEquals(bookingWS.getUserId().getAddress(), "test en mock");


        try{
            BookingWS bookingWSBDD = libraryWs.findBookingById(2);

            try{
                Assert.assertEquals(bookingWSBDD.getUserId().getAddress(), "4 rue des palmiers");
            }catch (ComparisonFailure e){
                System.out.println("module webservice");
                System.out.println("classe BookingWSTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }
        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }
    }
}
