package org.exemple.demo.webservice;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Booking;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml",
        "classpath:/org/exemple/demo/webservice/applicationContext.xml"})
public class LibraryWSSpringTest {


    @Autowired
    LibraryWSSpring libraryWSSpring;


    @Test
    public void verifyExampleWork(){

        try{
            Assert.assertNotNull(libraryWSSpring.bookingService);

            String book = libraryWSSpring.bookingService.findBookingById(2).getBookId().getAuthor();

            try{
                Assert.assertEquals(book, "Béatrice Quoniam");
            }catch (ComparisonFailure e){
                System.out.println("module webservice");
                System.out.println("classe LibraryWSSpringTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }
        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }
    }
}
