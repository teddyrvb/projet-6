package org.exemple.demo.webservice;

import org.exemple.demo.model.User;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml",
        "classpath:/org/exemple/demo/webservice/applicationContext.xml"})
public class UserWSTest {


    @Autowired
    UserWS userWS;

    @Autowired
    LibraryWS libraryWS;


    @Test
    public void verifyUserWS(){

        UserWS userWS = new UserWS();

        UserWS user = libraryWS.findUserById(2);
        userWS.setAddress(user.getAddress());

        Assert.assertNotNull(userWS.getAddress());



        try{
            // Dans un premier temps un test qui se sert de la base de donnée
            try{
                Assert.assertEquals(userWS.getAddress(), "4 rue de la ferme");
            }catch (ComparisonFailure e){
                System.out.println("module webservice");
                System.out.println("classe UserWSTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }

            UserWS userWSMock = new UserWS();
            userWSMock.setAddress("test en mock");

            // Ici un test qui se sert d'un mock
            Assert.assertEquals(userWSMock.getAddress(), "test en mock");
        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }
    }


}
