package org.exemple.demo.webservice;

import org.exemple.demo.model.Exemplary;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml",
        "classpath:/org/exemple/demo/webservice/applicationContext.xml"})
public class ExemplaryWSTest {


    @Autowired
    LibraryWS libraryWS;

    @Autowired
    ExemplaryWS exemplaryWS;

    @Test
    public void testForExemplaryWS(){

        ExemplaryWS exemplaryWS = new ExemplaryWS();
        BookWS bookWS = new BookWS();

        exemplaryWS.setBook(bookWS);
        bookWS.setId(1);
        bookWS.setAuthor("nom en mock");

        Assert.assertEquals(exemplaryWS.getBook().getAuthor(), "nom en mock");

        try{
            ExemplaryWS exemplary = libraryWS.findExemplaryById(4);

            try{
                Assert.assertEquals(exemplary.getBook().getAuthor(), "Phil Szostak");
            }catch (ComparisonFailure e){
                System.out.println("module webservice");
                System.out.println("classe ExemplaryWSTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }
        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }
    }
}
