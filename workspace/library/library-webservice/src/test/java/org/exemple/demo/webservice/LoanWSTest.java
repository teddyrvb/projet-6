package org.exemple.demo.webservice;

import org.exemple.demo.model.Exemplary;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml",
        "classpath:/org/exemple/demo/webservice/applicationContext.xml"})
public class LoanWSTest {


    @Autowired
    LibraryWS libraryWS;

    @Autowired
    LoanWS loanWS;


    @Test
    public void testForLoansWS(){

        LoanWS loanWS = new LoanWS();
        BookWS bookWS = new BookWS();
        ExemplaryWS exemplaryWS = new ExemplaryWS();

        bookWS.setId(1);
        exemplaryWS.setId(1);
        bookWS.setAuthor("test en mock");
        loanWS.setExemplary(exemplaryWS);
        exemplaryWS.setBook(bookWS);

        Assert.assertEquals(loanWS.getExemplary().getBook().getAuthor(), "test en mock");

        try{
            LoanWS loanWS1 = libraryWS.findLoanById(3);

            try{
                Assert.assertEquals(loanWS1.getExemplary().getBook().getAuthor(), "Philip Escartin");
            } catch (ComparisonFailure e){
                System.out.println("module webservice");
                System.out.println("classe LoanWSTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }
        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }
    }
}
