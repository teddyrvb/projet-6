package org.exemple.demo.webservice;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml",
        "classpath:/org/exemple/demo/webservice/applicationContext.xml"})
public class LocalDateXmlAdapterTest {


    @Autowired
    LocalDateXmlAdapter localDateXmlAdapter;


    public LocalDateTime parseLocalDateTime(String dateToParse){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        return LocalDateTime.parse(dateToParse, formatter);
    }

    // sur ces tests, on vérifie que les conversions sont effectuées correctement
    @Test
    public void verifyThatUnmarshalWork() throws Exception {

        String str = "2020-03-04T05:30:22";
        String strForParser = "2020-03-04 05:30:22";
        LocalDateTime test = localDateXmlAdapter.unmarshal(str);
        Assert.assertNotNull(test);
        Assert.assertEquals(test, parseLocalDateTime(strForParser));

    }

    @Test
    public void verifyThatMarshalWork() throws Exception {
        String str = "2020-03-04 05:30:22";
        LocalDateTime test = parseLocalDateTime(str);
        Assert.assertNotNull(localDateXmlAdapter.marshal(test));

        String strForParser = "2020-03-04 05:30:22";
        LocalDateTime parser = parseLocalDateTime(strForParser);

        Assert.assertEquals(parser, test);
    }
}
