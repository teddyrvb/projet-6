package org.exemple.demo.webservice;

import org.exemple.demo.model.Book;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml",
        "classpath:/org/exemple/demo/webservice/applicationContext.xml"})
public class LibraryWSTest {


    @Autowired
    LibraryWSSpring libraryWSSpring;

    @Autowired
    LibraryWS libraryWS;


    @Test
    public void verifyThatFindAllBookingWork() {

        Assert.assertNotNull(libraryWS.findAllBooking());
    }

    @Test
    public void verifyThatFindBookByIdWork(){

        try{
            Assert.assertNotNull(libraryWS.findBookById(
                    libraryWSSpring.bookService.findBookById(2).getId()
            ));
            try{
                BookWS book = libraryWS.findBookById(
                        libraryWSSpring.bookService.findBookById(2).getId());

                Assert.assertEquals(book.getAuthor(), "J.R.R. Tolkien");
            }catch (ComparisonFailure e){
                System.out.println("module webservice");
                System.out.println("classe LibraryWSTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }

        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }
    }
}
