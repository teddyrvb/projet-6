package org.exemple.demo.webservice;

import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/business/applicationContext.xml",
        "classpath:/org/exemple/demo/webservice/applicationContext.xml"})
public class BookWSTest {


    @Autowired
    LibraryWS libraryWS;

    @Autowired
    BookWS bookWS;

    @Test
    public void test(){
        BookWS bookWS = new BookWS();
        bookWS.setTitle("titre en mock");

        Assert.assertEquals(bookWS.getTitle(), "titre en mock");


        try{
            BookWS bookWSBDD = libraryWS.findBookById(5);

            try{
                Assert.assertEquals(bookWSBDD.getAuthor(), "Margot Zhang");
            }catch (ComparisonFailure e){
                System.out.println("module webservice");
                System.out.println("classe BookWSTest");
                System.out.println("Jeu de donnée altéré ou test incorrect.");
                System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
            }
        }catch (EmptyResultDataAccessException e){
            System.out.println("Information en base non disponible");
        }

    }
}
