package org.exemple.demo.batch;


import org.exemple.demo.webservice.*;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        /* interroger le loanservice à travers soap pour récupérer la liste des loans en retard*/
        /* boucle for */

        LibraryWSService factory = new LibraryWSService();
        LibraryWS service = factory.getLibraryWSPort();


        LocalDateTime dateOfday = LocalDateTime.now();

        for (LoanWS loanWS : service.findLoanWhereIsConfirmEqualOne()) {

            String convertBookingStartingDateToLocalDateTime = loanWS.getStartingDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
            LocalDateTime bookingStartingDate = LocalDateTime.parse(convertBookingStartingDateToLocalDateTime, formatter);

            if (bookingStartingDate.compareTo(LocalDateTime.now().minusDays(2)) <= 0) {

                // dans un premier temps, un message afin de voir si le résultat est le bon...
                System.out.println("L'utilisateur " + loanWS.getUser().getFirstName() + " " + loanWS.getUser().getName() +
                        " aurait du réserver le livre " + loanWS.getExemplary().getBook().getTitle() +
                        " avant 48 heures. Pour cette raison," +
                        " cette réservation sera supprimé sur le champ.");

                // suppression des emprunt et création des nouveaux temporaire pour les prochains de la liste
                service.restituer(loanWS);
            }
        }

   /*     for (UserWS userWS : service.retrieveAllUser()) {
            for (LoanWS loanWS : service.expiratedDate(userWS))
                System.out.println("L'utilisateur " + loanWS.getUser().getFirstName() + " " + loanWS.getUser().getName() + " est en retard et doit " +
                        "rendre le livre qu'il a emprunté, nous sommes le " + dateOfday + " et ce livre aurait dû être rendu le " +
                        loanWS.getExpiredDate() + ".");
            }
        }*/
    }

}
