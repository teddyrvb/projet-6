package org.exemple.demo.model;

import java.time.LocalDateTime;

public interface Loan {

    int getId();
    void setId(int value);

    User getUser();
    void setUser(User value);

    Exemplary getExemplary();
    void setExemplary(Exemplary value);

    LocalDateTime getStartingDate();
    void setStartingDate(LocalDateTime value);

    LocalDateTime getExpiredDate();
    void setExpiredDate(LocalDateTime value);

    boolean isLate();
    boolean getLate();

    void prolonger();

    int getConfirm();
    void isConfirm(int isConfirm);
}
