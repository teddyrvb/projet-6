package org.exemple.demo.model;


public interface Login {

    String getName();
    void setName(String value);

    String getEmailAddress();
    void setEmailAddress(String value);
}
