package org.exemple.demo.model;



import java.util.Date;


public interface Book {

    /* =============================================================================== */
    /* ==== signature des méthodes en relations avec BookImpl /
     * signature of methods in relation with BookImpl ==== */
    /* =============================================================================== */
    int getId();
    void setId(int id);

    String getTitle();
    void setTitle(String title);

    String getAuthor();
    void setAuthor(String author);

    int getNumberPage();
    void setNumberPage(int numberPage);
}
