package org.exemple.demo.model;

public interface Exemplary {

    int getId();
    void setId(int id);

    Book getBook();
    void setBook(Book book);
}
