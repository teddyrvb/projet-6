package org.exemple.demo.model;


import java.time.LocalDateTime;

public interface Booking {

    int getID();
    void setID(int ID);

    LocalDateTime getBookingStartingDate();
    void setBookingStartingDate(LocalDateTime startingDate);

    Book getBookId();
    void setBookId(Book bookId);

    User getUserId();
    void setUserId(User userId);

    boolean isLate();
    void setLate(boolean late);

    int getLocation();
    void setLocation(int location);

}
