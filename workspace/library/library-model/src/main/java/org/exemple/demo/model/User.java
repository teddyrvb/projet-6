package org.exemple.demo.model;


public interface User {

    int getId();
    void setId(int value);

    String getName();
    void setName(String value);

    String getFirstName();
    void setFirstName(String value);

    String getAddress();
    void setAddress(String value);

    String getPhoneNumber();
    void setPhoneNumber(String value);

    String getEmailAddress();
    void setEmailAddress(String value);
}
