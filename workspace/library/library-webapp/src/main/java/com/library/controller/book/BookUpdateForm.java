package com.library.controller.book;


import org.exemple.demo.webservice.BookWS;

public class BookUpdateForm {

    private int id;
    private String title;
    private String author;
    private int numberPage;

    public BookUpdateForm(BookWS source) {
        this.title = source.getTitle();
        this.author = source.getAuthor();
        this.id = source.getId();
        this.numberPage = source.getNumberPage();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public int getNumberPage() {
        return numberPage;
    }

    public void setNumberPage(int numberPage) {
        this.numberPage = numberPage;
    }
}
