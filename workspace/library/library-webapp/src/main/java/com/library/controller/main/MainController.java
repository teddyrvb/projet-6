package com.library.controller.main;


import com.library.controller.user.LoginForm;
import org.exemple.demo.webservice.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpSession;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Controller
public class MainController {


    /* ICI JE VIENS D'APPLIQUER L'IMPORT DU CLIENT  */
    @Autowired
    private LibraryWS libraryWS;

    /* ==================================================================*/
    /* ==== Route vers la page d'accueil / Path towards the
     * homePage ==== */
    /* ==================================================================*/
    @RequestMapping(value = "/*", method = RequestMethod.GET)
    public String indexBook(ModelMap modelMap) {

        List<BookWS> booksBdd = libraryWS.retrieveAllBook();
        modelMap.addAttribute("books", booksBdd);

        modelMap.addAttribute("availables", libraryWS.retrieveAllBook());

        return "view.home";
    }

    /* ==================================================================*/
    /* ==== Affichage du livre recherché / Display of the search book ==== */
    /* ==================================================================*/
    @RequestMapping(value = "/searchBook", method = RequestMethod.POST)
    public String loginProcess(@RequestParam("title") String title,
                               @ModelAttribute("login") LoginForm login,
                               ModelMap modelMap, HttpSession httpSession) {



        UserWS user = (UserWS) httpSession.getAttribute("currentUser");


        if (user == null){

            modelMap.addAttribute("login", login);


            return "redirect: personalUser";
        }

        modelMap.addAttribute("books", libraryWS.findBook(title, user));
        modelMap.addAttribute("localDateTimeFormat", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));

        return "view.find";
    }


    /* ==================================================================*/
    /* ==== Affichage de l'ensemble des utilisateurs avec leurs prêt /
     * Display all of the user with them loan ==== */
    /* ==================================================================*/
    @RequestMapping(value = "/displayLoan", method = RequestMethod.GET)
    public String displayUserAndLoan(ModelMap modelMap/*, LoanImpl loan*/) {

        modelMap.addAttribute("loan", libraryWS.displayUserAndLoan());
        modelMap.addAttribute("localDateTimeFormat", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));


        return "view.loan";
    }


    /* ==================================================================*/
    /* ==== Route vers la page d'administration / Path towards
     * the adminPage ==== */
    /* ==================================================================*/
    @RequestMapping(value = "adminPath", method = RequestMethod.GET)
    public String adminControlPath(ModelMap modelMap) {


        return "view.admin.main";
    }


    /* ==================================================================*/
    /* ==== Route vers la page d'administration des livres / Path towards
     * the adminPage of the book ==== */
    /* ==================================================================*/
    @RequestMapping(value = "adminBook", method = RequestMethod.GET)
    public String bookPath(ModelMap modelMap) {

        List<BookWS> booksBdd = libraryWS.retrieveAllBook();
        modelMap.addAttribute("books", booksBdd);

        return "view.admin.book";
    }


    /* ==================================================================*/
    /* ==== Route vers la page d'administration des utilisateurs / Path towards
     * the adminPage of the users ==== */ /* --- Fonctionnel ! --- */
    /* ==================================================================*/
    @RequestMapping(value = "adminUser", method = RequestMethod.GET)
    public String adminUserPath(ModelMap modelMap) {

        List<UserWS> usersBdd = libraryWS.retrieveAllUser();
        modelMap.addAttribute("users", usersBdd);

        return "view.admin.user";
    }

    /* ==================================================================*/
    /* ==== Affichage de l'ensemble des utilisateurs avec leurs réservation /
     * Display all of the user with theirs bookings ==== */
    /* ==================================================================*/
    @RequestMapping(value = "/bookingsUser", method = RequestMethod.GET)
    public String userAndTheirsBooking(ModelMap modelMap) {

        modelMap.addAttribute("booking", libraryWS.findAllBooking());
        modelMap.addAttribute("localDateTimeFormat", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));

        return "view.booking";
    }
}
