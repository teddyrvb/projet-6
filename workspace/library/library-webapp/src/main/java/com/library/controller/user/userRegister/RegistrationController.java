package com.library.controller.user.userRegister;

import com.library.controller.user.UserAddForm;
import org.exemple.demo.webservice.LibraryWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegistrationController {

    /* ICI JE VIENS D'APPLIQUER L'IMPORT DU CLIENT  */
    @Autowired
    private LibraryWS libraryWS;


    /* ==================================================================*/
    /* ==== Route qui mène à l'enregistrement d'un utilisateur / Path who
     * send to an register of an user */
    /* ==================================================================*/
    @RequestMapping(value = "/{register}", method = RequestMethod.GET)
    public ModelAndView addBook() {
        ModelAndView model = new ModelAndView();

        UserAddForm addFormRegisterUser = new UserAddForm();
        model.addObject("userAddForm", addFormRegisterUser);

        model.setViewName("view.register.form");
        return model;
    }


    /* ==================================================================*/
    /* ==== Route qui permet l'enregistrement de l'utilisateur en base / Path who
     * allow the register to an user in database */
    /* ==================================================================*/
    @RequestMapping(value="/{register}", method=RequestMethod.POST)
    public String formUser(@ModelAttribute("userAddForm") UserAddForm form , BindingResult validationResult) {

        if (!validationResult.hasErrors()) {
            libraryWS.insertNewUser(form.getName(), form.getFirstName(), form.getAddress(), form.getPhoneNumber(), form.getEmailAddress());
            return "redirect:/*";
        }
        return  "view.register.form";
    }
}
