package com.library.controller.loan;

import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Controller
public class LoanAddForm {

    private int id;
    private LocalDateTime startingDate;
    private LocalDateTime expiredDate;
    private int isConfirm;

    public LoanAddForm() {}

    public LoanAddForm(int id, LocalDateTime startingDate, LocalDateTime expiredDate, int isConfirm) {
        this.id = id;
        this.startingDate = startingDate;
        this.expiredDate = expiredDate;
        this.isConfirm = isConfirm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDateTime startingDate) {
        this.startingDate = startingDate;
    }

    public LocalDateTime getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(LocalDateTime expiredDate) {
        this.expiredDate = expiredDate;
    }

    public int getIsConfirm() {
        return isConfirm;
    }

    public void setIsConfirm(int isConfirm) {
        this.isConfirm = isConfirm;
    }
}
