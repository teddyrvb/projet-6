package com.library.controller.book;



import org.exemple.demo.webservice.BookWS;
import org.exemple.demo.webservice.LibraryWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

// @controller dérive de @Component
// @RestController pour un controller de type Rest
/* ==== Route vers la page d'accueil / Path towards the homePage ==== */


@Controller
public class BookController {

    /* ICI JE VIENS D'APPLIQUER L'IMPORT DU CLIENT  */
    @Autowired
    private LibraryWS libraryWS;


    /* ==================================================================*/
    /* ==== Route vers le formulaire d'ajout et de modification d'un livre Path towards
     * the addForm and modifForm of a book ==== */
    /* ==================================================================*/

    @RequestMapping(value = "addBook", method = RequestMethod.GET)
    public ModelAndView addBook() {
        ModelAndView model = new ModelAndView();

        BookAddForm bookAddForm = new BookAddForm();
        model.addObject("bookAddForm", bookAddForm);

        model.setViewName("view.admin.form.book");
        return model;
    }


    /* ==================================================================*/
    /* ==== Insertion de livre / Insert of book ==== */
    /* ==================================================================*/
    @RequestMapping(value="addBook", method=RequestMethod.POST)
    public String formBook(@ModelAttribute("bookAddForm") BookAddForm form, BindingResult validationResult) {

        if (!validationResult.hasErrors()) {
            libraryWS.insertNewBook(form.getTitle(), form.getAuthor(), form.getNumberPage());
            return "redirect:/adminBook";
        }


        return  "view.admin.form.book";
    }


    /* ==================================================================*/
    /* ==== Chemin d'accès à la suppression d'un book / Acces path to the
    removing of book ==== */
    /* ==================================================================*/
    @RequestMapping (value = "deleteBook/{id}" , method = RequestMethod.GET)
    public ModelAndView deleteContact(@PathVariable int id) {

        BookWS book = libraryWS.findBookById(id);
        libraryWS.removeBook(id);
        return new ModelAndView("view.admin.book");
    }


    /* ==================================================================*/
    /* ==== Affichage du formulaire d'édition d'un livre /
     * View of a new edit book form ==== */
    /* ==================================================================*/
    @GetMapping("/book/{id}")
    public String updateBookGet(ModelMap map, @PathVariable int id){

        BookWS bookId = libraryWS.findBookById(id);
        map.addAttribute("bookEditForm",new BookUpdateForm(bookId));

        return "view.admin.edit.form.book";
    }


    /* ==================================================================*/
    /* ==== Validation du formulaire d'édition / Validating of update form ==== */
    /* ==================================================================*/
    @PostMapping("/book/{id}")
    public String updateBookForm(@Valid @ModelAttribute("bookEditForm")BookAddForm form, BindingResult validationresult){

        if (!validationresult.hasErrors()){

            BookWS book = libraryWS.findBookById(form.getId());
            libraryWS.updateBook(book,form.getTitle(),form.getAuthor(), form.getNumberPage());
            return "redirect:/adminBook";
        }
        return "view.admin.edit.form.book";
    }
}
