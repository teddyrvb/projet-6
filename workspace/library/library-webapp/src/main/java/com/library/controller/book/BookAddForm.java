package com.library.controller.book;

import org.exemple.demo.webservice.BookWS;

public class BookAddForm {
    private int Id;

    private String title;
    private String author;
    private int numberPage;

    public BookAddForm(int id, String title, String author, int numberPage) {
        Id = id;
        this.title = title;
        this.author = author;
        this.numberPage = numberPage;
    }

    public BookAddForm(BookWS editBook) {

    }

    BookAddForm() {}

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNumberPage() {
        return numberPage;
    }

    public void setNumberPage(int numberPage) {
        this.numberPage = numberPage;
    }
}
