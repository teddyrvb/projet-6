package com.library.controller.user;


import org.exemple.demo.webservice.LibraryWS;
import org.exemple.demo.webservice.UserWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@Controller
public class UserController {

    /* ICI JE VIENS D'APPLIQUER L'IMPORT DU CLIENT  */
    @Autowired
    private LibraryWS libraryWS;



    /* ==================================================================*/
    /* ==== Route vers le formulaire d'ajout et de modification d'un utilisateur Path towards
     * the addForm and modifForm of a user ==== */ /* Fonctionnel ! */
    /* ==================================================================*/

 @RequestMapping(value = "/addFormUser", method = RequestMethod.GET)
    public ModelAndView addUser() {
        ModelAndView model = new ModelAndView();

        UserAddForm userAddForm = new UserAddForm();
        model.addObject("addFormUser", userAddForm);

        model.setViewName("view.admin.form.user");
        return model;
    }


    /* ==================================================================*/
    /* ==== Insertion d'un utilisateur / Insert of user ==== */ /* Fonctionnel ! */
    /* ==================================================================*/
  @RequestMapping(value="/addFormUser", method=RequestMethod.POST)
    public String formUser(@ModelAttribute("addFormUser") UserAddForm form , BindingResult validationResult) {

        if (!validationResult.hasErrors()) {
            libraryWS.insertNewUser(form.getName(), form.getFirstName(), form.getAddress(), form.getPhoneNumber(), form.getEmailAddress());
            return "redirect:/adminUser";
        }


        return  "view.admin.user";
    }

    /* ==================================================================*/
    /* ==== Chemin d'accès à la suppression d'un Utilisateur / Acces path to the
    removing of user ==== */ /* --- Fonctionnel ! --- */
    /* ==================================================================*/
    @RequestMapping (value = "deleteUser" , method = RequestMethod.GET)
    public ModelAndView deleteUser(HttpServletRequest request) {

        int userId = Integer.parseInt(request.getParameter("id"));
        libraryWS.removeUser(userId);
        return new ModelAndView("view.admin.user");
    }


    /* ==================================================================*/
    /* ==== Affichage du formulaire d'édition d'un utilisateur /
     * View of a new edit book user ==== */
    /* ==================================================================*/
    @GetMapping("user/{id}")
    public String updateUserGet(ModelMap map, @PathVariable int id){

        UserWS userId = libraryWS.findUserById(id);
        map.addAttribute("userEditForm",new UserUpdateForm(userId));

        return "view.admin.edit.form.user";
    }


    /* ==================================================================*/
    /* ==== Validation du formulaire d'édition / Validating of update form ==== */
    /* ==================================================================*/
   @PostMapping("user/{id}")
    public String updateUserPost(@Valid @ModelAttribute("userEditForm")UserAddForm form, BindingResult validationresult){

        if (!validationresult.hasErrors()){

            UserWS user = libraryWS.findUserById(form.getId());
            libraryWS.updateUser(user,form.getName(), form.getFirstName(), form.getAddress(), form.getPhoneNumber(), form.getEmailAddress());
            return "redirect:/adminUser";
        }
        return "view.admin.edit.form.user";
    }
}
