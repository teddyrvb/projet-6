package com.library.controller.user.userRegister;


import com.library.controller.loan.LoanAddForm;
import com.library.controller.user.LoginForm;
import org.exemple.demo.webservice.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


@Controller
public class UserDashboard {

    /* ICI JE VIENS D'APPLIQUER L'IMPORT DU CLIENT  */
    @Autowired
    private LibraryWS libraryWS;

    /* ------ partie login ------- */
    /* ==================================================================*/
    /* ==== Route vers la page d'administration utilisateur / Path towards
     * the user managePage ==== */
    /* ==================================================================*/
    @RequestMapping(value = "personalUser", method = RequestMethod.GET)
    public String userDashboardControl(ModelMap modelMap, HttpSession session) {

        UserWS user = (UserWS) session.getAttribute("currentUser");

        if(user == null) {
            return "redirect:/loginPath";
        }

        modelMap.addAttribute("findBook", libraryWS.retrieveAllBookAndUser(user.getId()));
        modelMap.addAttribute("readyForReservation", libraryWS.retrieveAllBooksWhereBookingAreAvailable(user.getId()));
        modelMap.addAttribute("loans", libraryWS.findLoanByUser(user.getId()));
        modelMap.addAttribute("availables", libraryWS.retrieveAllBook());
        modelMap.addAttribute("unavailables", libraryWS.findAllUnavailableBooks());
        modelMap.addAttribute("bookings", libraryWS.findBookingByUser(user));
        modelMap.addAttribute("loan", libraryWS.displayUserAndLoan());
        modelMap.addAttribute("localDateTimeFormat", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));


        return "view.user.dashboard";
    }

    /* ==================================================================*/
    /* ==== Route vers le formulaire de login de l'utilisateur / Path into
     * the user loginForm ==== */
    /* ==================================================================*/
    @RequestMapping(value = "/loginPath", method = RequestMethod.GET)
    public String showFormLogin(ModelMap model) {

        model.addAttribute("login", new LoginForm());

        return "view.login";
    }

    /* ==================================================================*/
    /* ==== Soumission du formulaire de validation d'un utilisateur / Submit
     * of the user validateForm ==== */
    /* ==================================================================*/
    @RequestMapping(value = "/loginPath", method = RequestMethod.POST)
    public String loginProcess(@ModelAttribute("login") LoginForm login,
                               BindingResult validationresult, HttpSession session) {

        if (validationresult.hasErrors())
            return "view.login";

        UserWS user = libraryWS.login(login.getName(), login.getEmailAddress());

        if (user == null)
            return "view.login";

        session.setAttribute("currentUser", user);

        return "redirect: personalUser";

    }


    /* --- après login --- */

    /* ==================================================================*/
    /* ==== Bouton permettant l'ajout d'un emprunt / Button
     * allow to add a loan ==== */
    /* ==================================================================*/
    @RequestMapping(value = "emprunt", method = RequestMethod.GET)
    public String addLoan(@RequestParam("exemplaryId") int exemplaryId, HttpSession session, LoanAddForm form) {

        BookWS bookWS = libraryWS.findBookById(exemplaryId);
        ExemplaryWS exemplary = libraryWS.findExemplaryNotInLoan(bookWS);



        UserWS user = ((UserWS) session.getAttribute("currentUser"));


        libraryWS.insertNewLoan(user, exemplary, form.getIsConfirm());

        return "redirect:/personalUser";
    }

    /* ==================================================================*/
    /* ==== Bouton permettant la prolongation du prêt d'un livre / Buton
     * allow to prolongate a loan of a book ==== */
    /* ==================================================================*/
    @RequestMapping(value = "prolongation", method = RequestMethod.GET)
    public String prolongation(@RequestParam("id") int id) {

        /* ---------------- */

        LoanWS loan = libraryWS.findLoanById(id);

        /* ---------------- */
        libraryWS.prolonger(loan);

        return "redirect:/personalUser";
    }

    /* ==================================================================*/
    /* ==== Bouton permettant la restitution d'un livre / Buton
     * allow to the restitution of a book ==== */
    /* ==================================================================*/
    @RequestMapping(value = "restitution", method = RequestMethod.GET)
    public String restitution(@RequestParam("id") int id, @RequestParam("exemplaryId") int exemplaryId) {

        LoanWS loan = libraryWS.findLoanById(id);

        libraryWS.restituer(loan);

        return "redirect:/personalUser";
    }

    /* ==================================================================*/
    /* ==== Bouton permettant la réservation d'un livre / Buton
     * allow to the booking of book ==== */
    /* ==================================================================*/

    @RequestMapping(value = "reservation", method = RequestMethod.GET)
    public String addBooking(@RequestParam("bookId") int bookId,  HttpSession session) {

        BookWS book = libraryWS.findBookById(bookId);

        UserWS user = ((UserWS) session.getAttribute("currentUser"));

        /* ici on ajoute la methode qui permet de faire l'insertion*/
        libraryWS.methodToManageBooking(user, book);


        return "redirect:/personalUser";
    }


    /* ==================================================================*/
    /* ==== Bouton permettant la suppression d'une réservation / Buton
     * allow to the remove of booking ==== */
    /* ==================================================================*/
    @RequestMapping(value = "suppressionReservation", method = RequestMethod.GET)
    public String suppressionReservation(@RequestParam("id") int id) {

        BookingWS booking = libraryWS.findBookingById(id);

        libraryWS.removeBooking(booking.getID());

        return "redirect:/personalUser";
    }

    @RequestMapping(value = "creationPretDepuisReservation", method = RequestMethod.GET)
    public String creationPretDepuisReservation(@RequestParam("id") int id,
                                                HttpSession session) {

        LoanWS loan = libraryWS.findLoanById(id);
        UserWS user = ((UserWS) session.getAttribute("currentUser"));

        libraryWS.createATrueLoan(loan);

        return "redirect:/personalUser";
    }
}
