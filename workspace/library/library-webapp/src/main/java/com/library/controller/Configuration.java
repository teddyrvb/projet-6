package com.library.controller;


import org.exemple.demo.webservice.LibraryWS;
import org.exemple.demo.webservice.LibraryWSService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public LibraryWS getLibraryWS(){
        LibraryWSService factory = new LibraryWSService();
        LibraryWS service = factory.getLibraryWSPort();

        return service;
    }
}
