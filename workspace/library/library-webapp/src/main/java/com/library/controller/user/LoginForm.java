package com.library.controller.user;


public class LoginForm {

    private String name;
    private String emailAddress;


    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }
}
