<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Affichage des livres après recherche</title>
</head>
<body>
<c:if test="${currentUser != null}">
<h1><c:out value="${currentUser.name}"/>, Voici les livres qui ont été trouvé pour vous:</h1>
</c:if>

<c:if test="${currentUser == null}">
    <h1>Livre trouvé:</h1>
</c:if>


<div class="container-fluid">
    <div class="table-responsive rounded">
        <table class="table table-striped rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
                <th scope="col">Nombre d'exemplaire disponible</th>
                <th scope="col">Date de retour la plus proche</th>
                <th scope="col">Si vous souhaitez réserver</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${books}" var="findBook">
                <tr>
                    <td><c:out value="${findBook.title}"/></td>
                    <td><c:out value="${findBook.author}"/></td>
                    <td><c:out value="${findBook.numberPage}"/></td>
                    <td><c:out value="${findBook.numberExemplaryAvailable}"/></td>
                    <td><c:out value="${LocalDateTime.parse(findBook.earliestDateForExemplaryId, localDateTimeFormat)
                                                             .format(DateTimeFormatter.ofPattern('dd-MM-yyyy'))}"/>
                    </td>
                    <td><c:if test="${findBook.trueIfLoanIsSaturated && currentUser != null
                    && findBook.numberExemplaryAvailable == 0}">
                        <a class="btn btn-primary hoverable buttonList"
                           href="reservation?bookId=${findBook.id}">Réserver</a>
                    </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
