<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="<c:url value='/static/css/form.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>


<title>Connexion</title>

<div class="container-fluid">
    <div class="dropdown">
        <form:form class="px-4 py-3" method="POST" modelAttribute="login">
            <div class="form-group">
                <form:label path="name" for="name" class="name">Votre nom:</form:label>
                <form:input path="name" type="text" class="form-control" id="name" placeholder="nom" />
            </div>
            <div class="form-group">
                <form:label path="emailAddress" for="emailAddress" class="emailAddress">Votre adresse email:</form:label>
                <form:input path="emailAddress" type="email" class="form-control" id="emailAddress" placeholder="email@exemple.com" />
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="dropdownCheck">
                <label class="form-check-label" for="dropdownCheck">
                    Se souvenir de moi
                </label>
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
            </button>
        </form:form>
    </div>
</div>
