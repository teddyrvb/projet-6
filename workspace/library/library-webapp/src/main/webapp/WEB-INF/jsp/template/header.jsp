<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<style>
    .espace{
        margin-left: 65%;
        margin-top: -10px;
    }

    .btnSearch{
        margin-top: -30px;
    }

    .espace-mobile{
        margin-left: 5px;
    }

    @media screen and (max-width: 992px){
        .espace{
            visibility: hidden;
        }
    }

    nav{
        height: 80px;
        padding-top: 20px;
    }

    .pulse{
        width: 65px;
    }

    .btn-mobile{
        text-decoration: none;
    }
</style>

<div class="navbar-fixed">
    <nav class="navbar-dark bg-dark">
        <div class="nav-wrapper col s9">
            <a href="#" data-activates="for-mobile" class="button-collapse btn-floating cyan pulse btn-mobile"><i class="material-icons">menu</i></a>
            <ul class="left hide-on-med-and-down">

                <li>
                    <a href="<c:url value="/" />">Accueil</a>
                </li>
                <li>
                    <a class = "dropdown-button" href="#" data-activates="dropdown-user">Connexion
                        <i class="mdi-navigation-arrow-drop-down right"></i></a>

                    <ul id="dropdown-user" class="dropdown-content">
                        <li><a href="<c:url value="/register" />">Vous enregistrer</a></li>
                        <li><a href="<c:url value="/loginPath" />">Login</a></li>
                    </ul>
                </li>

                <li>
                    <a class = "dropdown-button" href="#" data-activates="dropdown-admin">Administration
                        <i class="mdi-navigation-arrow-drop-down right"></i></a>

                    <ul id="dropdown-admin" class="dropdown-content">

                        <li><a href="<c:url value="/adminPath" />">Administration</a></li>
                        <li><a href="<c:url value="/displayLoan"/>">Utilisateurs avec leurs emprunt</a></li>
                        <li class="divider"></li>
                        <li><a href="<c:url value="/adminBook"/>">Gestion des livres</a></li>
                        <li><a href="<c:url value="/adminUser"/>">Gestion des utilisateurs</a></li>
                    </ul>
                </li>
            </ul>
            <form class="espace" method="POST" action="<c:url value="/searchBook"/>">
                <input class="form-control" type="search" placeholder="Rechercher" id="nomDuChamp" name="title">
                <button class="btn btn-outline-success btnSearch" type="submit">Nom du livre disponible</button>
            </form>

            <ul class="side-nav" id="for-mobile">
                <li>
                    <a href="<c:url value="/" />">Accueil</a>
                </li>
                <li>
                    <ul>
                        <li>User:</li>
                        <li><a href="<c:url value="/register" />">Vous enregistrer</a></li>
                        <li><a href="<c:url value="/loginPath" />">Login</a></li>
                    </ul>
                </li>

                <li>
                  <ul>
                      <li>Admin:</li>
                      <li><a href="<c:url value="/adminPath" />">Administration</a></li>
                      <li><a href="<c:url value="/displayLoan"/>">Utilisateurs avec leurs emprunt</a></li>
                      <li><a href="<c:url value="/adminBook"/>">Gestion des livres</a></li>
                      <li><a href="<c:url value="/adminUser"/>">Gestion des utilisateurs</a></li>
                  </ul>
                </li>


                <li>
                    <form class="form-inline my-2 my-lg-0 espace-mobile" method="POST"
                          action="<c:url value="/searchBook"/>">
                        <input class="form-control mr-sm-2" type="search" placeholder="Rechercher" id="nomDuChampM"
                               name="title">
                        <button class="btn btn-outline-success my-2 my-sm-0 btnSearch" type="submit">Nom du livre
                            disponible
                        </button>
                    </form>
                </li>
            </ul>
        </div>
    </nav>
</div>







