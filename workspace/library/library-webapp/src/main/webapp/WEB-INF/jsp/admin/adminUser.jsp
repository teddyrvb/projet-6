<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<link href="<c:url value='/static/css/adminUser.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>


<html>
<head>
    <title>Affichage des utilisateurs depuis l'administration</title>
</head>
<body>


<div class="container-fluid">
    <h1 class="text-center text-uppercase">Les personnes enregistrées sur le site</h1>
    <p class="text-center">Gestion des utilisateurs</p>
    <div class="table-responsive rounded">
        <table class="table table-striped rounded">
            <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Adresse</th>
                <th scope="col">Tel</th>
                <th scope="col">Email</th>
                <th scope="col">Vos interventions</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${users}" var="user">
                <tr>
                    <td><c:out value="${user.name}"/></td>
                    <td><c:out value="${user.firstName}"/></td>
                    <td><c:out value="${user.address}"/></td>
                    <td><c:out value="${user.phoneNumber}"/></td>
                    <td><c:out value="${user.emailAddress}"/></td>

                    <td>
                        <a class="btn btn-warning hoverable sizeButton" href='<c:out value="/user/${user.id}" />'>Modifier un utilisateur</a>
                        <a class="btn btn-danger hoverable sizeButton" href="${contextPath} /deleteUser?id=${user.id}">Supprimer un utilisateur</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <p style="text-align:center">
        <a class="btn btn-primary hoverable" id="formLink" href='<c:url value="/addFormUser" />'>Ajouter un nouvel utilisateur</a>
    </p>
    <div>
        <a class="btn btn-primary hoverable" href='<c:url value="/" />'>Page d'accueil</a>
    </div>
</div>
</body>
</html>
