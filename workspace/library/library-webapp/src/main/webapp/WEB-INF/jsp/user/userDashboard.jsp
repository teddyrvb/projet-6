<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="<c:url value='/static/css/userDashboard.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>
    <title>L'utilisateur et ses prêts</title>
</head>
<body>


<div class="container-fluid">
    <h3>Bonjour <c:out value="${currentUser.name}"/> et bienvenue</h3>
    <h4>Voici les livres que vous avez sélectionné pour ce mois-ci</h4>

    <div class="table-responsive rounded">
        <table class="table table-striped table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Date où vous avez emprunté ce livre</th>
                <th scope="col">Date d'expiration</th>
                <th scope="col">Vous avez atteint la date d'expiration</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${loans}" var="loan">
                <c:if test="${loan.isConfirm != 1}">
                    <tr>
                        <td><c:out value="${loan.exemplary.book.title}"/></td>
                        <td><c:out value="${LocalDateTime.parse(loan.startingDate.substring(0, 16), localDateTimeFormat).format(DateTimeFormatter.ofPattern('dd-MM-yyyy'))}" /></td>
                        <td><c:out value="${LocalDateTime.parse(loan.expiredDate.substring(0, 16), localDateTimeFormat).format(DateTimeFormatter.ofPattern('dd-MM-yyyy'))}"/></td>
                        <td><c:if test="${loan.late}"><p>Vous avez du retard.</p></c:if>
                            <c:if test="${!loan.trueIfUserClicOnExpiratedDateButton}">
                            <a class="btn btn-primary hoverable buttonList" href="prolongation?id=${loan.id}">prolonger</a>
                            </c:if>
                            <a class="btn btn-primary hoverable buttonList" href="restitution?id=${loan.id}&exemplaryId=${loan.exemplary.id}">restituer</a>
                    </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <h4>Vous devez valider ce(s) livre(s) sous 48 heures afin qu'il(s) soi(en)t à vous</h4>

    <div class="table-responsive rounded">
        <table class="table table-striped table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Date depuis laquelle ce livre vous est apparu disponible</th>
                <th scope="col">Date butoire</th>
                <th scope="col">Validation du livre</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${loans}" var="loan">
                <c:if test="${loan.isConfirm == 1}">
                    <tr>
                        <td><c:out value="${loan.exemplary.book.title}"/></td>
                        <td><c:out value="${LocalDateTime.parse(loan.startingDate.substring(0, 16), localDateTimeFormat)
                        .format(DateTimeFormatter.ofPattern('dd-MM-yyyy'))}" /></td>
                        <td>Dépend de vous.</td>
                        <td><a class="btn btn-primary hoverable buttonList"
                               href="creationPretDepuisReservation?id=${loan.id}">Valider ce livre</a></td>
                    </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>
    </div>




    <h4>Voici vos réservations actuelles</h4>

    <div class="table-responsive rounded">
        <table class="table table-striped table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre du livre</th>
                <th scope="col">Date à laquelle vous avez reservé</th>
                <th scope="col">Annulation de la réservation</th>
                <th scope="col">Votre position dans la liste</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${bookings}" var="bookings">
                <tr>
                    <td><c:out value="${bookings.bookId.title}"/></td>
                    <td><c:out value="${LocalDateTime.parse(bookings.bookingStartingDate.substring(0, 16), localDateTimeFormat).format(DateTimeFormatter.ofPattern('dd-MM-yyyy'))}"/></td>
                    <td><a class="btn btn-primary hoverable buttonList"
                           href="suppressionReservation?id=${bookings.ID}">annuler</a></td>
                        <%--  <td>
                          <c:if test="${bookings.messageForValidate}">
                                <a class="btn btn-primary hoverable buttonList"
                                   href="creationPretDepuisReservation?id=${bookings.ID}">emprunter</a><br />
                               <span>Vous pouvez désormais emprunter ce livre</span> </c:if></td> --%>
                    <td><c:out value="${bookings.positionOfUserInTheWaitingList}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>



    <h4>Liste des livres disponibles</h4>
    <div class="table-responsive rounded">
        <table class="table table-striped table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
                <th scope="col">Nombre de livres disponibles</th>
                <th scope="col">Prêt</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${availables}" var="available">
                <tr><c:if test="${available.numberExemplaryAvailable > 0}">
                    <td><c:out value="${available.title}"/></td>
                    <td><c:out value="${available.author}"/></td>
                    <td><c:out value="${available.numberPage}"/></td>
                    <td><c:out value="${available.numberExemplaryAvailable}"/></td>
                    <td>
                        <c:if test="${!available.trueIfBookEqualZero}">
                            <a class="btn btn-primary hoverable buttonList" href="emprunt?exemplaryId=${available.id}">emprunter</a>
                        </c:if>
                    </td>
                </c:if>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <h4>Liste des livres disponibles à la réservation</h4>
    <div class="table-responsive rounded">
        <table class="table table-striped table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
                <th scope="col">Date de retour la plus proche pour ce livre</th>
                <th scope="col">Tout ceux qui ont réservé ce livre</th>
                <th scope="col">Réservation</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${readyForReservation}" var="readyForReservation">
                <tr>
                    <td><c:out value="${readyForReservation.title}"/></td>
                    <td><c:out value="${readyForReservation.author}"/></td>
                    <td><c:out value="${readyForReservation.numberPage}"/></td>
                    <td><c:out value="${LocalDateTime.parse(readyForReservation.earliestDateForExemplaryId.substring(0, 16), localDateTimeFormat)
                                                             .format(DateTimeFormatter.ofPattern('dd-MM-yyyy'))}"/></td>
                    <td><c:out value="${readyForReservation.numberBookForTheSameBooking}"/> personne(s) à/ont réservé(e)(s) ce livre</td>
                    <td><%-- <c:if test="${ !readyForReservation.trueForTheSameLoan}">
                                <c:if test="${ !readyForReservation.trueForTheSameBooking}"> --%>
                        <c:if test="${readyForReservation.trueIfLoanIsSaturated}">
                            <a class="btn btn-primary hoverable buttonList"
                               href="reservation?bookId=${readyForReservation.id}">Réserver</a>
                        </c:if>
                            <%--      </c:if>
                              </c:if>  --%>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>


</div>
</body>
</html>
