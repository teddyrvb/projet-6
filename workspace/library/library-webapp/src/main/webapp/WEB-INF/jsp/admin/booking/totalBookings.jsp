<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ensemble des utilisateurs et de leurs réservations</title>
</head>
<body>

<h1>Ensemble des réservations</h1>

<p>Voici un tableau contenant l'ensemble des utilisateurs ainsi que leurs réservations</p>

<div class="container-fluid">
    <h1 class="text-center text-uppercase">Les livres</h1>
    <p class="text-center">Vue de l'ensemble des livres:</p>
    <div class="table-responsive rounded">
        <table class="table table-striped rounded">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Nom de l'utilisateur ayant réservé ce livre</th>
                <th scope="col">Nom du livre</th>
                <th scope="col">Date de la réservation</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${booking}" var="booking">
                <tr>
                    <th scope="row">${booking.userId.name}</th>
                    <td><c:out value="${booking.bookId.title}"/></td>
                    <td><c:out value="${LocalDateTime.parse(booking.bookingStartingDate, localDateTimeFormat)
                                                             .format(DateTimeFormatter.ofPattern('dd-MM-yyyy'))}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div>
        <a href='<c:url value="/" />'>Page d'accueil</a>
    </div>
</div>

</body>
</html>
