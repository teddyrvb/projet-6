<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page pageEncoding="UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<c:url value='/static/css/home.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>
    <title>menu principal</title>
</head>
<body>

<div class="container-fluid">
<c:if test="${currentUser != null}">
    <h1>Bonjour, <c:out value="${currentUser.name}"/></h1>
</c:if>
    <br />
    <h1>Liste des livres présents dans la bibliothèque</h1>
    <br/>
    <h2>Liste des livres</h2>

    <div class="table-responsive rounded">
        <table class="table table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${availables}" var="available">
                <tr>
                    <td><c:out value="${available.title}"/></td>
                    <td><c:out value="${available.author}"/></td>
                    <td><c:out value="${available.numberPage}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <h2>Liste des livres disponibles</h2>
    <div class="table-responsive rounded">
        <table class="table table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
                <th scope="col">Nombre de livres disponibles</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${availables}" var="available">
                <tr><c:if test="${available.numberExemplaryAvailable > 0}">
                    <td><c:out value="${available.title}"/></td>
                    <td><c:out value="${available.author}"/></td>
                    <td><c:out value="${available.numberPage}"/></td>
                    <td><c:out value="${available.numberExemplaryAvailable}"/></td>
                </c:if>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <h2>Liste des livres non disponibles</h2>
    <div class="table-responsive rounded">
        <table class="table table-dark rounded">
            <thead>
            <tr>
                <th scope="col">Titre</th>
                <th scope="col">Auteur</th>
                <th scope="col">Nombre de page</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${availables}" var="available">
                <tr><c:if test="${available.numberExemplaryAvailable == 0}">
                    <td><c:out value="${available.title}"/></td>
                    <td><c:out value="${available.author}"/></td>
                    <td><c:out value="${available.numberPage}"/></td>
                </c:if>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>

    <div class="linkButton">
            <a class="btn btn-primary hoverable buttonList" style="color: white; text-underline:none" href='<c:url value="/register" />'>Pour vous enregistrer</a><br />
            <a class="btn btn-primary hoverable buttonList" style="color: white; text-underline:none" href='<c:url value="/loginPath" />'>Pour vous connecter</a><br />
            <a class="btn btn-primary hoverable buttonList"  style="color: white; text-underline:none" href='<c:url value="/displayLoan"/>'>Les utilisateurs et leurs emprunts</a><br />
            <a class="btn btn-primary hoverable buttonList"  style="color: white; text-underline:none" href='<c:url value="/bookingsUser"/>'>Les utilisateurs et leurs réservations</a><br />
            <a class="btn btn-primary hoverable buttonList"  style="color: white; text-underline:none" href='<c:url value="/adminPath" />'>Vers l'administration</a><br />
    </div>
</div>
</body>
</html>
