<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--
<div id="footer">
    <p>premier message</p>
</div>
-->

<footer class="page-footer grey darken-4">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Nos sponsors</h5>
                <ul class="none">
                    <li><a class="grey-text text-lighten-3" href="#!">Amazon</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Ebay</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Cdiscount</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Les grands marchands</a></li>
                </ul>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Nos partenaires</h5>
                <ul class="none">
                    <li><a class="grey-text text-lighten-3" href="#!">Bibliotecarius</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Ultimate Book</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Le livre</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Les grands marchands</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright Text
        </div>
    </div>
</footer>