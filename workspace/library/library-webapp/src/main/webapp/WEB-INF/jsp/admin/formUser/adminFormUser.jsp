<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">
    <link href="<c:url value='/static/css/form.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>
    <title>Formulaire d'ajout d'un utilisateur par l'administrateur</title>
</head>
<body>


<div class="container-fluid">
    <form:form method="POST" modelAttribute="addFormUser">
        <div class="form-group">
            <form:label path="name" for="name" class="name">Nom</form:label>
            <form:input path="name" type="text" class="form-control" id="name" aria-describedby="nameHelp" />
            <small id="nameHelp" class="form-text text-muted">Veuillez entrer ici le votre nom</small>
        </div>

        <div class="form-group">
            <form:label path="firstName" for="firstName" class="firstName">Prénom</form:label>
            <form:input path="firstName" type="text" class="form-control" id="firstName" aria-describedby="fist_nameHelp" />
            <small id="first_nameHelp" class="form-text text-muted">Veuillez entrer ici votre prénom</small>
        </div>


        <div class="form-group">
            <form:label path="address" for="address" class="address">Adresse</form:label>
            <form:input path="address" type="text" class="form-control" id="address" aria-describedby="addressHelp" />
            <small id="adressHelp" class="form-text text-muted">Veuillez entrer ici votre adresse</small>
        </div>

        <div class="form-group">
            <form:label path="phoneNumber" for="phoneNumber" class="phoneNumber">Numéro de téléphone</form:label>
            <form:input path="phoneNumber" type="text" class="form-control" id="phoneNumber" aria-describedby="phone_numberHelp" />
            <small id="phone_numberHelp" class="form-text text-muted">Veuillez entrer ici votre numéro de téléphone</small>
        </div>

        <div class="form-group">
            <form:label path="emailAddress" for="emailAddress" class="emailAddress">Adresse mail</form:label>
            <form:input path="emailAddress" type="text" class="form-control" id="emailAddress" aria-describedby="email_adressHelp" />
            <small id="email_adressHelp" class="form-text text-muted">Veuillez entrer ici votre adresse email</small>
        </div>


        <button class="btn waves-effect waves-light" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
    </form:form>
    <br />
    <div>
        <a class="btn btn-primary hoverable" href="${contextPath} /">Page d'accueil</a>
        <a class="btn btn-primary hoverable" href="${contextPath} admin">Vers le tableau de gestion administrateur</a>
    </div>
</div>
</body>
</html>

