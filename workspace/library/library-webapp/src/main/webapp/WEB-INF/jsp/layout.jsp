<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <!-- Add bootstrap -->
   <link rel="stylesheet" href="<c:url value="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


    <!-- The different css datasource -->
    <link href="<c:url value='/static/css/header.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<c:url value='/static/css/body.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<c:url value='/static/css/footer.css' />" type="text/css" rel="stylesheet" media="screen,projection"/>


    <!-- Materialize -->
    <link rel="stylesheet" href="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css" />" />
    <link href="<c:url value="https://fonts.googleapis.com/icon?family=Material+Icons" />" rel="stylesheet" />

    <!-- end Materialize -->
    <title>Ma bibliothèque</title>
    <!--<tiles:insertAttribute name="styles"/> -->
    <!--<tiles:insertAttribute name="scripts" /> -->

    <style>
        /* on redéfinie ici z-index à 1 et non à 997 */
        #sidenav-overlay{
            z-index: 1;
        }
    </style>
</head>


<body>
<header>
    <tiles:insertAttribute name="header"/>
</header>

<section id="content">
    <tiles:insertAttribute name="body"/>
</section>

<footer>
    <tiles:insertAttribute name="footer" />
</footer>


<!-- Import jQuery -->
<script src="<c:url value='https://code.jquery.com/jquery-3.3.1.min.js' />"></script>

<!-- javascript bootstrap -->
<script src="<c:url value="https://code.jquery.com/jquery-3.3.1.slim.min.js" />"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" /> "
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="<c:url value="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" />"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>


<!-- Javascript materialize -->
<script type="text/javascript" src="<c:url value="https://code.jquery.com/jquery-3.3.1.min.js" />"></script>
<script src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js" />"></script>

<script>
        $(".button-collapse").sideNav();
        $('.dropdown-trigger').dropdown();
</script>
</body>


</html>