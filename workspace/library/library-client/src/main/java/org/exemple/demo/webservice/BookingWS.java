
package org.exemple.demo.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour bookingWS complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="bookingWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bookingStartingDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="userId" type="{http://webservice.demo.exemple.org/}userWS" minOccurs="0"/>
 *         &lt;element name="bookId" type="{http://webservice.demo.exemple.org/}bookWS" minOccurs="0"/>
 *         &lt;element name="isLate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="positionOfUserInTheWaitingList" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="isConfirm" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="messageForValidate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="expiratedBooking" type="{http://webservice.demo.exemple.org/}localDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bookingWS", propOrder = {
    "bookingStartingDate",
    "id",
    "userId",
    "bookId",
    "isLate",
    "positionOfUserInTheWaitingList",
    "isConfirm",
    "messageForValidate",
    "expiratedBooking"
})
public class BookingWS {

    protected String bookingStartingDate;
    @XmlElement(name = "ID")
    protected int id;
    protected UserWS userId;
    protected BookWS bookId;
    protected boolean isLate;
    protected int positionOfUserInTheWaitingList;
    protected boolean isConfirm;
    protected boolean messageForValidate;
    protected LocalDate expiratedBooking;

    /**
     * Obtient la valeur de la propri�t� bookingStartingDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingStartingDate() {
        return bookingStartingDate;
    }

    /**
     * D�finit la valeur de la propri�t� bookingStartingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingStartingDate(String value) {
        this.bookingStartingDate = value;
    }

    /**
     * Obtient la valeur de la propri�t� id.
     * 
     */
    public int getID() {
        return id;
    }

    /**
     * D�finit la valeur de la propri�t� id.
     * 
     */
    public void setID(int value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propri�t� userId.
     * 
     * @return
     *     possible object is
     *     {@link UserWS }
     *     
     */
    public UserWS getUserId() {
        return userId;
    }

    /**
     * D�finit la valeur de la propri�t� userId.
     * 
     * @param value
     *     allowed object is
     *     {@link UserWS }
     *     
     */
    public void setUserId(UserWS value) {
        this.userId = value;
    }

    /**
     * Obtient la valeur de la propri�t� bookId.
     * 
     * @return
     *     possible object is
     *     {@link BookWS }
     *     
     */
    public BookWS getBookId() {
        return bookId;
    }

    /**
     * D�finit la valeur de la propri�t� bookId.
     * 
     * @param value
     *     allowed object is
     *     {@link BookWS }
     *     
     */
    public void setBookId(BookWS value) {
        this.bookId = value;
    }

    /**
     * Obtient la valeur de la propri�t� isLate.
     * 
     */
    public boolean isIsLate() {
        return isLate;
    }

    /**
     * D�finit la valeur de la propri�t� isLate.
     * 
     */
    public void setIsLate(boolean value) {
        this.isLate = value;
    }

    /**
     * Obtient la valeur de la propri�t� positionOfUserInTheWaitingList.
     * 
     */
    public int getPositionOfUserInTheWaitingList() {
        return positionOfUserInTheWaitingList;
    }

    /**
     * D�finit la valeur de la propri�t� positionOfUserInTheWaitingList.
     * 
     */
    public void setPositionOfUserInTheWaitingList(int value) {
        this.positionOfUserInTheWaitingList = value;
    }

    /**
     * Obtient la valeur de la propri�t� isConfirm.
     * 
     */
    public boolean isIsConfirm() {
        return isConfirm;
    }

    /**
     * D�finit la valeur de la propri�t� isConfirm.
     * 
     */
    public void setIsConfirm(boolean value) {
        this.isConfirm = value;
    }

    /**
     * Obtient la valeur de la propri�t� messageForValidate.
     * 
     */
    public boolean isMessageForValidate() {
        return messageForValidate;
    }

    /**
     * D�finit la valeur de la propri�t� messageForValidate.
     * 
     */
    public void setMessageForValidate(boolean value) {
        this.messageForValidate = value;
    }

    /**
     * Obtient la valeur de la propri�t� expiratedBooking.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getExpiratedBooking() {
        return expiratedBooking;
    }

    /**
     * D�finit la valeur de la propri�t� expiratedBooking.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setExpiratedBooking(LocalDate value) {
        this.expiratedBooking = value;
    }

}
