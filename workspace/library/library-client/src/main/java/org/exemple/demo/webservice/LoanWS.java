
package org.exemple.demo.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour loanWS complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="loanWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="startingDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expiredDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="user" type="{http://webservice.demo.exemple.org/}userWS" minOccurs="0"/>
 *         &lt;element name="exemplary" type="{http://webservice.demo.exemple.org/}exemplaryWS" minOccurs="0"/>
 *         &lt;element name="late" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isConfirm" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="forZeroExemplary" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="trueIfUserClicOnExpiratedDateButton" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="qtyOfBookingForOneBook" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loanWS", propOrder = {
    "id",
    "startingDate",
    "expiredDate",
    "user",
    "exemplary",
    "late",
    "isConfirm",
    "forZeroExemplary",
    "trueIfUserClicOnExpiratedDateButton",
    "qtyOfBookingForOneBook"
})
public class LoanWS {

    protected int id;
    protected String startingDate;
    protected String expiredDate;
    protected UserWS user;
    protected ExemplaryWS exemplary;
    protected boolean late;
    protected int isConfirm;
    protected int forZeroExemplary;
    protected boolean trueIfUserClicOnExpiratedDateButton;
    protected int qtyOfBookingForOneBook;

    /**
     * Obtient la valeur de la propri�t� id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * D�finit la valeur de la propri�t� id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propri�t� startingDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartingDate() {
        return startingDate;
    }

    /**
     * D�finit la valeur de la propri�t� startingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartingDate(String value) {
        this.startingDate = value;
    }

    /**
     * Obtient la valeur de la propri�t� expiredDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiredDate() {
        return expiredDate;
    }

    /**
     * D�finit la valeur de la propri�t� expiredDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiredDate(String value) {
        this.expiredDate = value;
    }

    /**
     * Obtient la valeur de la propri�t� user.
     * 
     * @return
     *     possible object is
     *     {@link UserWS }
     *     
     */
    public UserWS getUser() {
        return user;
    }

    /**
     * D�finit la valeur de la propri�t� user.
     * 
     * @param value
     *     allowed object is
     *     {@link UserWS }
     *     
     */
    public void setUser(UserWS value) {
        this.user = value;
    }

    /**
     * Obtient la valeur de la propri�t� exemplary.
     * 
     * @return
     *     possible object is
     *     {@link ExemplaryWS }
     *     
     */
    public ExemplaryWS getExemplary() {
        return exemplary;
    }

    /**
     * D�finit la valeur de la propri�t� exemplary.
     * 
     * @param value
     *     allowed object is
     *     {@link ExemplaryWS }
     *     
     */
    public void setExemplary(ExemplaryWS value) {
        this.exemplary = value;
    }

    /**
     * Obtient la valeur de la propri�t� late.
     * 
     */
    public boolean isLate() {
        return late;
    }

    /**
     * D�finit la valeur de la propri�t� late.
     * 
     */
    public void setLate(boolean value) {
        this.late = value;
    }

    /**
     * Obtient la valeur de la propri�t� isConfirm.
     * 
     */
    public int getIsConfirm() {
        return isConfirm;
    }

    /**
     * D�finit la valeur de la propri�t� isConfirm.
     * 
     */
    public void setIsConfirm(int value) {
        this.isConfirm = value;
    }

    /**
     * Obtient la valeur de la propri�t� forZeroExemplary.
     * 
     */
    public int getForZeroExemplary() {
        return forZeroExemplary;
    }

    /**
     * D�finit la valeur de la propri�t� forZeroExemplary.
     * 
     */
    public void setForZeroExemplary(int value) {
        this.forZeroExemplary = value;
    }

    /**
     * Obtient la valeur de la propri�t� trueIfUserClicOnExpiratedDateButton.
     * 
     */
    public boolean isTrueIfUserClicOnExpiratedDateButton() {
        return trueIfUserClicOnExpiratedDateButton;
    }

    /**
     * D�finit la valeur de la propri�t� trueIfUserClicOnExpiratedDateButton.
     * 
     */
    public void setTrueIfUserClicOnExpiratedDateButton(boolean value) {
        this.trueIfUserClicOnExpiratedDateButton = value;
    }

    /**
     * Obtient la valeur de la propri�t� qtyOfBookingForOneBook.
     * 
     */
    public int getQtyOfBookingForOneBook() {
        return qtyOfBookingForOneBook;
    }

    /**
     * D�finit la valeur de la propri�t� qtyOfBookingForOneBook.
     * 
     */
    public void setQtyOfBookingForOneBook(int value) {
        this.qtyOfBookingForOneBook = value;
    }

}
