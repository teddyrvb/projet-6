
package org.exemple.demo.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour methodToDeleteBookingWhenTheDateIsExpired complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="methodToDeleteBookingWhenTheDateIsExpired">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://webservice.demo.exemple.org/}loanWS" minOccurs="0"/>
 *         &lt;element name="arg1" type="{http://webservice.demo.exemple.org/}userWS" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "methodToDeleteBookingWhenTheDateIsExpired", propOrder = {
    "arg0",
    "arg1"
})
public class MethodToDeleteBookingWhenTheDateIsExpired {

    protected LoanWS arg0;
    protected UserWS arg1;

    /**
     * Obtient la valeur de la propri�t� arg0.
     * 
     * @return
     *     possible object is
     *     {@link LoanWS }
     *     
     */
    public LoanWS getArg0() {
        return arg0;
    }

    /**
     * D�finit la valeur de la propri�t� arg0.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanWS }
     *     
     */
    public void setArg0(LoanWS value) {
        this.arg0 = value;
    }

    /**
     * Obtient la valeur de la propri�t� arg1.
     * 
     * @return
     *     possible object is
     *     {@link UserWS }
     *     
     */
    public UserWS getArg1() {
        return arg1;
    }

    /**
     * D�finit la valeur de la propri�t� arg1.
     * 
     * @param value
     *     allowed object is
     *     {@link UserWS }
     *     
     */
    public void setArg1(UserWS value) {
        this.arg1 = value;
    }

}
