
package org.exemple.demo.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour bookWS complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="bookWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="author" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="earliestDateForExemplaryId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numberBookForTheSameBooking" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numberExemplaryAvailable" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numberPage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trueForTheSameBooking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="trueForTheSameLoan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="trueIfBookEqualZero" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="trueIfLoanIsSaturated" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bookWS", propOrder = {
    "author",
    "earliestDateForExemplaryId",
    "id",
    "numberBookForTheSameBooking",
    "numberExemplaryAvailable",
    "numberPage",
    "title",
    "trueForTheSameBooking",
    "trueForTheSameLoan",
    "trueIfBookEqualZero",
    "trueIfLoanIsSaturated"
})
public class BookWS {

    protected String author;
    protected String earliestDateForExemplaryId;
    protected int id;
    protected int numberBookForTheSameBooking;
    protected int numberExemplaryAvailable;
    protected int numberPage;
    protected String title;
    protected boolean trueForTheSameBooking;
    protected boolean trueForTheSameLoan;
    protected boolean trueIfBookEqualZero;
    protected boolean trueIfLoanIsSaturated;

    /**
     * Obtient la valeur de la propri�t� author.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthor() {
        return author;
    }

    /**
     * D�finit la valeur de la propri�t� author.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthor(String value) {
        this.author = value;
    }

    /**
     * Obtient la valeur de la propri�t� earliestDateForExemplaryId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarliestDateForExemplaryId() {
        return earliestDateForExemplaryId;
    }

    /**
     * D�finit la valeur de la propri�t� earliestDateForExemplaryId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarliestDateForExemplaryId(String value) {
        this.earliestDateForExemplaryId = value;
    }

    /**
     * Obtient la valeur de la propri�t� id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * D�finit la valeur de la propri�t� id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propri�t� numberBookForTheSameBooking.
     * 
     */
    public int getNumberBookForTheSameBooking() {
        return numberBookForTheSameBooking;
    }

    /**
     * D�finit la valeur de la propri�t� numberBookForTheSameBooking.
     * 
     */
    public void setNumberBookForTheSameBooking(int value) {
        this.numberBookForTheSameBooking = value;
    }

    /**
     * Obtient la valeur de la propri�t� numberExemplaryAvailable.
     * 
     */
    public int getNumberExemplaryAvailable() {
        return numberExemplaryAvailable;
    }

    /**
     * D�finit la valeur de la propri�t� numberExemplaryAvailable.
     * 
     */
    public void setNumberExemplaryAvailable(int value) {
        this.numberExemplaryAvailable = value;
    }

    /**
     * Obtient la valeur de la propri�t� numberPage.
     * 
     */
    public int getNumberPage() {
        return numberPage;
    }

    /**
     * D�finit la valeur de la propri�t� numberPage.
     * 
     */
    public void setNumberPage(int value) {
        this.numberPage = value;
    }

    /**
     * Obtient la valeur de la propri�t� title.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * D�finit la valeur de la propri�t� title.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propri�t� trueForTheSameBooking.
     * 
     */
    public boolean isTrueForTheSameBooking() {
        return trueForTheSameBooking;
    }

    /**
     * D�finit la valeur de la propri�t� trueForTheSameBooking.
     * 
     */
    public void setTrueForTheSameBooking(boolean value) {
        this.trueForTheSameBooking = value;
    }

    /**
     * Obtient la valeur de la propri�t� trueForTheSameLoan.
     * 
     */
    public boolean isTrueForTheSameLoan() {
        return trueForTheSameLoan;
    }

    /**
     * D�finit la valeur de la propri�t� trueForTheSameLoan.
     * 
     */
    public void setTrueForTheSameLoan(boolean value) {
        this.trueForTheSameLoan = value;
    }

    /**
     * Obtient la valeur de la propri�t� trueIfBookEqualZero.
     * 
     */
    public boolean isTrueIfBookEqualZero() {
        return trueIfBookEqualZero;
    }

    /**
     * D�finit la valeur de la propri�t� trueIfBookEqualZero.
     * 
     */
    public void setTrueIfBookEqualZero(boolean value) {
        this.trueIfBookEqualZero = value;
    }

    /**
     * Obtient la valeur de la propri�t� trueIfLoanIsSaturated.
     * 
     */
    public boolean isTrueIfLoanIsSaturated() {
        return trueIfLoanIsSaturated;
    }

    /**
     * D�finit la valeur de la propri�t� trueIfLoanIsSaturated.
     * 
     */
    public void setTrueIfLoanIsSaturated(boolean value) {
        this.trueIfLoanIsSaturated = value;
    }

}
