
package org.exemple.demo.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.exemple.demo.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RetrieveAllUser_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllUser");
    private final static QName _UpdateBook_QNAME = new QName("http://webservice.demo.exemple.org/", "updateBook");
    private final static QName _InsertNewLoanResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "insertNewLoanResponse");
    private final static QName _ExpiratedDateResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "expiratedDateResponse");
    private final static QName _MethodToDeleteBookingWhenTheDateIsExpiredResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "methodToDeleteBookingWhenTheDateIsExpiredResponse");
    private final static QName _DeleteLoan_QNAME = new QName("http://webservice.demo.exemple.org/", "deleteLoan");
    private final static QName _FindUserByIdResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findUserByIdResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://webservice.demo.exemple.org/", "removeUser");
    private final static QName _LoanWS_QNAME = new QName("http://webservice.demo.exemple.org/", "LoanWS");
    private final static QName _FindBookById_QNAME = new QName("http://webservice.demo.exemple.org/", "findBookById");
    private final static QName _FindExemplaryNotInLoan_QNAME = new QName("http://webservice.demo.exemple.org/", "findExemplaryNotInLoan");
    private final static QName _FindLoanByUser_QNAME = new QName("http://webservice.demo.exemple.org/", "findLoanByUser");
    private final static QName _StartingDateForBookingResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "startingDateForBookingResponse");
    private final static QName _FindAllAvailableBooksResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findAllAvailableBooksResponse");
    private final static QName _UpdateUserResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "updateUserResponse");
    private final static QName _LoginResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "loginResponse");
    private final static QName _FindBookByIdResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findBookByIdResponse");
    private final static QName _RetrieveAllBookAndUser_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllBookAndUser");
    private final static QName _RetrieveAllBookResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllBookResponse");
    private final static QName _DisplayUserAndLoan_QNAME = new QName("http://webservice.demo.exemple.org/", "displayUserAndLoan");
    private final static QName _FindAllAvailableBooks_QNAME = new QName("http://webservice.demo.exemple.org/", "findAllAvailableBooks");
    private final static QName _RemoveBooking_QNAME = new QName("http://webservice.demo.exemple.org/", "removeBooking");
    private final static QName _DisplayUserAndLoanResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "displayUserAndLoanResponse");
    private final static QName _RetrieveAllBooksWhereBookingAreAvailableResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllBooksWhereBookingAreAvailableResponse");
    private final static QName _Restituer_QNAME = new QName("http://webservice.demo.exemple.org/", "restituer");
    private final static QName _FindBookResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findBookResponse");
    private final static QName _Prolonger_QNAME = new QName("http://webservice.demo.exemple.org/", "prolonger");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "removeUserResponse");
    private final static QName _FindBookingByIdResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findBookingByIdResponse");
    private final static QName _FindLoanByUserResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findLoanByUserResponse");
    private final static QName _FindAllUnavailableBooks_QNAME = new QName("http://webservice.demo.exemple.org/", "findAllUnavailableBooks");
    private final static QName _FindLoanWhereIsConfirmEqualOneResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findLoanWhereIsConfirmEqualOneResponse");
    private final static QName _RetrieveAllExemplaryResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllExemplaryResponse");
    private final static QName _FindBookingByUser_QNAME = new QName("http://webservice.demo.exemple.org/", "findBookingByUser");
    private final static QName _InsertNewBookingResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "insertNewBookingResponse");
    private final static QName _FindBook_QNAME = new QName("http://webservice.demo.exemple.org/", "findBook");
    private final static QName _RetrievePositionInListOfUserResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "retrievePositionInListOfUserResponse");
    private final static QName _RetrieveAllBookAndUserResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllBookAndUserResponse");
    private final static QName _InsertNewBook_QNAME = new QName("http://webservice.demo.exemple.org/", "insertNewBook");
    private final static QName _InsertNewBookResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "insertNewBookResponse");
    private final static QName _RemoveBookingResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "removeBookingResponse");
    private final static QName _FindLoanByIdResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findLoanByIdResponse");
    private final static QName _FindAllBooking_QNAME = new QName("http://webservice.demo.exemple.org/", "findAllBooking");
    private final static QName _InsertNewLoan_QNAME = new QName("http://webservice.demo.exemple.org/", "insertNewLoan");
    private final static QName _Login_QNAME = new QName("http://webservice.demo.exemple.org/", "login");
    private final static QName _StartingDateForBooking_QNAME = new QName("http://webservice.demo.exemple.org/", "startingDateForBooking");
    private final static QName _RetrieveAllBooksWhereBookingAreAvailable_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllBooksWhereBookingAreAvailable");
    private final static QName _FindBookingById_QNAME = new QName("http://webservice.demo.exemple.org/", "findBookingById");
    private final static QName _RemoveBookResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "removeBookResponse");
    private final static QName _ProlongerResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "prolongerResponse");
    private final static QName _FindAllUnavailableBooksResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findAllUnavailableBooksResponse");
    private final static QName _FindUserById_QNAME = new QName("http://webservice.demo.exemple.org/", "findUserById");
    private final static QName _MethodToManageBooking_QNAME = new QName("http://webservice.demo.exemple.org/", "methodToManageBooking");
    private final static QName _ExpiratedDate_QNAME = new QName("http://webservice.demo.exemple.org/", "expiratedDate");
    private final static QName _RetrieveAllUserResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllUserResponse");
    private final static QName _CreateATrueLoan_QNAME = new QName("http://webservice.demo.exemple.org/", "createATrueLoan");
    private final static QName _DeleteLoanResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "deleteLoanResponse");
    private final static QName _FindExemplaryByIdResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findExemplaryByIdResponse");
    private final static QName _RetrieveAllBook_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllBook");
    private final static QName _RetrievePositionInListOfUser_QNAME = new QName("http://webservice.demo.exemple.org/", "retrievePositionInListOfUser");
    private final static QName _UpdateUser_QNAME = new QName("http://webservice.demo.exemple.org/", "updateUser");
    private final static QName _FindExemplaryNotInLoanResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findExemplaryNotInLoanResponse");
    private final static QName _FindExemplaryById_QNAME = new QName("http://webservice.demo.exemple.org/", "findExemplaryById");
    private final static QName _InsertNewUser_QNAME = new QName("http://webservice.demo.exemple.org/", "insertNewUser");
    private final static QName _RestituerResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "restituerResponse");
    private final static QName _InsertNewUserResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "insertNewUserResponse");
    private final static QName _FindAllBookingResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findAllBookingResponse");
    private final static QName _MethodToManageBookingResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "methodToManageBookingResponse");
    private final static QName _FindAllUserAndThemLoanResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findAllUserAndThemLoanResponse");
    private final static QName _RetrieveAllExemplary_QNAME = new QName("http://webservice.demo.exemple.org/", "retrieveAllExemplary");
    private final static QName _UpdateBookResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "updateBookResponse");
    private final static QName _MethodToDeleteBookingWhenTheDateIsExpired_QNAME = new QName("http://webservice.demo.exemple.org/", "methodToDeleteBookingWhenTheDateIsExpired");
    private final static QName _FindAllUserAndThemLoan_QNAME = new QName("http://webservice.demo.exemple.org/", "findAllUserAndThemLoan");
    private final static QName _BookingWS_QNAME = new QName("http://webservice.demo.exemple.org/", "BookingWS");
    private final static QName _InsertNewBooking_QNAME = new QName("http://webservice.demo.exemple.org/", "insertNewBooking");
    private final static QName _FindLoanById_QNAME = new QName("http://webservice.demo.exemple.org/", "findLoanById");
    private final static QName _CreateATrueLoanResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "createATrueLoanResponse");
    private final static QName _FindLoanWhereIsConfirmEqualOne_QNAME = new QName("http://webservice.demo.exemple.org/", "findLoanWhereIsConfirmEqualOne");
    private final static QName _FindBookingByUserResponse_QNAME = new QName("http://webservice.demo.exemple.org/", "findBookingByUserResponse");
    private final static QName _RemoveBook_QNAME = new QName("http://webservice.demo.exemple.org/", "removeBook");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.exemple.demo.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrieveAllUser }
     * 
     */
    public RetrieveAllUser createRetrieveAllUser() {
        return new RetrieveAllUser();
    }

    /**
     * Create an instance of {@link UpdateBook }
     * 
     */
    public UpdateBook createUpdateBook() {
        return new UpdateBook();
    }

    /**
     * Create an instance of {@link InsertNewLoanResponse }
     * 
     */
    public InsertNewLoanResponse createInsertNewLoanResponse() {
        return new InsertNewLoanResponse();
    }

    /**
     * Create an instance of {@link ExpiratedDateResponse }
     * 
     */
    public ExpiratedDateResponse createExpiratedDateResponse() {
        return new ExpiratedDateResponse();
    }

    /**
     * Create an instance of {@link MethodToDeleteBookingWhenTheDateIsExpiredResponse }
     * 
     */
    public MethodToDeleteBookingWhenTheDateIsExpiredResponse createMethodToDeleteBookingWhenTheDateIsExpiredResponse() {
        return new MethodToDeleteBookingWhenTheDateIsExpiredResponse();
    }

    /**
     * Create an instance of {@link DeleteLoan }
     * 
     */
    public DeleteLoan createDeleteLoan() {
        return new DeleteLoan();
    }

    /**
     * Create an instance of {@link FindUserByIdResponse }
     * 
     */
    public FindUserByIdResponse createFindUserByIdResponse() {
        return new FindUserByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link LoanWS }
     * 
     */
    public LoanWS createLoanWS() {
        return new LoanWS();
    }

    /**
     * Create an instance of {@link FindExemplaryNotInLoan }
     * 
     */
    public FindExemplaryNotInLoan createFindExemplaryNotInLoan() {
        return new FindExemplaryNotInLoan();
    }

    /**
     * Create an instance of {@link FindBookById }
     * 
     */
    public FindBookById createFindBookById() {
        return new FindBookById();
    }

    /**
     * Create an instance of {@link FindLoanByUser }
     * 
     */
    public FindLoanByUser createFindLoanByUser() {
        return new FindLoanByUser();
    }

    /**
     * Create an instance of {@link StartingDateForBookingResponse }
     * 
     */
    public StartingDateForBookingResponse createStartingDateForBookingResponse() {
        return new StartingDateForBookingResponse();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     * 
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link FindAllAvailableBooksResponse }
     * 
     */
    public FindAllAvailableBooksResponse createFindAllAvailableBooksResponse() {
        return new FindAllAvailableBooksResponse();
    }

    /**
     * Create an instance of {@link FindBookByIdResponse }
     * 
     */
    public FindBookByIdResponse createFindBookByIdResponse() {
        return new FindBookByIdResponse();
    }

    /**
     * Create an instance of {@link RetrieveAllBookAndUser }
     * 
     */
    public RetrieveAllBookAndUser createRetrieveAllBookAndUser() {
        return new RetrieveAllBookAndUser();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link RetrieveAllBookResponse }
     * 
     */
    public RetrieveAllBookResponse createRetrieveAllBookResponse() {
        return new RetrieveAllBookResponse();
    }

    /**
     * Create an instance of {@link DisplayUserAndLoan }
     * 
     */
    public DisplayUserAndLoan createDisplayUserAndLoan() {
        return new DisplayUserAndLoan();
    }

    /**
     * Create an instance of {@link FindAllAvailableBooks }
     * 
     */
    public FindAllAvailableBooks createFindAllAvailableBooks() {
        return new FindAllAvailableBooks();
    }

    /**
     * Create an instance of {@link RemoveBooking }
     * 
     */
    public RemoveBooking createRemoveBooking() {
        return new RemoveBooking();
    }

    /**
     * Create an instance of {@link DisplayUserAndLoanResponse }
     * 
     */
    public DisplayUserAndLoanResponse createDisplayUserAndLoanResponse() {
        return new DisplayUserAndLoanResponse();
    }

    /**
     * Create an instance of {@link RetrieveAllBooksWhereBookingAreAvailableResponse }
     * 
     */
    public RetrieveAllBooksWhereBookingAreAvailableResponse createRetrieveAllBooksWhereBookingAreAvailableResponse() {
        return new RetrieveAllBooksWhereBookingAreAvailableResponse();
    }

    /**
     * Create an instance of {@link Restituer }
     * 
     */
    public Restituer createRestituer() {
        return new Restituer();
    }

    /**
     * Create an instance of {@link FindBookResponse }
     * 
     */
    public FindBookResponse createFindBookResponse() {
        return new FindBookResponse();
    }

    /**
     * Create an instance of {@link FindBookingByIdResponse }
     * 
     */
    public FindBookingByIdResponse createFindBookingByIdResponse() {
        return new FindBookingByIdResponse();
    }

    /**
     * Create an instance of {@link Prolonger }
     * 
     */
    public Prolonger createProlonger() {
        return new Prolonger();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link FindLoanByUserResponse }
     * 
     */
    public FindLoanByUserResponse createFindLoanByUserResponse() {
        return new FindLoanByUserResponse();
    }

    /**
     * Create an instance of {@link FindAllUnavailableBooks }
     * 
     */
    public FindAllUnavailableBooks createFindAllUnavailableBooks() {
        return new FindAllUnavailableBooks();
    }

    /**
     * Create an instance of {@link FindLoanWhereIsConfirmEqualOneResponse }
     * 
     */
    public FindLoanWhereIsConfirmEqualOneResponse createFindLoanWhereIsConfirmEqualOneResponse() {
        return new FindLoanWhereIsConfirmEqualOneResponse();
    }

    /**
     * Create an instance of {@link RetrieveAllExemplaryResponse }
     * 
     */
    public RetrieveAllExemplaryResponse createRetrieveAllExemplaryResponse() {
        return new RetrieveAllExemplaryResponse();
    }

    /**
     * Create an instance of {@link FindBook }
     * 
     */
    public FindBook createFindBook() {
        return new FindBook();
    }

    /**
     * Create an instance of {@link RetrievePositionInListOfUserResponse }
     * 
     */
    public RetrievePositionInListOfUserResponse createRetrievePositionInListOfUserResponse() {
        return new RetrievePositionInListOfUserResponse();
    }

    /**
     * Create an instance of {@link FindBookingByUser }
     * 
     */
    public FindBookingByUser createFindBookingByUser() {
        return new FindBookingByUser();
    }

    /**
     * Create an instance of {@link InsertNewBookingResponse }
     * 
     */
    public InsertNewBookingResponse createInsertNewBookingResponse() {
        return new InsertNewBookingResponse();
    }

    /**
     * Create an instance of {@link InsertNewBook }
     * 
     */
    public InsertNewBook createInsertNewBook() {
        return new InsertNewBook();
    }

    /**
     * Create an instance of {@link RetrieveAllBookAndUserResponse }
     * 
     */
    public RetrieveAllBookAndUserResponse createRetrieveAllBookAndUserResponse() {
        return new RetrieveAllBookAndUserResponse();
    }

    /**
     * Create an instance of {@link InsertNewBookResponse }
     * 
     */
    public InsertNewBookResponse createInsertNewBookResponse() {
        return new InsertNewBookResponse();
    }

    /**
     * Create an instance of {@link RemoveBookingResponse }
     * 
     */
    public RemoveBookingResponse createRemoveBookingResponse() {
        return new RemoveBookingResponse();
    }

    /**
     * Create an instance of {@link FindAllBooking }
     * 
     */
    public FindAllBooking createFindAllBooking() {
        return new FindAllBooking();
    }

    /**
     * Create an instance of {@link InsertNewLoan }
     * 
     */
    public InsertNewLoan createInsertNewLoan() {
        return new InsertNewLoan();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link StartingDateForBooking }
     * 
     */
    public StartingDateForBooking createStartingDateForBooking() {
        return new StartingDateForBooking();
    }

    /**
     * Create an instance of {@link FindLoanByIdResponse }
     * 
     */
    public FindLoanByIdResponse createFindLoanByIdResponse() {
        return new FindLoanByIdResponse();
    }

    /**
     * Create an instance of {@link FindBookingById }
     * 
     */
    public FindBookingById createFindBookingById() {
        return new FindBookingById();
    }

    /**
     * Create an instance of {@link RemoveBookResponse }
     * 
     */
    public RemoveBookResponse createRemoveBookResponse() {
        return new RemoveBookResponse();
    }

    /**
     * Create an instance of {@link RetrieveAllBooksWhereBookingAreAvailable }
     * 
     */
    public RetrieveAllBooksWhereBookingAreAvailable createRetrieveAllBooksWhereBookingAreAvailable() {
        return new RetrieveAllBooksWhereBookingAreAvailable();
    }

    /**
     * Create an instance of {@link ProlongerResponse }
     * 
     */
    public ProlongerResponse createProlongerResponse() {
        return new ProlongerResponse();
    }

    /**
     * Create an instance of {@link FindAllUnavailableBooksResponse }
     * 
     */
    public FindAllUnavailableBooksResponse createFindAllUnavailableBooksResponse() {
        return new FindAllUnavailableBooksResponse();
    }

    /**
     * Create an instance of {@link ExpiratedDate }
     * 
     */
    public ExpiratedDate createExpiratedDate() {
        return new ExpiratedDate();
    }

    /**
     * Create an instance of {@link FindUserById }
     * 
     */
    public FindUserById createFindUserById() {
        return new FindUserById();
    }

    /**
     * Create an instance of {@link MethodToManageBooking }
     * 
     */
    public MethodToManageBooking createMethodToManageBooking() {
        return new MethodToManageBooking();
    }

    /**
     * Create an instance of {@link RetrieveAllUserResponse }
     * 
     */
    public RetrieveAllUserResponse createRetrieveAllUserResponse() {
        return new RetrieveAllUserResponse();
    }

    /**
     * Create an instance of {@link CreateATrueLoan }
     * 
     */
    public CreateATrueLoan createCreateATrueLoan() {
        return new CreateATrueLoan();
    }

    /**
     * Create an instance of {@link DeleteLoanResponse }
     * 
     */
    public DeleteLoanResponse createDeleteLoanResponse() {
        return new DeleteLoanResponse();
    }

    /**
     * Create an instance of {@link FindExemplaryByIdResponse }
     * 
     */
    public FindExemplaryByIdResponse createFindExemplaryByIdResponse() {
        return new FindExemplaryByIdResponse();
    }

    /**
     * Create an instance of {@link RetrieveAllBook }
     * 
     */
    public RetrieveAllBook createRetrieveAllBook() {
        return new RetrieveAllBook();
    }

    /**
     * Create an instance of {@link RetrievePositionInListOfUser }
     * 
     */
    public RetrievePositionInListOfUser createRetrievePositionInListOfUser() {
        return new RetrievePositionInListOfUser();
    }

    /**
     * Create an instance of {@link UpdateUser }
     * 
     */
    public UpdateUser createUpdateUser() {
        return new UpdateUser();
    }

    /**
     * Create an instance of {@link FindExemplaryById }
     * 
     */
    public FindExemplaryById createFindExemplaryById() {
        return new FindExemplaryById();
    }

    /**
     * Create an instance of {@link FindExemplaryNotInLoanResponse }
     * 
     */
    public FindExemplaryNotInLoanResponse createFindExemplaryNotInLoanResponse() {
        return new FindExemplaryNotInLoanResponse();
    }

    /**
     * Create an instance of {@link RestituerResponse }
     * 
     */
    public RestituerResponse createRestituerResponse() {
        return new RestituerResponse();
    }

    /**
     * Create an instance of {@link InsertNewUser }
     * 
     */
    public InsertNewUser createInsertNewUser() {
        return new InsertNewUser();
    }

    /**
     * Create an instance of {@link FindAllBookingResponse }
     * 
     */
    public FindAllBookingResponse createFindAllBookingResponse() {
        return new FindAllBookingResponse();
    }

    /**
     * Create an instance of {@link InsertNewUserResponse }
     * 
     */
    public InsertNewUserResponse createInsertNewUserResponse() {
        return new InsertNewUserResponse();
    }

    /**
     * Create an instance of {@link MethodToManageBookingResponse }
     * 
     */
    public MethodToManageBookingResponse createMethodToManageBookingResponse() {
        return new MethodToManageBookingResponse();
    }

    /**
     * Create an instance of {@link FindAllUserAndThemLoanResponse }
     * 
     */
    public FindAllUserAndThemLoanResponse createFindAllUserAndThemLoanResponse() {
        return new FindAllUserAndThemLoanResponse();
    }

    /**
     * Create an instance of {@link RetrieveAllExemplary }
     * 
     */
    public RetrieveAllExemplary createRetrieveAllExemplary() {
        return new RetrieveAllExemplary();
    }

    /**
     * Create an instance of {@link MethodToDeleteBookingWhenTheDateIsExpired }
     * 
     */
    public MethodToDeleteBookingWhenTheDateIsExpired createMethodToDeleteBookingWhenTheDateIsExpired() {
        return new MethodToDeleteBookingWhenTheDateIsExpired();
    }

    /**
     * Create an instance of {@link UpdateBookResponse }
     * 
     */
    public UpdateBookResponse createUpdateBookResponse() {
        return new UpdateBookResponse();
    }

    /**
     * Create an instance of {@link FindAllUserAndThemLoan }
     * 
     */
    public FindAllUserAndThemLoan createFindAllUserAndThemLoan() {
        return new FindAllUserAndThemLoan();
    }

    /**
     * Create an instance of {@link BookingWS }
     * 
     */
    public BookingWS createBookingWS() {
        return new BookingWS();
    }

    /**
     * Create an instance of {@link InsertNewBooking }
     * 
     */
    public InsertNewBooking createInsertNewBooking() {
        return new InsertNewBooking();
    }

    /**
     * Create an instance of {@link CreateATrueLoanResponse }
     * 
     */
    public CreateATrueLoanResponse createCreateATrueLoanResponse() {
        return new CreateATrueLoanResponse();
    }

    /**
     * Create an instance of {@link FindLoanWhereIsConfirmEqualOne }
     * 
     */
    public FindLoanWhereIsConfirmEqualOne createFindLoanWhereIsConfirmEqualOne() {
        return new FindLoanWhereIsConfirmEqualOne();
    }

    /**
     * Create an instance of {@link FindLoanById }
     * 
     */
    public FindLoanById createFindLoanById() {
        return new FindLoanById();
    }

    /**
     * Create an instance of {@link FindBookingByUserResponse }
     * 
     */
    public FindBookingByUserResponse createFindBookingByUserResponse() {
        return new FindBookingByUserResponse();
    }

    /**
     * Create an instance of {@link RemoveBook }
     * 
     */
    public RemoveBook createRemoveBook() {
        return new RemoveBook();
    }

    /**
     * Create an instance of {@link BookWS }
     * 
     */
    public BookWS createBookWS() {
        return new BookWS();
    }

    /**
     * Create an instance of {@link LocalDate }
     * 
     */
    public LocalDate createLocalDate() {
        return new LocalDate();
    }

    /**
     * Create an instance of {@link UserWS }
     * 
     */
    public UserWS createUserWS() {
        return new UserWS();
    }

    /**
     * Create an instance of {@link ExemplaryWS }
     * 
     */
    public ExemplaryWS createExemplaryWS() {
        return new ExemplaryWS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllUser")
    public JAXBElement<RetrieveAllUser> createRetrieveAllUser(RetrieveAllUser value) {
        return new JAXBElement<RetrieveAllUser>(_RetrieveAllUser_QNAME, RetrieveAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateBook }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "updateBook")
    public JAXBElement<UpdateBook> createUpdateBook(UpdateBook value) {
        return new JAXBElement<UpdateBook>(_UpdateBook_QNAME, UpdateBook.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertNewLoanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "insertNewLoanResponse")
    public JAXBElement<InsertNewLoanResponse> createInsertNewLoanResponse(InsertNewLoanResponse value) {
        return new JAXBElement<InsertNewLoanResponse>(_InsertNewLoanResponse_QNAME, InsertNewLoanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExpiratedDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "expiratedDateResponse")
    public JAXBElement<ExpiratedDateResponse> createExpiratedDateResponse(ExpiratedDateResponse value) {
        return new JAXBElement<ExpiratedDateResponse>(_ExpiratedDateResponse_QNAME, ExpiratedDateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MethodToDeleteBookingWhenTheDateIsExpiredResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "methodToDeleteBookingWhenTheDateIsExpiredResponse")
    public JAXBElement<MethodToDeleteBookingWhenTheDateIsExpiredResponse> createMethodToDeleteBookingWhenTheDateIsExpiredResponse(MethodToDeleteBookingWhenTheDateIsExpiredResponse value) {
        return new JAXBElement<MethodToDeleteBookingWhenTheDateIsExpiredResponse>(_MethodToDeleteBookingWhenTheDateIsExpiredResponse_QNAME, MethodToDeleteBookingWhenTheDateIsExpiredResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteLoan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "deleteLoan")
    public JAXBElement<DeleteLoan> createDeleteLoan(DeleteLoan value) {
        return new JAXBElement<DeleteLoan>(_DeleteLoan_QNAME, DeleteLoan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findUserByIdResponse")
    public JAXBElement<FindUserByIdResponse> createFindUserByIdResponse(FindUserByIdResponse value) {
        return new JAXBElement<FindUserByIdResponse>(_FindUserByIdResponse_QNAME, FindUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanWS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "LoanWS")
    public JAXBElement<LoanWS> createLoanWS(LoanWS value) {
        return new JAXBElement<LoanWS>(_LoanWS_QNAME, LoanWS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBookById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findBookById")
    public JAXBElement<FindBookById> createFindBookById(FindBookById value) {
        return new JAXBElement<FindBookById>(_FindBookById_QNAME, FindBookById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindExemplaryNotInLoan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findExemplaryNotInLoan")
    public JAXBElement<FindExemplaryNotInLoan> createFindExemplaryNotInLoan(FindExemplaryNotInLoan value) {
        return new JAXBElement<FindExemplaryNotInLoan>(_FindExemplaryNotInLoan_QNAME, FindExemplaryNotInLoan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindLoanByUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findLoanByUser")
    public JAXBElement<FindLoanByUser> createFindLoanByUser(FindLoanByUser value) {
        return new JAXBElement<FindLoanByUser>(_FindLoanByUser_QNAME, FindLoanByUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartingDateForBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "startingDateForBookingResponse")
    public JAXBElement<StartingDateForBookingResponse> createStartingDateForBookingResponse(StartingDateForBookingResponse value) {
        return new JAXBElement<StartingDateForBookingResponse>(_StartingDateForBookingResponse_QNAME, StartingDateForBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllAvailableBooksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findAllAvailableBooksResponse")
    public JAXBElement<FindAllAvailableBooksResponse> createFindAllAvailableBooksResponse(FindAllAvailableBooksResponse value) {
        return new JAXBElement<FindAllAvailableBooksResponse>(_FindAllAvailableBooksResponse_QNAME, FindAllAvailableBooksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "updateUserResponse")
    public JAXBElement<UpdateUserResponse> createUpdateUserResponse(UpdateUserResponse value) {
        return new JAXBElement<UpdateUserResponse>(_UpdateUserResponse_QNAME, UpdateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBookByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findBookByIdResponse")
    public JAXBElement<FindBookByIdResponse> createFindBookByIdResponse(FindBookByIdResponse value) {
        return new JAXBElement<FindBookByIdResponse>(_FindBookByIdResponse_QNAME, FindBookByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllBookAndUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllBookAndUser")
    public JAXBElement<RetrieveAllBookAndUser> createRetrieveAllBookAndUser(RetrieveAllBookAndUser value) {
        return new JAXBElement<RetrieveAllBookAndUser>(_RetrieveAllBookAndUser_QNAME, RetrieveAllBookAndUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllBookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllBookResponse")
    public JAXBElement<RetrieveAllBookResponse> createRetrieveAllBookResponse(RetrieveAllBookResponse value) {
        return new JAXBElement<RetrieveAllBookResponse>(_RetrieveAllBookResponse_QNAME, RetrieveAllBookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisplayUserAndLoan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "displayUserAndLoan")
    public JAXBElement<DisplayUserAndLoan> createDisplayUserAndLoan(DisplayUserAndLoan value) {
        return new JAXBElement<DisplayUserAndLoan>(_DisplayUserAndLoan_QNAME, DisplayUserAndLoan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllAvailableBooks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findAllAvailableBooks")
    public JAXBElement<FindAllAvailableBooks> createFindAllAvailableBooks(FindAllAvailableBooks value) {
        return new JAXBElement<FindAllAvailableBooks>(_FindAllAvailableBooks_QNAME, FindAllAvailableBooks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "removeBooking")
    public JAXBElement<RemoveBooking> createRemoveBooking(RemoveBooking value) {
        return new JAXBElement<RemoveBooking>(_RemoveBooking_QNAME, RemoveBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisplayUserAndLoanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "displayUserAndLoanResponse")
    public JAXBElement<DisplayUserAndLoanResponse> createDisplayUserAndLoanResponse(DisplayUserAndLoanResponse value) {
        return new JAXBElement<DisplayUserAndLoanResponse>(_DisplayUserAndLoanResponse_QNAME, DisplayUserAndLoanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllBooksWhereBookingAreAvailableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllBooksWhereBookingAreAvailableResponse")
    public JAXBElement<RetrieveAllBooksWhereBookingAreAvailableResponse> createRetrieveAllBooksWhereBookingAreAvailableResponse(RetrieveAllBooksWhereBookingAreAvailableResponse value) {
        return new JAXBElement<RetrieveAllBooksWhereBookingAreAvailableResponse>(_RetrieveAllBooksWhereBookingAreAvailableResponse_QNAME, RetrieveAllBooksWhereBookingAreAvailableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Restituer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "restituer")
    public JAXBElement<Restituer> createRestituer(Restituer value) {
        return new JAXBElement<Restituer>(_Restituer_QNAME, Restituer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findBookResponse")
    public JAXBElement<FindBookResponse> createFindBookResponse(FindBookResponse value) {
        return new JAXBElement<FindBookResponse>(_FindBookResponse_QNAME, FindBookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Prolonger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "prolonger")
    public JAXBElement<Prolonger> createProlonger(Prolonger value) {
        return new JAXBElement<Prolonger>(_Prolonger_QNAME, Prolonger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBookingByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findBookingByIdResponse")
    public JAXBElement<FindBookingByIdResponse> createFindBookingByIdResponse(FindBookingByIdResponse value) {
        return new JAXBElement<FindBookingByIdResponse>(_FindBookingByIdResponse_QNAME, FindBookingByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindLoanByUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findLoanByUserResponse")
    public JAXBElement<FindLoanByUserResponse> createFindLoanByUserResponse(FindLoanByUserResponse value) {
        return new JAXBElement<FindLoanByUserResponse>(_FindLoanByUserResponse_QNAME, FindLoanByUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUnavailableBooks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findAllUnavailableBooks")
    public JAXBElement<FindAllUnavailableBooks> createFindAllUnavailableBooks(FindAllUnavailableBooks value) {
        return new JAXBElement<FindAllUnavailableBooks>(_FindAllUnavailableBooks_QNAME, FindAllUnavailableBooks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindLoanWhereIsConfirmEqualOneResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findLoanWhereIsConfirmEqualOneResponse")
    public JAXBElement<FindLoanWhereIsConfirmEqualOneResponse> createFindLoanWhereIsConfirmEqualOneResponse(FindLoanWhereIsConfirmEqualOneResponse value) {
        return new JAXBElement<FindLoanWhereIsConfirmEqualOneResponse>(_FindLoanWhereIsConfirmEqualOneResponse_QNAME, FindLoanWhereIsConfirmEqualOneResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllExemplaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllExemplaryResponse")
    public JAXBElement<RetrieveAllExemplaryResponse> createRetrieveAllExemplaryResponse(RetrieveAllExemplaryResponse value) {
        return new JAXBElement<RetrieveAllExemplaryResponse>(_RetrieveAllExemplaryResponse_QNAME, RetrieveAllExemplaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBookingByUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findBookingByUser")
    public JAXBElement<FindBookingByUser> createFindBookingByUser(FindBookingByUser value) {
        return new JAXBElement<FindBookingByUser>(_FindBookingByUser_QNAME, FindBookingByUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertNewBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "insertNewBookingResponse")
    public JAXBElement<InsertNewBookingResponse> createInsertNewBookingResponse(InsertNewBookingResponse value) {
        return new JAXBElement<InsertNewBookingResponse>(_InsertNewBookingResponse_QNAME, InsertNewBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBook }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findBook")
    public JAXBElement<FindBook> createFindBook(FindBook value) {
        return new JAXBElement<FindBook>(_FindBook_QNAME, FindBook.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePositionInListOfUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrievePositionInListOfUserResponse")
    public JAXBElement<RetrievePositionInListOfUserResponse> createRetrievePositionInListOfUserResponse(RetrievePositionInListOfUserResponse value) {
        return new JAXBElement<RetrievePositionInListOfUserResponse>(_RetrievePositionInListOfUserResponse_QNAME, RetrievePositionInListOfUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllBookAndUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllBookAndUserResponse")
    public JAXBElement<RetrieveAllBookAndUserResponse> createRetrieveAllBookAndUserResponse(RetrieveAllBookAndUserResponse value) {
        return new JAXBElement<RetrieveAllBookAndUserResponse>(_RetrieveAllBookAndUserResponse_QNAME, RetrieveAllBookAndUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertNewBook }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "insertNewBook")
    public JAXBElement<InsertNewBook> createInsertNewBook(InsertNewBook value) {
        return new JAXBElement<InsertNewBook>(_InsertNewBook_QNAME, InsertNewBook.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertNewBookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "insertNewBookResponse")
    public JAXBElement<InsertNewBookResponse> createInsertNewBookResponse(InsertNewBookResponse value) {
        return new JAXBElement<InsertNewBookResponse>(_InsertNewBookResponse_QNAME, InsertNewBookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "removeBookingResponse")
    public JAXBElement<RemoveBookingResponse> createRemoveBookingResponse(RemoveBookingResponse value) {
        return new JAXBElement<RemoveBookingResponse>(_RemoveBookingResponse_QNAME, RemoveBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindLoanByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findLoanByIdResponse")
    public JAXBElement<FindLoanByIdResponse> createFindLoanByIdResponse(FindLoanByIdResponse value) {
        return new JAXBElement<FindLoanByIdResponse>(_FindLoanByIdResponse_QNAME, FindLoanByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findAllBooking")
    public JAXBElement<FindAllBooking> createFindAllBooking(FindAllBooking value) {
        return new JAXBElement<FindAllBooking>(_FindAllBooking_QNAME, FindAllBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertNewLoan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "insertNewLoan")
    public JAXBElement<InsertNewLoan> createInsertNewLoan(InsertNewLoan value) {
        return new JAXBElement<InsertNewLoan>(_InsertNewLoan_QNAME, InsertNewLoan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartingDateForBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "startingDateForBooking")
    public JAXBElement<StartingDateForBooking> createStartingDateForBooking(StartingDateForBooking value) {
        return new JAXBElement<StartingDateForBooking>(_StartingDateForBooking_QNAME, StartingDateForBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllBooksWhereBookingAreAvailable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllBooksWhereBookingAreAvailable")
    public JAXBElement<RetrieveAllBooksWhereBookingAreAvailable> createRetrieveAllBooksWhereBookingAreAvailable(RetrieveAllBooksWhereBookingAreAvailable value) {
        return new JAXBElement<RetrieveAllBooksWhereBookingAreAvailable>(_RetrieveAllBooksWhereBookingAreAvailable_QNAME, RetrieveAllBooksWhereBookingAreAvailable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBookingById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findBookingById")
    public JAXBElement<FindBookingById> createFindBookingById(FindBookingById value) {
        return new JAXBElement<FindBookingById>(_FindBookingById_QNAME, FindBookingById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveBookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "removeBookResponse")
    public JAXBElement<RemoveBookResponse> createRemoveBookResponse(RemoveBookResponse value) {
        return new JAXBElement<RemoveBookResponse>(_RemoveBookResponse_QNAME, RemoveBookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProlongerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "prolongerResponse")
    public JAXBElement<ProlongerResponse> createProlongerResponse(ProlongerResponse value) {
        return new JAXBElement<ProlongerResponse>(_ProlongerResponse_QNAME, ProlongerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUnavailableBooksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findAllUnavailableBooksResponse")
    public JAXBElement<FindAllUnavailableBooksResponse> createFindAllUnavailableBooksResponse(FindAllUnavailableBooksResponse value) {
        return new JAXBElement<FindAllUnavailableBooksResponse>(_FindAllUnavailableBooksResponse_QNAME, FindAllUnavailableBooksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findUserById")
    public JAXBElement<FindUserById> createFindUserById(FindUserById value) {
        return new JAXBElement<FindUserById>(_FindUserById_QNAME, FindUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MethodToManageBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "methodToManageBooking")
    public JAXBElement<MethodToManageBooking> createMethodToManageBooking(MethodToManageBooking value) {
        return new JAXBElement<MethodToManageBooking>(_MethodToManageBooking_QNAME, MethodToManageBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExpiratedDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "expiratedDate")
    public JAXBElement<ExpiratedDate> createExpiratedDate(ExpiratedDate value) {
        return new JAXBElement<ExpiratedDate>(_ExpiratedDate_QNAME, ExpiratedDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllUserResponse")
    public JAXBElement<RetrieveAllUserResponse> createRetrieveAllUserResponse(RetrieveAllUserResponse value) {
        return new JAXBElement<RetrieveAllUserResponse>(_RetrieveAllUserResponse_QNAME, RetrieveAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateATrueLoan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "createATrueLoan")
    public JAXBElement<CreateATrueLoan> createCreateATrueLoan(CreateATrueLoan value) {
        return new JAXBElement<CreateATrueLoan>(_CreateATrueLoan_QNAME, CreateATrueLoan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteLoanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "deleteLoanResponse")
    public JAXBElement<DeleteLoanResponse> createDeleteLoanResponse(DeleteLoanResponse value) {
        return new JAXBElement<DeleteLoanResponse>(_DeleteLoanResponse_QNAME, DeleteLoanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindExemplaryByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findExemplaryByIdResponse")
    public JAXBElement<FindExemplaryByIdResponse> createFindExemplaryByIdResponse(FindExemplaryByIdResponse value) {
        return new JAXBElement<FindExemplaryByIdResponse>(_FindExemplaryByIdResponse_QNAME, FindExemplaryByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllBook }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllBook")
    public JAXBElement<RetrieveAllBook> createRetrieveAllBook(RetrieveAllBook value) {
        return new JAXBElement<RetrieveAllBook>(_RetrieveAllBook_QNAME, RetrieveAllBook.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePositionInListOfUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrievePositionInListOfUser")
    public JAXBElement<RetrievePositionInListOfUser> createRetrievePositionInListOfUser(RetrievePositionInListOfUser value) {
        return new JAXBElement<RetrievePositionInListOfUser>(_RetrievePositionInListOfUser_QNAME, RetrievePositionInListOfUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "updateUser")
    public JAXBElement<UpdateUser> createUpdateUser(UpdateUser value) {
        return new JAXBElement<UpdateUser>(_UpdateUser_QNAME, UpdateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindExemplaryNotInLoanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findExemplaryNotInLoanResponse")
    public JAXBElement<FindExemplaryNotInLoanResponse> createFindExemplaryNotInLoanResponse(FindExemplaryNotInLoanResponse value) {
        return new JAXBElement<FindExemplaryNotInLoanResponse>(_FindExemplaryNotInLoanResponse_QNAME, FindExemplaryNotInLoanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindExemplaryById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findExemplaryById")
    public JAXBElement<FindExemplaryById> createFindExemplaryById(FindExemplaryById value) {
        return new JAXBElement<FindExemplaryById>(_FindExemplaryById_QNAME, FindExemplaryById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertNewUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "insertNewUser")
    public JAXBElement<InsertNewUser> createInsertNewUser(InsertNewUser value) {
        return new JAXBElement<InsertNewUser>(_InsertNewUser_QNAME, InsertNewUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestituerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "restituerResponse")
    public JAXBElement<RestituerResponse> createRestituerResponse(RestituerResponse value) {
        return new JAXBElement<RestituerResponse>(_RestituerResponse_QNAME, RestituerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertNewUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "insertNewUserResponse")
    public JAXBElement<InsertNewUserResponse> createInsertNewUserResponse(InsertNewUserResponse value) {
        return new JAXBElement<InsertNewUserResponse>(_InsertNewUserResponse_QNAME, InsertNewUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findAllBookingResponse")
    public JAXBElement<FindAllBookingResponse> createFindAllBookingResponse(FindAllBookingResponse value) {
        return new JAXBElement<FindAllBookingResponse>(_FindAllBookingResponse_QNAME, FindAllBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MethodToManageBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "methodToManageBookingResponse")
    public JAXBElement<MethodToManageBookingResponse> createMethodToManageBookingResponse(MethodToManageBookingResponse value) {
        return new JAXBElement<MethodToManageBookingResponse>(_MethodToManageBookingResponse_QNAME, MethodToManageBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUserAndThemLoanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findAllUserAndThemLoanResponse")
    public JAXBElement<FindAllUserAndThemLoanResponse> createFindAllUserAndThemLoanResponse(FindAllUserAndThemLoanResponse value) {
        return new JAXBElement<FindAllUserAndThemLoanResponse>(_FindAllUserAndThemLoanResponse_QNAME, FindAllUserAndThemLoanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveAllExemplary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "retrieveAllExemplary")
    public JAXBElement<RetrieveAllExemplary> createRetrieveAllExemplary(RetrieveAllExemplary value) {
        return new JAXBElement<RetrieveAllExemplary>(_RetrieveAllExemplary_QNAME, RetrieveAllExemplary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateBookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "updateBookResponse")
    public JAXBElement<UpdateBookResponse> createUpdateBookResponse(UpdateBookResponse value) {
        return new JAXBElement<UpdateBookResponse>(_UpdateBookResponse_QNAME, UpdateBookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MethodToDeleteBookingWhenTheDateIsExpired }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "methodToDeleteBookingWhenTheDateIsExpired")
    public JAXBElement<MethodToDeleteBookingWhenTheDateIsExpired> createMethodToDeleteBookingWhenTheDateIsExpired(MethodToDeleteBookingWhenTheDateIsExpired value) {
        return new JAXBElement<MethodToDeleteBookingWhenTheDateIsExpired>(_MethodToDeleteBookingWhenTheDateIsExpired_QNAME, MethodToDeleteBookingWhenTheDateIsExpired.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUserAndThemLoan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findAllUserAndThemLoan")
    public JAXBElement<FindAllUserAndThemLoan> createFindAllUserAndThemLoan(FindAllUserAndThemLoan value) {
        return new JAXBElement<FindAllUserAndThemLoan>(_FindAllUserAndThemLoan_QNAME, FindAllUserAndThemLoan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BookingWS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "BookingWS")
    public JAXBElement<BookingWS> createBookingWS(BookingWS value) {
        return new JAXBElement<BookingWS>(_BookingWS_QNAME, BookingWS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertNewBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "insertNewBooking")
    public JAXBElement<InsertNewBooking> createInsertNewBooking(InsertNewBooking value) {
        return new JAXBElement<InsertNewBooking>(_InsertNewBooking_QNAME, InsertNewBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindLoanById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findLoanById")
    public JAXBElement<FindLoanById> createFindLoanById(FindLoanById value) {
        return new JAXBElement<FindLoanById>(_FindLoanById_QNAME, FindLoanById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateATrueLoanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "createATrueLoanResponse")
    public JAXBElement<CreateATrueLoanResponse> createCreateATrueLoanResponse(CreateATrueLoanResponse value) {
        return new JAXBElement<CreateATrueLoanResponse>(_CreateATrueLoanResponse_QNAME, CreateATrueLoanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindLoanWhereIsConfirmEqualOne }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findLoanWhereIsConfirmEqualOne")
    public JAXBElement<FindLoanWhereIsConfirmEqualOne> createFindLoanWhereIsConfirmEqualOne(FindLoanWhereIsConfirmEqualOne value) {
        return new JAXBElement<FindLoanWhereIsConfirmEqualOne>(_FindLoanWhereIsConfirmEqualOne_QNAME, FindLoanWhereIsConfirmEqualOne.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBookingByUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "findBookingByUserResponse")
    public JAXBElement<FindBookingByUserResponse> createFindBookingByUserResponse(FindBookingByUserResponse value) {
        return new JAXBElement<FindBookingByUserResponse>(_FindBookingByUserResponse_QNAME, FindBookingByUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveBook }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.demo.exemple.org/", name = "removeBook")
    public JAXBElement<RemoveBook> createRemoveBook(RemoveBook value) {
        return new JAXBElement<RemoveBook>(_RemoveBook_QNAME, RemoveBook.class, null, value);
    }

}
