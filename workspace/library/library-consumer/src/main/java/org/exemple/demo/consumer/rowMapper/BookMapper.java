package org.exemple.demo.consumer.rowMapper;

import org.exemple.demo.consumer.BookImpl;
import org.exemple.demo.model.Book;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper implements RowMapper<Book> {


        @Override
        public Book mapRow(ResultSet resultSet, int i) throws SQLException {

            Book book = new BookImpl();

            book.setId(resultSet.getInt("id"));
            book.setTitle(resultSet.getString("title"));
            book.setAuthor(resultSet.getString("author"));
            book.setNumberPage(resultSet.getInt("number_page"));

            return book;
        }
    }

