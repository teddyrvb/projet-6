package org.exemple.demo.consumer.contract.dao;


import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.User;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public interface UserDao {


    List<User> findAllUsers();

    User insertNewUser(String name, String firstName, String address, String phoneNumber,
                       String emailAddress);

    User updateUser(User user, String name, String firstName, String address,
                    String phoneNumber, String emailAddress);

    User findUserById(int id);

    void removeUser(int id);

    User login(String name, String emailAddress);
}
