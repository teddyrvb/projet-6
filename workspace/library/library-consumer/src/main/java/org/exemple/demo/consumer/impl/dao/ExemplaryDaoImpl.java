package org.exemple.demo.consumer.impl.dao;

import org.exemple.demo.consumer.ExemplaryImpl;
import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.consumer.contract.dao.ExemplaryDao;
import org.exemple.demo.consumer.rowMapper.BookMapper;
import org.exemple.demo.consumer.rowMapper.ExemplaryMapper;
import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ExemplaryDaoImpl implements ExemplaryDao {


    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private BookDao bookDao;


    @PostConstruct
    public void initialize() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }



    @Override
    public List<Exemplary> readsAllExemplary() {
        return this.jdbcTemplate.query("SELECT * FROM exemplary", new ExemplaryMapper(bookDao));
    }

    @Override
    public Exemplary insertNewExemplary() {
        Integer maxId = jdbcTemplate.queryForObject("SELECT max(id)+1 FROM exemplary", Integer.class);

        String sql = "INSERT INTO exemplary (id) VALUE (?)";

        jdbcTemplate.update(sql, maxId);

        return this.findExemplaryById(maxId);
    }

    @Override
    public Exemplary findExemplaryById(int id) {
        String query = "SELECT * FROM exemplary WHERE id = ?";

        return this.jdbcTemplate.queryForObject(query, new ExemplaryMapper(bookDao), id);
    }

    // sélectionne tout les exemplaires non lié à un loan
    @Override
    public Exemplary findExemplaryByBookIdNotInLoan(int id){
        String query = "SELECT * FROM exemplary WHERE exemplary.book_id = ? AND exemplary.id NOT IN(SELECT " +
                "exemplary_id FROM loan) LIMIT 1";

        return this.jdbcTemplate.queryForObject(query, new ExemplaryMapper(bookDao), id);
    }

    @Override
    public void removeExemplary(int id) {
        String sql = "DELETE FROM exemplary WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public List<Exemplary> findAllAvailableBooks() {

        return this.jdbcTemplate.query("SELECT * FROM exemplary", new ExemplaryMapper(bookDao));
    }

    @Override
    public List<Book> findAllUnavailableBooks() {

        return this.jdbcTemplate.query("SELECT * FROM book WHERE NOT EXISTS (SELECT null FROM exemplary WHERE exemplary.book_id = book.ID)", new BookMapper());
    }

    /* ==== permet d'obtenir le nombre d'exemplaire d'un book ==== */
    public List<Exemplary> findExemplaryByBook(Book book) {

        return this.jdbcTemplate.query("SELECT * FROM exemplary WHERE book_id = ?", new ExemplaryMapper(bookDao), book.getId());
    }
}
