package org.exemple.demo.consumer.rowMapper;

import org.exemple.demo.consumer.ExemplaryImpl;
import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ExemplaryMapper implements RowMapper<Exemplary> {

        @Autowired
        private BookDao bookDao;

        public ExemplaryMapper(BookDao bookDao) {
            this.bookDao = bookDao;
        }

        @Override
        public Exemplary mapRow(ResultSet resultSet, int i) throws SQLException {

            Book book = bookDao.findBookById(resultSet.getInt("book_id"));

            Exemplary exemplary = new ExemplaryImpl();
            exemplary.setId(resultSet.getInt("id"));

            exemplary.setBook(book);

            return exemplary;
        }
    }
