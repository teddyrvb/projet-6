package org.exemple.demo.consumer.impl.dao;

import org.exemple.demo.consumer.BookingImpl;
import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.consumer.contract.dao.BookingDao;
import org.exemple.demo.consumer.contract.dao.UserDao;
import org.exemple.demo.consumer.rowMapper.BookingMapper;
import org.exemple.demo.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class BookingDaoImpl implements BookingDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private BookDao bookDao;

    @Autowired
    private UserDao userDao;

    @PostConstruct
    public void initialize() {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /* ================================================================================ */
    /* ==== Méthodes de gestion de réservation  / Manage booking methods ==== */
    /* ================================================================================  */

    /* Afficher les réservations d'un utilisateur */
    @Override
    public List<Booking> findBookingByUser(User user) {
        Object[] args = new Object[1];
        args[0] = user.getId();

        return this.jdbcTemplate.query("SELECT * FROM booking " +
                "WHERE booking.user_id = ?", args, new BookingMapper(userDao, bookDao));
    }

    @Override
    public List<Booking> findBookingByBook(Book book) {
        Object[] args = new Object[1];
        args[0] = book.getId();

        return this.jdbcTemplate.query("SELECT * FROM booking" +
                " WHERE booking.book_id = ?", args, new BookingMapper(userDao, bookDao));
    }

    @Override
    public Booking insertNewBooking(User user, LocalDateTime startingDate, Book book) {

        String sql = "INSERT INTO booking(user_id, starting_date, book_id) VALUE(?, NOW(), ?)";

         jdbcTemplate.update(sql, user.getId(), book.getId());


        Booking bookingCreated = new BookingImpl();
        bookingCreated.setUserId(bookingCreated.getUserId());
        bookingCreated.setBookingStartingDate(bookingCreated.getBookingStartingDate());
        bookingCreated.setBookId(bookingCreated.getBookId());

        bookingCreated.setLocation(bookingCreated.getLocation());

        return bookingCreated;
    }

    /* ---- retrouve la réservation d'un id particulier ---- */
    @Override
    public Booking findBookingById(int id) {
        String querry ="SELECT * FROM booking WHERE ID = ?";

        return jdbcTemplate.queryForObject(querry, new BookingMapper(userDao, bookDao), id);
    }

    /* permet la suppression d'une réservation */
    @Override
    public void removeBooking(int id) {
        String sql = "DELETE FROM booking WHERE id = ?";

        jdbcTemplate.update(sql, id);
    }

    /* L'ensemble des réservations */
    @Override
    public List<Booking> findAllBooking() {

        return this.jdbcTemplate.query("SELECT * FROM booking", new BookingMapper(userDao, bookDao));
    }



    @Override
    public List<Booking> startingDateForBooking(User user) {
        Object[] args = new Object[1];
        args[0] = user.getId();

        return this.jdbcTemplate.query("SELECT * FROM booking WHERE starting_date = " +
                "library.loan.expired_date and user_id = ?", args, new BookingMapper(userDao, bookDao));
    }

    @Override
    public void twoDayForDate(Booking booking) {
        this.jdbcTemplate.update("UPDATE booking SET booking.starting_date =" +
                " DATE_ADD(booking.starting_date, INTERVAL 2 DAY) WHERE ID = ?;", booking.getID());
    }

    @Override
    public List<Booking> retrieveAllUserForTheSameBookOrderByDate(Book book){
        Object[] args = new Object[1];
        args[0] = book.getId();

        return this.jdbcTemplate.query("SELECT * FROM booking WHERE booking.book_id = ? " +
                "ORDER BY booking.starting_date", args, new BookingMapper(userDao, bookDao));
    }

    @Override
    public void incrementLocation(Booking booking) {
         this.jdbcTemplate.update("UPDATE booking SET location = location + 1 WHERE ID = ?", booking.getID());
    }


}
