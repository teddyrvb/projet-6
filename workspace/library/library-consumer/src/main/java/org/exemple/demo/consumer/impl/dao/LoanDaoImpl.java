package org.exemple.demo.consumer.impl.dao;


import org.exemple.demo.consumer.LoanImpl;
import org.exemple.demo.consumer.contract.dao.ExemplaryDao;
import org.exemple.demo.consumer.contract.dao.LoanDao;
import org.exemple.demo.consumer.contract.dao.UserDao;
import org.exemple.demo.consumer.rowMapper.LoanMapper;
import org.exemple.demo.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class LoanDaoImpl implements LoanDao {


    private JdbcTemplate jdbcTemplate;


    @Autowired
    private final UserDao userDao;

    @Autowired
    private final ExemplaryDao exemplaryDao;

    @Autowired
    private DataSource dataSource;



    public LoanDaoImpl(UserDao userDao, ExemplaryDao exemplaryDao) {
        this.userDao = userDao;
        this.exemplaryDao = exemplaryDao;
    }



    @PostConstruct
    public void initialize() {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }



    /* Afficher les prêts des utilisateurs*/
    @Override
    public List<Loan> displayUserAndLoan() {
        return jdbcTemplate.query("SELECT * FROM loan", new LoanMapper(userDao, exemplaryDao));
    }

    /*Affiche que les livres disponibles*/
    @Override
    public List<Loan> displayAloneBookAvailable() {
        return jdbcTemplate.query("SELECT * FROM loan", new LoanMapper(userDao, exemplaryDao));

    }

    @Override
    public Loan insertNewLoan(User user, Exemplary exemplary, LocalDateTime startingDate, LocalDateTime expiredDate, int isConfirm) {

        String sql = "INSERT INTO loan(user_id, exemplary_id, starting_date, expired_date, to_confirm) VALUE(?, ?, ?, ?, ?)";

        jdbcTemplate.update(sql, user.getId(), exemplary.getId(), startingDate, expiredDate, isConfirm);

        Loan loanCreated = new LoanImpl();
        loanCreated.setUser(loanCreated.getUser());
        loanCreated.setExemplary(loanCreated.getExemplary());
        loanCreated.setStartingDate(loanCreated.getStartingDate());
        loanCreated.setExpiredDate(loanCreated.getExpiredDate());
        loanCreated.isConfirm(loanCreated.getConfirm());

        return loanCreated;
    }

    /* ---- retrouve le prêt d'un id particulier ---- */
    @Override
    public Loan findLoanById(int id) {
        String querry ="SELECT * FROM loan WHERE ID = ?";

        return jdbcTemplate.queryForObject(querry, new LoanMapper(userDao, exemplaryDao), id);
    }

    @Override
    public List<Loan> findLoansByUser(User user) {

        Object[] args = new Object[1];
        args[0] = user.getId();

        return this.jdbcTemplate.query("SELECT * FROM loan " +
                "WHERE loan.user_id = ?", args, new LoanMapper(userDao, exemplaryDao));
    }

    @Override
    public void prolonger(Loan loan) {
        loan.prolonger();

        this.jdbcTemplate.update("UPDATE loan SET expired_date = ? WHERE ID = ?", loan.getExpiredDate(), loan.getId());
    }

    @Override
    public void toConfirm(Loan loan){

        this.jdbcTemplate.update("UPDATE loan SET to_confirm = 0 WHERE ID = ?", loan.getId());
    }

    @Override
    public void deleteLoan(Loan loan) {

        this.jdbcTemplate.update("DELETE FROM loan WHERE ID = ?", loan.getId());
    }

    @Override
    public List<Loan> findLoanByExemplary(Exemplary exemplary_id) {

        Object[] args = new Object[1];
        args[0] = exemplary_id.getId();

        /* ==== ici on fait une jointure pour lié l'exemplaire au prêt ==== */
        return this.jdbcTemplate.query("SELECT * FROM library.loan" +
                " WHERE exemplary_id = ?", args, new LoanMapper(userDao, exemplaryDao));
    }

    @Override
    public List<Loan> expiratedDate(User user) {
        Object[] args = new Object[1];
        args[0] = user.getId();

        return this.jdbcTemplate.query("SELECT * FROM loan where expired_date < CURRENT_DATE and user_id = ?",args,new LoanMapper(userDao,exemplaryDao));
    }

    @Override
    public List<Loan> findLoanByBook(Book book) {
        Object[] args = new Object[1];
        args[0] = book.getId();

        return  this.jdbcTemplate.query("SELECT * FROM loan INNER JOIN exemplary " +
                "ON exemplary.id = loan.exemplary_id " +
                "WHERE exemplary.book_id = ? " +
                " ORDER BY loan.expired_date", args, new LoanMapper(userDao, exemplaryDao));
    }

    @Override
    public List<Loan> findLoanOrderByExpiredDate(){
        return this.jdbcTemplate.query("SELECT * FROM loan ORDER BY expired_date" , new LoanMapper(userDao, exemplaryDao));
    }

    @Override
    public List<Loan> findLoanWhereIsConfirmEqualOne(){
        return this.jdbcTemplate.query("SELECT * FROM loan WHERE to_confirm = 1", new LoanMapper(userDao, exemplaryDao));
    }
}
