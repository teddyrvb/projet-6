package org.exemple.demo.consumer.contract.dao;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;

import java.util.List;

public interface ExemplaryDao {

    List<Exemplary> readsAllExemplary();

    Exemplary insertNewExemplary();

    Exemplary findExemplaryById(int id);

    void removeExemplary(int id);

    List<Exemplary> findAllAvailableBooks();

    List<Book> findAllUnavailableBooks();

    List<Exemplary> findExemplaryByBook(Book book);

    Exemplary findExemplaryByBookIdNotInLoan(int id);
}
