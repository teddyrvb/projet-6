package org.exemple.demo.consumer.impl.dao;

import org.exemple.demo.consumer.LoginImpl;
import org.exemple.demo.consumer.UserImpl;
import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.consumer.contract.dao.ExemplaryDao;
import org.exemple.demo.consumer.contract.dao.LoanDao;
import org.exemple.demo.consumer.contract.dao.UserDao;
import org.exemple.demo.consumer.rowMapper.UserMapper;
import org.exemple.demo.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {


    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;


    @PostConstruct
    public void initialize() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public List<User> findAllUsers() {
        return this.jdbcTemplate.query("SELECT * FROM user", new UserMapper());
    }


    @Override
    public User insertNewUser(String name, String firstName, String address, String phoneNumber, String emailAddress) {
        Integer maxId = jdbcTemplate.queryForObject("SELECT max(id)+1 FROM user", Integer.class);
        String sql = "INSERT INTO user (id, name, first_name, adress, phone_number, email_adress) VALUE (?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, maxId, name, firstName, address, phoneNumber, emailAddress);
        return this.findUserById(maxId);
    }

    @Override
    public User updateUser(User user, String name, String firstName, String address, String phoneNumber, String emailAddress) {
        String sql = "UPDATE user SET name = ?, first_name = ?, adress = ?, phone_number = ?, email_adress = ? WHERE id= ?";
        jdbcTemplate.update(sql, name, firstName, address, phoneNumber, emailAddress, user.getId());

        return findUserById(user.getId());
    }

    @Override
    public User findUserById(int id) {
        String query = "SELECT * FROM user WHERE id = ?";
        return this.jdbcTemplate.queryForObject(query, new UserMapper(), id);
    }

    @Override
    public void removeUser(int id) {
        String sql = "DELETE FROM user WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public User login(String name, String emailAddress) {

        Object[] args = new Object[2];
        args[0] = name;
        args[1] = emailAddress;
        List<User> lst = this.jdbcTemplate.query("SELECT * FROM user where name = ? and email_adress = ?",args,new UserMapper());
        if(lst.isEmpty() || lst.size()> 1){
            return null;
        }
        return lst.get(0);
    }
}
