package org.exemple.demo.consumer;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;

public class ExemplaryImpl implements Exemplary {

    private int id;
    private Book book;


    public ExemplaryImpl() {}


    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }


    @Override
    public Book getBook() {
        return book;
    }

    @Override
    public void setBook(Book book) {
        this.book = book;
    }
}
