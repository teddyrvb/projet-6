package org.exemple.demo.consumer.rowMapper;


import org.exemple.demo.consumer.BookingImpl;
import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.consumer.contract.dao.UserDao;
import org.exemple.demo.model.Book;
import org.exemple.demo.model.Booking;
import org.exemple.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;


public class BookingMapper implements RowMapper<Booking> {

    @Autowired
    UserDao userDao;

    @Autowired
    BookDao bookDao;

    public BookingMapper(UserDao userDao, BookDao bookDao) {
        this.userDao = userDao;
        this.bookDao = bookDao;
    }

    @Override
    public Booking mapRow(ResultSet resultSet, int i) throws SQLException {

        User user = userDao.findUserById(resultSet.getInt("user_id"));
        Book book = bookDao.findBookById(resultSet.getInt("book_id"));

        Booking booking = new BookingImpl();

        booking.setID(resultSet.getInt("ID"));
        booking.setBookingStartingDate(resultSet.getTimestamp("starting_date").toLocalDateTime());

        booking.setUserId(user);
        booking.setBookId(book);

        return booking;
    }
}

