package org.exemple.demo.consumer.impl.dao;

import org.exemple.demo.consumer.BookImpl;
import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.consumer.rowMapper.BookMapper;
import org.exemple.demo.consumer.rowMapper.ExemplaryMapper;
import org.exemple.demo.consumer.rowMapper.LoanMapper;
import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;


import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


// on indique à spring qu'il s'agit ici d'un repository
// permet le système de normalisation de sql
@Repository
public class BookDaoImpl implements BookDao {


    /* ==== import JDCB template ==== */
    private JdbcTemplate jdbcTemplate;

    /* ==== Injection de la datasource ==== */
    @Autowired
    private DataSource dataSource;

    /* ==== On injecte un objet dataSource dans le constructeur JdbcTemplate ==== */
    @PostConstruct
    public void initialize() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }



    @Override
    public List<Book> findAllBooks() {
        return this.jdbcTemplate.query("SELECT * FROM book", new BookMapper());
    }

    @Override
    public Book insertNewBook(String title, String author, int numberPage) {
        Integer maxId = jdbcTemplate.queryForObject("SELECT MAX(id)+1 FROM book", Integer.class);

        String sql = "INSERT INTO book (id, title, author, number_page) VALUE (?, ?, ?, ?)";

        jdbcTemplate.update(sql, maxId, title, author, numberPage);

        return this.findBookById(maxId);
    }

    @Override
    public Book updateBook(Book book, String title, String author, int numberPage) {
            String sql = "UPDATE book SET title = ?, author = ?, number_page = ? WHERE id= ?";
            jdbcTemplate.update(sql, title, author, numberPage, book.getId());

            return findBookById(book.getId());
    }

    @Override
    public Book findBookById(int id) {
        String query = "SELECT * FROM book WHERE id = ?";

       return this.jdbcTemplate.queryForObject(query, new BookMapper(), id);
    }

    @Override
    public void removeBook(int id) {
       String sql = "DELETE FROM book WHERE id = ?";

       this.jdbcTemplate.update(sql, id);
    }

    @Override
    public List<Book> retrieveAllBooksWhereBookingAreAvailable() {
        /* Au sein de la requête, on va retrancher le contenu que l'on ne souhaite pas avec not in */
        return this.jdbcTemplate.query(
                "SELECT * FROM book WHERE book.ID NOT IN " +
                        "(SELECT book.ID FROM book LEFT JOIN exemplary ON book.id = exemplary.book_id WHERE exemplary.id " +
                        "NOT IN (SELECT loan.exemplary_id FROM loan));", new BookMapper()
        );
    }

    @Override
    public List<Book> findBook(String title) {
        title = "%" + title + "%";

        return this.jdbcTemplate.query("SELECT * FROM book " +
                "WHERE book.title LIKE ?", new BookMapper(), title);
    }
}
