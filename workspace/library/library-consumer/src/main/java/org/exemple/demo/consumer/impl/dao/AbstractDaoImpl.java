package org.exemple.demo.consumer.impl.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public abstract class AbstractDaoImpl {

    @Autowired
    protected JdbcTemplate jdbcTemplate;


    public JdbcTemplate JdbcTemplate() {
        return jdbcTemplate;
    }
}
