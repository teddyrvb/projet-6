package org.exemple.demo.consumer.rowMapper;

import org.exemple.demo.consumer.UserImpl;
import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.consumer.contract.dao.ExemplaryDao;
import org.exemple.demo.consumer.contract.dao.LoanDao;
import org.exemple.demo.consumer.impl.dao.AbstractDaoImpl;
import org.exemple.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper extends AbstractDaoImpl implements RowMapper<User> {


        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {

            User user = new UserImpl();

            user.setId(resultSet.getInt("ID"));
            user.setName(resultSet.getString("name"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setAddress(resultSet.getString("adress"));
            user.setPhoneNumber(resultSet.getString("phone_number"));
            user.setEmailAddress(resultSet.getString("email_adress"));

            return user;
        }
    }
