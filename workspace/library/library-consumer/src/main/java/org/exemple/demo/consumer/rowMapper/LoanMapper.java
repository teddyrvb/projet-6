package org.exemple.demo.consumer.rowMapper;

import org.exemple.demo.consumer.LoanImpl;
import org.exemple.demo.consumer.contract.dao.ExemplaryDao;
import org.exemple.demo.consumer.contract.dao.UserDao;
import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.Loan;
import org.exemple.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoanMapper implements RowMapper<Loan> {

        @Autowired
        UserDao userDao;
        ExemplaryDao exemplaryDao;

        public LoanMapper(UserDao userDao, ExemplaryDao exemplaryDao) {
            this.userDao = userDao;
            this.exemplaryDao = exemplaryDao;
        }

        @Override
        public Loan mapRow(ResultSet resultSet, int i) throws SQLException {

            User user = userDao.findUserById(resultSet.getInt("user_id"));
            Exemplary exemplary = exemplaryDao.findExemplaryById(resultSet.getInt("exemplary_id"));

            Loan loan = new LoanImpl();

            loan.setId(resultSet.getInt("id"));

            loan.setStartingDate(resultSet.getTimestamp("starting_date").toLocalDateTime());
            loan.setExpiredDate(resultSet.getTimestamp("expired_date").toLocalDateTime());
            loan.isConfirm(resultSet.getInt("to_confirm"));


            loan.setUser(user);
            loan.setExemplary(exemplary);

            return loan;
        }
    }

