package org.exemple.demo.consumer.contract.dao;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Booking;
import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface BookingDao {

    List<Booking> findBookingByUser(User user);

    Booking insertNewBooking(User user, LocalDateTime startingDate, Book book);

    Booking findBookingById(int id);

    void removeBooking(int id);

    List<Booking> findAllBooking();

    List<Booking> startingDateForBooking(User user);

    void twoDayForDate(Booking booking);

    List<Booking> findBookingByBook(Book book);

    List<Booking> retrieveAllUserForTheSameBookOrderByDate(Book book);


    void incrementLocation(Booking booking);


}
