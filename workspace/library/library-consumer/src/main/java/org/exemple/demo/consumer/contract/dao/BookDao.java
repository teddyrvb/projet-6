package org.exemple.demo.consumer.contract.dao;

import org.exemple.demo.model.Book;

import java.util.Date;
import java.util.List;

public interface BookDao {

    List<Book> findAllBooks();

    Book insertNewBook(String title, String author, int numberPage);

    Book updateBook(Book book, String title, String author, int numberPage);

    void removeBook(int id);

    Book findBookById(int id);

    List<Book> retrieveAllBooksWhereBookingAreAvailable();

    List<Book> findBook(String title);
}
