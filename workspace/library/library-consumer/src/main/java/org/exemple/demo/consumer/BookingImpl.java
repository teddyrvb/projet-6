package org.exemple.demo.consumer;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Booking;
import org.exemple.demo.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class BookingImpl implements Booking {

    private int ID;
    private LocalDateTime bookingStartingDate;
    private Book bookId;
    private User userId;
    private int location;

    private boolean isLate;

    @Override
    public int getID() {
        return ID;
    }

    @Override
    public void setID(int ID) {
        this.ID = ID;
    }


    @Override
    public Book getBookId() {
        return bookId;
    }

    @Override
    public void setBookId(Book bookId) {
        this.bookId = bookId;
    }


    @Override
    public LocalDateTime getBookingStartingDate() {
        return bookingStartingDate;
    }

    @Override
    public void setBookingStartingDate(LocalDateTime bookingStartingDate) {
        this.bookingStartingDate = bookingStartingDate;
    }


    @Override
    public User getUserId() {
        return userId;
    }

    @Override
    public void setUserId(User userId) {
        this.userId = userId;
    }


    @Override
    public boolean isLate() {
        return isLate;
    }

    @Override
    public void setLate(boolean late) {
        isLate = late;
    }


    @Override
    public int getLocation() {
        return location;
    }

    @Override
    public void setLocation(int location) {
        this.location = location;
    }

}
