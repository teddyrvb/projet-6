package org.exemple.demo.consumer;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;


public class BookImpl implements Book {


    private int id;
    private String title;
    private String author;
    private int numberPage;


    public BookImpl() {}


public BookImpl(int id, String title, String author, int numberPage){
    this.id = id;
    this.title = title;
    this.author = author;
    this.numberPage = numberPage;
}



    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public void setAuthor(String author) {
        this.author = author;
    }


    @Override
    public int getNumberPage() {
        return numberPage;
    }

    @Override
    public void setNumberPage(int numberPage) {
        this.numberPage = numberPage;
    }
}
