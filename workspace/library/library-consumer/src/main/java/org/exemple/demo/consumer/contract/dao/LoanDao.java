package org.exemple.demo.consumer.contract.dao;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.Loan;
import org.exemple.demo.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface LoanDao {


    List<Loan>findLoanByExemplary(Exemplary exemplary_id);

    List<Loan> findLoansByUser(User user);

    List<Loan> displayUserAndLoan();

    Loan findLoanById(int id);

    void prolonger(Loan loan);

    void deleteLoan(Loan loan);

    List<Loan> displayAloneBookAvailable();

    Loan insertNewLoan(User user, Exemplary exemplary, LocalDateTime startingDate,
                       LocalDateTime expiredDate, int isConfirm);

    List<Loan> expiratedDate(User user);

    void toConfirm(Loan loan);

    List<Loan> findLoanByBook(Book book);

    List<Loan> findLoanOrderByExpiredDate();

    List<Loan> findLoanWhereIsConfirmEqualOne();
}
