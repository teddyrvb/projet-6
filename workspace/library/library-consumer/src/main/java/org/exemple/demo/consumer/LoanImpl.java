package org.exemple.demo.consumer;

import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.Loan;
import org.exemple.demo.model.User;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;


public class LoanImpl implements Loan {


    private int id;
    private LocalDateTime startingDate;
    private LocalDateTime expiredDate;
    private int isConfirm;
    private User user;
    private Exemplary exemplary;


    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }


    @Override
    public User getUser() {
        return this.user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public Exemplary getExemplary() {
        return this.exemplary;
    }

    @Override
    public void setExemplary(Exemplary exemplary) {
         this.exemplary = exemplary;
    }


    @Override
    public LocalDateTime getStartingDate() {
        return startingDate;
    }

    @Override
    public void setStartingDate(LocalDateTime startingDate) {
        this.startingDate = startingDate;
    }


    @Override
    public LocalDateTime getExpiredDate() {
        return expiredDate;
    }

    @Override
    public void setExpiredDate(LocalDateTime expiredDate) {
        this.expiredDate = expiredDate;
    }


    @Override
    public int getConfirm() {
        return isConfirm;
    }

    @Override
    public void isConfirm(int isConfirm) {
        this.isConfirm = isConfirm;
    }


    @Override
    public boolean isLate(){
        return expiredDate.isBefore(LocalDateTime.now());
    }

    @Override
    public boolean getLate() {
        return this.isLate();
    }


    @Override
    public void prolonger(){
        Timestamp test = new Timestamp(System.currentTimeMillis());
        expiredDate = startingDate.plusWeeks(8);
    }


    public void startDate(){
        setStartingDate(LocalDateTime.now());
    }
}
