package org.exemple.demo.consumer;

import org.exemple.demo.model.Login;

public class LoginImpl implements Login {

    private String name;
    private String emailAddress;


    public LoginImpl() {}


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String value) {
        this.name = value;
    }


    @Override
    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
