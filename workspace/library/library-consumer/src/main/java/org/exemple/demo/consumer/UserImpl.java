package org.exemple.demo.consumer;

import org.exemple.demo.model.User;

import javax.validation.constraints.Pattern;

public class UserImpl implements User {


    private int id;
    private String name;
    private String firstName;

    private String address;

    @Pattern(regexp = "^0[1-9]([-. ]?[0-9]{2}){4}$")
    private String phoneNumber;



    @Pattern(regexp = "^[a-zA-Z0-9._-]+@[a-zAZ0-9._-]{2,}\\.[a-z]{2,4}$")
    private String emailAdress;


    public UserImpl() {}


    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int value) {
        this.id = value;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }


    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    @Override
    public String getEmailAddress() {
        return emailAdress;
    }


    @Override
    public void setEmailAddress(String emailAdress) {
        this.emailAdress = emailAdress;
    }
}
