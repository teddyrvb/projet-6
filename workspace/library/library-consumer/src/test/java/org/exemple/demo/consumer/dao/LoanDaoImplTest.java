package org.exemple.demo.consumer.dao;

import org.exemple.demo.consumer.contract.dao.LoanDao;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class LoanDaoImplTest {

    @Autowired
    LoanDao loanDao;

    @Test
    public void verifyIfDisplayUserAndLoanWork(){
        Assert.assertNotNull(loanDao.displayUserAndLoan());
    }

    @Test
    public void verifyThatFindLoanByIdWork(){
        Assert.assertNotNull(loanDao.findLoanById(2).getExemplary().getBook().getAuthor());

        try{
            Assert.assertEquals(loanDao.findLoanById(8).getExemplary().getBook().getAuthor(),
                    "Stuart Farrimond");
        }catch (ComparisonFailure e){
            System.out.println("module consumer");
            System.out.println("classe LoanDaoImplTest");
            System.out.println("Jeu de donnée altéré ou test incorrect.");
            System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
        }
    }
}
