package org.exemple.demo.consumer;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.exemple.demo.model.Loan;
import org.exemple.demo.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class LoanImplTest {

    @Autowired
    Loan loan;

    @Autowired
    Exemplary exemplary;

    @Autowired
    User user;

    @Autowired
    Book book;

    public LocalDateTime dateFormater(String dateToParse){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(dateToParse, formatter);
    }

    @Test
    public void testForVerifyIfLoanIsCorrect(){

        // instanciation des classes
        Exemplary exemplaryTest = new ExemplaryImpl();
        User userTest = new UserImpl();
        Book bookTest = new BookImpl();

        // on définit un id pour chaque instance
        exemplaryTest.setId(1);
        userTest.setId(1);
        bookTest.setId(1);
        bookTest.setAuthor("testAuthor");

        // définition des jointures
        Loan loanTest = new LoanImpl();
        loanTest.setId(3);
        loanTest.setExemplary(exemplaryTest);
        loanTest.setUser(userTest);
        exemplaryTest.setBook(bookTest);


        Assert.assertNotNull(loanTest);
        Assert.assertNotEquals(loanTest.getId(), 2);
        Assert.assertNotNull(loanTest.getExemplary());
        Assert.assertNotNull(loanTest.getExemplary().getBook());
        Assert.assertNull(loanTest.getUser().getEmailAddress());
        Assert.assertNull(loanTest.getExemplary().getBook().getTitle());
        Assert.assertNotNull(loanTest.getExemplary().getBook().getAuthor());
        Assert.assertEquals(loanTest.getExemplary().getBook().getAuthor(),
                "testAuthor");


        // Gestion des dates
        LocalDateTime today = LocalDateTime.now();
        loanTest.setStartingDate(LocalDateTime.now().minusDays(2));

        LocalDateTime expiredDate = dateFormater("2020-02-04 06:07:24");
        loanTest.setExpiredDate(expiredDate);

        LocalDateTime beforeExpiredDate = dateFormater("2020-02-04 06:07:23");
        LocalDateTime afterExpiredDate = dateFormater("2020-02-04 06:07:25");
        LocalDateTime equalToExpiredDate = dateFormater("2020-02-04 06:07:24");

        Assert.assertTrue(today.isAfter(loanTest.getStartingDate())
        || today.isEqual(loanTest.getStartingDate()));

        Assert.assertNotEquals(loanTest.getExpiredDate(), afterExpiredDate);
        Assert.assertNotEquals(loanTest.getExpiredDate(), beforeExpiredDate);
        Assert.assertEquals(loanTest.getExpiredDate(), equalToExpiredDate);

        LocalDateTime startingDate = expiredDate;

        loanTest.setStartingDate(startingDate);

        Assert.assertNotEquals(loanTest.getStartingDate(), afterExpiredDate);
        Assert.assertNotEquals(loanTest.getStartingDate(), beforeExpiredDate);
        Assert.assertEquals(loanTest.getStartingDate(), equalToExpiredDate);
    }
}
