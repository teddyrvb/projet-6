package org.exemple.demo.consumer.dao;

import org.exemple.demo.consumer.contract.dao.UserDao;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class UserDaoImplTest {


    @Autowired
    UserDao userDao;

    @Test
    public void verifyThatFindAllUsersWork(){
        Assert.assertNotNull(userDao.findAllUsers());
    }

    @Test
    public void verifyThatFindUserByIdWork(){
        Assert.assertNotNull(userDao.findUserById(2).getAddress());

        try{
            Assert.assertEquals(userDao.findUserById(2).getName(),
                    "Thomas");
        }catch (ComparisonFailure e){
            System.out.println("module consumer");
            System.out.println("classe UserDaoImplTest");
            System.out.println("Jeu de donnée altéré ou test incorrect.");
            System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
        }
    }
}
