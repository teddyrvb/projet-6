package org.exemple.demo.consumer;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Exemplary;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class ExemplaryImplTest {

    @Autowired
    Exemplary exemplary;

    @Autowired
    Book book;

    @Test
    public void testIfExemplaryWorkCorrectly(){

        Exemplary exemplaryTest = new ExemplaryImpl();
        Book bookTest = new BookImpl();

        bookTest.setId(1);
        bookTest.setAuthor("authorTest");
        bookTest.setNumberPage(176);

        exemplaryTest.setBook(bookTest);
        exemplaryTest.setId(3);

        Assert.assertNotNull(exemplaryTest.getBook());
        Assert.assertEquals(exemplaryTest.getBook().getNumberPage(), 176);
    }
}
