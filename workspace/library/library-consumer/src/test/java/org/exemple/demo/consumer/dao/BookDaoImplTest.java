package org.exemple.demo.consumer.dao;


import org.exemple.demo.consumer.contract.dao.BookDao;
import org.exemple.demo.model.Book;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class BookDaoImplTest {

    @Autowired
    BookDao bookDao;

    @Test
    public void verifyThatTheMethodFindAllBookWork(){
        Assert.assertNotNull(bookDao.findAllBooks());

        for (Book book: bookDao.findAllBooks()){
            Assert.assertNotNull(book.getTitle(), book.getAuthor());
        }
    }

    @Test
    public void verifyThatFindBookByIdReturnGoodInfo() {

        try{
            Assert.assertEquals(bookDao.findBookById(2).getAuthor(), "J.R.R. Tolkien");
        }catch (ComparisonFailure e) {
            System.out.println("module consumer");
            System.out.println("classe BookDaoImplTest");
            System.out.println("Jeu de donnée altéré ou test incorrect.");
            System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
        }

        // vérifie l'incrémentation en base
        try{
            Assert.assertNotEquals(bookDao.findBookById(1).getId(), bookDao.findBookById(2).getId());
        } catch (ComparisonFailure e){
            System.out.println("Incrémentation en erreur.");
        }

    }
}
