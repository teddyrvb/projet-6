package org.exemple.demo.consumer.dao;

import org.exemple.demo.consumer.contract.dao.BookingDao;
import org.exemple.demo.model.Book;
import org.exemple.demo.model.Booking;
import org.exemple.demo.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class BookingdDaoImplTest {

    @Autowired
    BookingDao bookingDao;

    @Autowired
    Book book;

    @Autowired
    User user;

    @Test
    public void verifyIfFindBookingByBookReturnBook(){
        for (Booking booking : bookingDao.findBookingByBook(book)){
            Assert.assertNotNull(booking.getBookId().getAuthor(), booking.getBookId().getTitle());
        }
    }

    @Test
    public void verifyIfFindBookingByUserReturnUser(){
        for(Booking booking: bookingDao.findBookingByUser(user)){
            Assert.assertNotNull(booking.getUserId().getName(), booking.getUserId().getFirstName());
        }
    }
}
