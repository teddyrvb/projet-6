package org.exemple.demo.consumer;

import org.exemple.demo.model.Book;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class BookImplTest {

    @Autowired
    BookImpl book;

    @Test
    public void verifyIfBookReturnCorrectValue() {

        Book bookTest = new BookImpl();
        bookTest.setId(1);
        bookTest.setAuthor("name");
        bookTest.setNumberPage(100);
        bookTest.setTitle("title");

        Assert.assertEquals(bookTest.getId(), 1);
        Assert.assertEquals(bookTest.getAuthor(), "name");
        Assert.assertEquals(bookTest.getNumberPage(), 100);
        Assert.assertEquals(bookTest.getTitle(), "title");

        bookTest.setAuthor("");
        bookTest.setNumberPage(50);

        Assert.assertNotEquals(bookTest.getAuthor(), "name");
        Assert.assertNotEquals(bookTest.getNumberPage(), 100);
    }
}
