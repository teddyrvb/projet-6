package org.exemple.demo.consumer.dao;

import org.exemple.demo.consumer.contract.dao.ExemplaryDao;
import org.exemple.demo.model.Exemplary;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class ExemplaryDaoImplTest {

    @Autowired
    ExemplaryDao exemplaryDao;

    @Test
    public void verifyThatMethodWhoRetrieveAllExemplaryWork(){
        Assert.assertNotNull(exemplaryDao.readsAllExemplary());

        for(Exemplary exemplary : exemplaryDao.readsAllExemplary()){
            Assert.assertNotNull(exemplary.getBook().getTitle());
        }
    }

    @Test
    public void verifyIfRetrieveExemplaryByIdWork(){
       Assert.assertNotNull(exemplaryDao.findExemplaryById(2).getBook().getTitle());

       try{
           Assert.assertEquals(exemplaryDao.findExemplaryById(4).getBook().getAuthor(),
                   "Phil Szostak");
       } catch (ComparisonFailure e){
           System.out.println("module consumer");
           System.out.println("classe ExemplaryDaoImplTest");
           System.out.println("Jeu de donnée altéré ou test incorrect.");
           System.out.println("Ce qui aurait du être trouvé : " + e.getMessage());
       }

    }
}
