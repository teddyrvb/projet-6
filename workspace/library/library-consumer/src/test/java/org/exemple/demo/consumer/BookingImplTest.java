package org.exemple.demo.consumer;

import org.exemple.demo.model.Book;
import org.exemple.demo.model.Booking;
import org.exemple.demo.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class BookingImplTest {

    @Autowired
    Booking booking;

    @Autowired
    Book book;

    @Autowired
    User user;

    public LocalDateTime dateFormater(String dateToParse){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(dateToParse, formatter);
    }

    @Test
    public void verifyIfBookingReturnCorrectValue(){
        Booking bookingTest = new BookingImpl();

        bookingTest.setID(1);
        bookingTest.setBookId(book);
        bookingTest.setUserId(user);

        bookingTest.setLocation(1);
        bookingTest.setLate(true);

        Assert.assertEquals(bookingTest.getID(), 1);
        Assert.assertEquals(bookingTest.getLocation(), 1);
        Assert.assertTrue(bookingTest.isLate());

        bookingTest.setBookingStartingDate(dateFormater("2020-02-07 02:45:38"));
        LocalDateTime notTheSameDateThatBookingStartingDateForSecond = dateFormater("2020-02-07 02:45:39");
        LocalDateTime theSameDateThatBookingStartingDateForSecond= dateFormater("2020-02-07 02:45:38");

        Assert.assertEquals(bookingTest.getBookingStartingDate(), theSameDateThatBookingStartingDateForSecond);
        Assert.assertNotEquals(bookingTest.getBookingStartingDate(), notTheSameDateThatBookingStartingDateForSecond);
    }
}
