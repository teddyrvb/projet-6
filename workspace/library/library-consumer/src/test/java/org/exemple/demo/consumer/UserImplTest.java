package org.exemple.demo.consumer;

import org.exemple.demo.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/org/exemple/demo/consumer/applicationContext.xml"})
public class UserImplTest {

    @Autowired
    User user;

    @Test
    public void testIfUserReturnGoodValue(){
        User userTest = new UserImpl();
        userTest.setId(1);
        userTest.setAddress("6 rue du coin");
        userTest.setName("name");
        userTest.setEmailAddress("userTest@gmail.com");
        userTest.setPhoneNumber("08 54 67 34 23");

        Assert.assertNotNull(userTest);
        Assert.assertEquals(userTest.getAddress(), "6 rue du coin");
        Assert.assertNotEquals(userTest.getName(), "Name");
        Assert.assertNull(userTest.getFirstName());
        Assert.assertNotNull(userTest.getEmailAddress());
        Assert.assertEquals(userTest.getPhoneNumber(), "08 54 67 34 23");

        User userTest2 = new UserImpl();
        userTest2.setId(2);
        userTest2.setAddress("6 rue du coin");

        Assert.assertNotSame(userTest, userTest2);


        //Permet de vérifier que la regex pour la l'adresse mail à bien le comportement souhaité
        Assert.assertTrue(userTest.getEmailAddress(),
                userTest.getEmailAddress()
                        .matches("^[a-zA-Z0-9._-]+@[a-zAZ0-9._-]{2,}\\.[a-z]{2,3}$"));

        //Exemple ou l'adresse mail n'est pas conforme
        userTest2.setEmailAddress("trucBidon.com");

        Assert.assertFalse(userTest2.getEmailAddress(),
                userTest2.getEmailAddress()
                        .matches("^[a-zA-Z0-9._-]+@[a-zAZ0-9._-]{2,}\\.[a-z]{2,4}$"));


        //Vérification de la regex pour le numéro de téléphone ou c'est bon
        userTest.setPhoneNumber("0632985618");
        Assert.assertTrue(userTest.getPhoneNumber(),
                userTest.getPhoneNumber().matches("^0[1-9]([-. ]?[0-9]{2}){4}$"));

        //Vérification de la regex pour un numéro de téléphone juste mais avec des espaces
        userTest.setPhoneNumber("06 32 98 56 18");
        Assert.assertTrue(userTest.getPhoneNumber(),
                userTest.getPhoneNumber().matches("^0[1-9]([-. ]?[0-9]{2}){4}$"));

        // Vérification pour le cas ou il n'y a pas le zéro au début du numéro
        userTest2.setPhoneNumber("5434765645");
        Assert.assertFalse(userTest.getPhoneNumber(),
                userTest2.getPhoneNumber().matches("^0[1-9]([-. ]?[0-9]{2}){4}$"));

        // Vérification pour le cas ou le numéro de téléphone est trop long
        userTest2.setPhoneNumber("05434765645");
        Assert.assertFalse(userTest.getPhoneNumber(),
                userTest2.getPhoneNumber().matches("^0[1-9]([-. ]?[0-9]{2}){4}$"));

    }
}

