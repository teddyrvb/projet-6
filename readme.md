# Projet 6 – Améliorez le système d’information de la bibliothèque

## Contenu
Ce projet est un site web permettant à différents utilisateurs de pouvoir effectuer des prêts ou des réservations au sein de l’application.

## Prérequis
Java jdk 1.8.0
Maven 3.6.3

## Base de données

Les scripts SQL présents dans src/main/resources/script_sql permettent de créer les tables d’une base de données avec un jeu de données.

## Exécutez les scripts, après avoir créé une base de données, dans cet ordre :
Création des tables :
- script_table.sql

## Alimentation des données :
- jeu_de_donnees.sql

Le SGBD (Système de gestion de Base de Données) configuré dans ce projet est **MySQL**, si vous utilisez aussi **MySQL**, 
il faudra alors, pour pouvoir connecter l’application à la base de données, changer :
1. Le **nom de la base de données**, que vous souhaitez utiliser.
2. Le **nom de l’utilisateur**.
3. Le **mot de passe**. 

Dans le répertoire library-consumer, dans le fichier src/main/resources/org/exemple/demo/consumer/applicationContext.xml, 
veuillez rentrer les valeurs adéquates au niveau des valeurs jdbcUrl, user et password. Cela correspond aux données de la base de données:


```xml
	<bean id="libraryDS"
		  class="com.mchange.v2.c3p0.ComboPooledDataSource">
		<property name="driverClass" value="com.mysql.jdbc.Driver" />
		<property name="jdbcUrl" value="jdbc:mysql://localhost:3306/{ici le nom de la base de donnée}?useUnicode=true&amp;useJDBCCompliantTimezoneShift=true&amp;useLegacyDatetimeCode=false&amp;serverTimezone=UTC" />
		<property name="user" value="identifiant de connexion à la base de donnée" />
		<property name="password" value="mot de passe de connexion à la base de donnée" />
		<property name="idleConnectionTestPeriod" value="120" />
		<property name="maxStatements" value="0" />
		<property name="maxPoolSize" value="15" />
		<property name="minPoolSize" value="3" />
	</bean>
```

Refaire la même chose dans le répertoire library-business, dans le fichier src/main/resources/org/exemple/demo/business/applicationContext.xml
Ainsi que dans le répertoire library-webservice, dans le fichier src/main/resources/org/exemple/demo/webservice/applicationContext.xml

Et pour finir, dans le répertoire library-webapp, dans le fichier src/main/resources/webapp/WEB-INF/application-context.xml

Et vu que ce dernier est relié à un fichier application.properties, changer les valeurs dans le fichier src/main/resources/application.properties:


```
database.jdbcUrl=jdbc:mysql://localhost:3306/{ici le nom de la base de donnée}?useLegacyDatetimeCode=false&serverTimezone=UTC

database.username=identifiant de connexion à la base de donnée

database.password=mot de passe de connexion à la base de donnée
```



## Déploiement dans un conteneur Web
Version **Tomcat** utilisée dans ce projet : Apache Tomcat 8.5.49
Générez un package au format WAR dans le répertoire target avec la méthode suivante :

## Récupérez le projet Gitlab en copiant ce lien :
https://gitlab.com/teddyrvb/projet-6

Ouvrez le terminal ou la console, placez-vous dans le dossier où vous voulez déposer le projet.
Clonez le projet **Gitlab** en exécutant cette commande :
git clone [projet 6](https://gitlab.com/teddyrvb/projet-6.git)

Placez-vous dans le dossier projet-6/workspace/library
Exécutez la commance : ```mvn clean package```
un war applicatif se trouve dans le dossier library-webapp et un war service se trouve dans le dossier libary-webservice.
A la racine du Tomcat, ajouter d’abord le library-webservice.war puis le library-webapp.war.
La commande ```bin/startup.bat``` à la racine du tomcat permet de lancer un serveur tomcat.
